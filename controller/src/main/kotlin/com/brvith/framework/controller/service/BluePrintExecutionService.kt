package com.brvith.framework.controller.service

import com.brvith.framework.controller.ControllerProperty
import com.brvith.framework.controller.data.*
import com.brvith.framework.core.interfaces.BlueprintStateMachineService
import com.brvith.framework.core.service.BluePrintContext
import com.brvith.framework.core.utils.BluePrintMetadataUtils
import com.brvith.framework.core.utils.BluePrintRuntimeUtils
import com.brvith.framework.core.utils.JacksonUtils
import com.brvith.framework.modules.db.service.BlueprintCatalogDBService
import com.google.common.base.CaseFormat
import org.apache.commons.io.FileUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import reactor.core.publisher.toMono
import java.io.File

@Service
class BluePrintExecutionService(
        private val controllerProperty: ControllerProperty,
        private val blueprintCatalogDBService: BlueprintCatalogDBService,
        private val blueprintStateMachineService: BlueprintStateMachineService) {

    val logger: Logger = LoggerFactory.getLogger(this::class.toString())


    fun execute(executionRequest: ExecutionRequest): Mono<ExecutionResponse> {
        return {
            val header: Header = executionRequest.header
            val requestId = header.requestId

            val blueprintIdentifier: BlueprintIdentifier = executionRequest.blueprintIdentifier
            var status: Status = Status(code = "200", message = "Success")
            try {
                logger.info("received execution request {}", executionRequest)

                val mode: String = blueprintIdentifier.executionMode

                val data: MutableMap<String, Any> = executionRequest.data as MutableMap<String, Any>
                logger.info("Request process data {}", data)

                val blueprintExecuteLocation = StringBuffer(controllerProperty.executionPath).append(File.separator)
                        .append(requestId).toString()

                prepareExecution(executionRequest).doOnSuccess { deployedPlans ->

                    val outputs = execute(requestId, blueprintExecuteLocation, blueprintIdentifier.workflowName, data).block()
                    logger.info("Execution successful with output : {}", outputs)
                }.doOnSuccess { outputs ->

                    cleanBlueprintExecution(requestId, blueprintExecuteLocation).block()
                }.subscribe()

            } catch (e: Exception) {
                logger.error("failed in execute", e)
                status = Status(code = "400", message = "Failure", errorMessage = e.message)
            } finally {
                // Do Nothing
            }
            ExecutionResponse(header = header, status = status, blueprintIdentifier = blueprintIdentifier)
        }.toMono()
    }

    private fun prepareExecution(executionRequest: ExecutionRequest): Mono<File> {
        val header: Header = executionRequest.header
        val blueprintName = executionRequest.blueprintIdentifier.blueprintName
        val blueprintVersion = executionRequest.blueprintIdentifier.blueprintVersion
        val requestId = header.requestId

        val executionPath = controllerProperty.executionPath.plus(File.separator).plus(requestId)

        logger.info("Blueprint downloading type ({}) to execution path ({})", controllerProperty.retrieveType, controllerProperty.executionPath)

        return blueprintCatalogDBService.prepareBlueprint(blueprintName, blueprintVersion, executionPath)
    }


    fun execute(requestId: String, csarBasePath: String, workflowName: String,
                payload: MutableMap<String, Any>): Mono<MutableMap<String, Any>> {

        logger.info("received ModelProcessExecutor request id {}, csar location {} and workflow {}", requestId, csarBasePath, workflowName)

        val bluePrintContext: BluePrintContext = BluePrintMetadataUtils.getBluePrintContext(csarBasePath)

        val workflowName: String = getWorkflowName(bluePrintContext, workflowName)

        val dataNode = JacksonUtils.jsonNodeFromObject(payload)

        val context: MutableMap<String, Any> = hashMapOf()
        context[com.brvith.framework.core.BluePrintConstants.PROPERTY_BLUEPRINT_PROCESS_ID] = requestId
        context[com.brvith.framework.core.BluePrintConstants.PROPERTY_BLUEPRINT_INPUTS_DATA] = dataNode
        context[com.brvith.framework.core.BluePrintConstants.PROPERTY_BLUEPRINT_CONTEXT] = bluePrintContext
        context[com.brvith.framework.core.BluePrintConstants.PROPERTY_BLUEPRINT_BASE_PATH] = csarBasePath

        // Include Input Data in Proper format
        BluePrintRuntimeUtils.assignInputs(bluePrintContext, dataNode, context)

        logger.info("Executing Plan {} with inputs {}", workflowName, context)

        return blueprintStateMachineService.execute(requestId, workflowName, context)

    }

    fun getWorkflowName(bluePrintContext: BluePrintContext, workflowName: String): String {
        bluePrintContext.workflowByName(workflowName)
                ?: throw com.brvith.framework.core.BlueprintProcessException(String.format("No process name found for the action name %s", workflowName))
        return CaseFormat.LOWER_HYPHEN.to(CaseFormat.UPPER_CAMEL, workflowName)
    }

    fun cleanBlueprintExecution(requestId: String, blueprintExecuteLocation: String): Mono<MutableList<String>> {

        return blueprintStateMachineService.unDeploy(requestId)
                .doOnSuccess { undeplyedBpmns ->
                    logger.info("Undeployed  Plan ids {} successfully for request id {}", undeplyedBpmns, requestId)
                }.doOnSuccess {
                    FileUtils.forceDeleteOnExit(File(blueprintExecuteLocation))
                    logger.info("Cleaned execution blueprint location {}", blueprintExecuteLocation)
                }.doOnError { e ->
                    logger.error("Failed to clean executed blueprint")
                }
    }

}