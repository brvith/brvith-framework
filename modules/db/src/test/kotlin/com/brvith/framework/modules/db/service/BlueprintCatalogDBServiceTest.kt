package com.brvith.framework.modules.db.service

import com.brvith.framework.modules.db.DatabaseAutoConfiguration
import com.brvith.framework.modules.db.domain.BlueprintCatalog
import com.brvith.framework.modules.db.repository.BlueprintCatalogRepository
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.mongodb.core.ReactiveMongoOperations
import org.springframework.test.context.junit4.SpringRunner
import reactor.test.StepVerifier
import java.util.*


@RunWith(SpringRunner::class)
@SpringBootTest(classes = arrayOf(EmbeddedMongoAutoConfiguration::class, DatabaseAutoConfiguration::class)
        //,properties = arrayOf("orchestrator.blueprint.initial-load-path=./test-classes/load")
)
class BlueprintCatalogDBServiceTest {
    var logger: Logger = LoggerFactory.getLogger(this::class.java)

    @Autowired
    lateinit var blueprintCatalogDBService: BlueprintCatalogDBService

    @Autowired
    lateinit var operations: ReactiveMongoOperations

    @Autowired
    lateinit var blueprintCatalogRepository: BlueprintCatalogRepository


    @Before
    fun before() {
        operations.collectionExists(BlueprintCatalog::class.java).flatMap { exists ->
            logger.info("Removing Successfully {}", exists)
            operations.dropCollection(BlueprintCatalog::class.java)
        }.block()
        logger.info("Saved Successfully {}")
    }

    @Test
    fun testSaveBlueprintCatalog(): Unit {
        val catalog = BlueprintCatalog(name = "sample", version = "1.0.0", updatedBy = "xx@xx.com",
                userGroups = arrayListOf("ADMIN"),
                artifactId = UUID.randomUUID().toString(),
                tags = "xxx,yyy",
                id = UUID.randomUUID().toString())

        StepVerifier.create(blueprintCatalogRepository.save(catalog))
                .expectNextCount(1)
                .verifyComplete()

    }

    @Test
    fun testUploadCSAR() {
        val fileName = javaClass.classLoader.getResource(".").path + "/load/csar.zip"
        val executionPath = javaClass.classLoader.getResource(".").path + "/load/execution/1234"

        logger.info("***********  deployBlueprint  {} **********")
        blueprintCatalogDBService.deployBlueprint(fileName, executionPath).doOnNext {

            logger.info("***********  prepareBlueprint **********")

            blueprintCatalogDBService.prepareBlueprint("baseconfiguration",
                    "1.0.0", executionPath)
        }.block()
    }

    // @Test
    fun testDownloadNExtractBlueprintFromDB() {
        logger.info("***********  prepareBlueprint **********")
        val executionPath = javaClass.classLoader.getResource(".").path + "/load/execution/1234"

    }


}

