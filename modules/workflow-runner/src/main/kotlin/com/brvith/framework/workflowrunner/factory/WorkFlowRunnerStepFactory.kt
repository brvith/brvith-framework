package com.brvith.framework.workflowrunner.factory

import com.brvith.framework.workflowrunner.WorkFlowRunnerTaskProperties
import com.brvith.framework.workflowrunner.function.WorkFlowStepFunction
import org.springframework.batch.core.Step
import org.springframework.batch.core.StepExecutionListener
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Isolation
import org.springframework.transaction.interceptor.DefaultTransactionAttribute
import org.springframework.transaction.interceptor.TransactionAttribute
import org.springframework.util.Assert


@Component
class WorkFlowRunnerStepFactory {

    @Autowired
    private lateinit var stepBuilderFactory: StepBuilderFactory

    @Autowired
    private lateinit var workFlowRunnerTaskProperties: WorkFlowRunnerTaskProperties

    @Autowired
    private lateinit var context: ApplicationContext

    @Autowired
    private lateinit var workFlowStepExecutionListener: StepExecutionListener

    private val transactionAttribute: TransactionAttribute
        get() {
            val defaultTransactionAttribute = DefaultTransactionAttribute()
            defaultTransactionAttribute.isolationLevel = Isolation.READ_COMMITTED.value()
            return defaultTransactionAttribute
        }

    @Throws(Exception::class)
    fun getWorkFlowStep(taskName: String): Step {

        Assert.notNull(workFlowRunnerTaskProperties,
                "workFlowRunnerTaskProperties must not be null")
        Assert.hasText(taskName, "taskName must not be empty nor null")

        //val workFlowStepTasklet = WorkFlowStepFunction()

        val workFlowStepTasklet = context.getBean(WorkFlowStepFunction::class.java)
        workFlowStepTasklet.setTaskName(taskName)

        val stepName = taskName

        return this.stepBuilderFactory.get(stepName)
                .tasklet(workFlowStepTasklet)
                .transactionAttribute(transactionAttribute)
                .listener(this.workFlowStepExecutionListener)
                .build()
    }
}