package com.brvith.framework.core.factories


import com.brvith.framework.core.BluePrintException
import com.brvith.framework.core.service.BluePrintContext


interface BluePrintSpecService {

    @Throws(BluePrintException::class)
    fun generateSpec(bluePrintContext: BluePrintContext): String

}


object BluePrintSpecFactory {

    var bluePrintSpecServices: MutableMap<String, BluePrintSpecService> = HashMap()

    fun register(key: String, bluePrintSpecService: BluePrintSpecService) {
        bluePrintSpecServices.put(key, bluePrintSpecService)
    }

    fun instance(key: String): BluePrintSpecService? {
        return bluePrintSpecServices.get(key)
    }

}