package com.brvith.framework.adaptors.git.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.stereotype.Component

@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "configurator.adaptors.git")
class GitAdaptorProperty {
    lateinit var targetDirectory: String
    lateinit var repositoryUrl: String
    lateinit var username: String
    lateinit var password: String
    lateinit var branch: String
}