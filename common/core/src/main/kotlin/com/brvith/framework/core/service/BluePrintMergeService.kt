package com.brvith.framework.core.service

object BluePrintMergeService {

    fun merge(parentServiceTemplate: com.brvith.framework.core.ServiceTemplate, toMerge: com.brvith.framework.core.ServiceTemplate, removeImports : Boolean? = true): com.brvith.framework.core.ServiceTemplate {
            if(removeImports!!){
                parentServiceTemplate.imports = null
                toMerge.imports = null
            }

            toMerge.metadata?.let {
                parentServiceTemplate.metadata = parentServiceTemplate.metadata ?: hashMapOf()
                parentServiceTemplate.metadata?.putAll(toMerge.metadata as HashMap)
            }

            toMerge.dslDefinitions?.let {
                parentServiceTemplate.dslDefinitions = parentServiceTemplate.dslDefinitions ?: hashMapOf()
                parentServiceTemplate.dslDefinitions?.putAll(toMerge.dslDefinitions as HashMap)
            }

            toMerge.dataTypes?.let {
                parentServiceTemplate.dataTypes = parentServiceTemplate.dataTypes ?: hashMapOf()
                parentServiceTemplate.dataTypes?.putAll(toMerge.dataTypes as HashMap)
            }

            toMerge.nodeTypes?.let {
                parentServiceTemplate.nodeTypes = parentServiceTemplate.nodeTypes ?: hashMapOf()
                parentServiceTemplate.nodeTypes?.putAll(toMerge.nodeTypes as HashMap)
            }

            toMerge.artifactTypes?.let {
                parentServiceTemplate.artifactTypes = parentServiceTemplate.artifactTypes ?: hashMapOf()
                parentServiceTemplate.artifactTypes?.putAll(toMerge.artifactTypes as HashMap)
            }

            toMerge.repositories?.let {
                parentServiceTemplate.repositories = parentServiceTemplate.repositories ?: hashMapOf()
                parentServiceTemplate.repositories?.putAll(toMerge.repositories as HashMap)
            }

            parentServiceTemplate.topologyTemplate = parentServiceTemplate.topologyTemplate ?: com.brvith.framework.core.TopologyTemplate()

            toMerge.topologyTemplate?.inputs?.let {
                parentServiceTemplate.topologyTemplate?.inputs = parentServiceTemplate.topologyTemplate?.inputs ?: hashMapOf()
                parentServiceTemplate.topologyTemplate?.inputs?.putAll(parentServiceTemplate.topologyTemplate?.inputs as HashMap)
            }

            toMerge.topologyTemplate?.nodeTemplates?.let {
                parentServiceTemplate.topologyTemplate?.nodeTemplates = parentServiceTemplate.topologyTemplate?.nodeTemplates ?: hashMapOf()
                parentServiceTemplate.topologyTemplate?.nodeTemplates?.putAll(parentServiceTemplate.topologyTemplate?.nodeTemplates as HashMap)
            }

            toMerge.topologyTemplate?.relationshipTemplates?.let {
                parentServiceTemplate.topologyTemplate?.relationshipTemplates = parentServiceTemplate.topologyTemplate?.relationshipTemplates ?: hashMapOf()
                parentServiceTemplate.topologyTemplate?.relationshipTemplates?.putAll(parentServiceTemplate.topologyTemplate?.relationshipTemplates as HashMap)
            }

            toMerge.topologyTemplate?.policies?.let {
                parentServiceTemplate.topologyTemplate?.policies = parentServiceTemplate.topologyTemplate?.policies ?: hashMapOf()
                parentServiceTemplate.topologyTemplate?.policies?.putAll(parentServiceTemplate.topologyTemplate?.policies as HashMap)
            }

            toMerge.topologyTemplate?.workflows?.let {
                parentServiceTemplate.topologyTemplate?.workflows = parentServiceTemplate.topologyTemplate?.workflows ?: hashMapOf()
                parentServiceTemplate.topologyTemplate?.workflows?.putAll(parentServiceTemplate.topologyTemplate?.workflows as HashMap)
            }

        return parentServiceTemplate
    }
}