package com.brvith.framework.executor.service

import com.brvith.framework.core.checkNotEmptyNThrow
import com.brvith.framework.core.format
import com.brvith.framework.core.interfaces.ComponentNode
import com.brvith.framework.script.executor.ScriptExecutorFactory
import com.brvith.framework.script.executor.ScriptExecutorType
import com.brvith.tosca.model.service.BluePrintRuntimeService
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class JythonComponentExecutorService {
    val logger: Logger = LoggerFactory.getLogger(this::class.toString())

    fun getComponentNode(nodeTemplateName: String, componentName: String, nodeType: com.brvith.framework.core.NodeType,
                         nodeTemplate: com.brvith.framework.core.NodeTemplate, operationAssignment: com.brvith.framework.core.OperationAssignment,
                         bluePrintRuntimeService: BluePrintRuntimeService,
                         context: MutableMap<String, Any>): ComponentNode {

        val executePath: String = context[com.brvith.framework.core.BluePrintConstants.PROPERTY_BLUEPRINT_BASE_PATH] as? String
                ?: throw com.brvith.framework.core.BlueprintProcessException(format("python execute path is missing for node template ({})"
                        , nodeTemplateName))

        val artifactName: String = operationAssignment.implementation?.primary
                ?: throw com.brvith.framework.core.BlueprintProcessException(format("missing primary field to get artifact name for node template ({})"
                        , nodeTemplateName))
        val content: String? = bluePrintRuntimeService.resolveNodeTemplateArtifact(nodeTemplateName, artifactName)

        checkNotEmptyNThrow( content,format("artifact ({}) content is empty", artifactName))

        val pythonPath: MutableList<String> = operationAssignment.implementation?.dependencies ?: arrayListOf()

        val properties: MutableMap<String, Any> = hashMapOf()
        properties.put("logger", logger)

        val scriptExecutorService = ScriptExecutorFactory.instance(ScriptExecutorType.PYTHON)
        return scriptExecutorService.getComponent(executePath, pythonPath,
                content, componentName, properties)
    }
}