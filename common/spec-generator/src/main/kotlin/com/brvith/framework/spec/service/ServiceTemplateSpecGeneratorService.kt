package com.brvith.framework.spec.service

import com.brvith.framework.core.service.BluePrintContext
import com.brvith.framework.spec.utils.SpecGeneratorUtils
import com.google.common.io.Resources
import io.swagger.models.*
import io.swagger.models.parameters.BodyParameter
import io.swagger.models.parameters.Parameter
import io.swagger.util.Json
import org.slf4j.Logger
import org.slf4j.LoggerFactory


open class ServiceTemplateSpecGeneratorService : AbstractSpecGeneratorService() {

    private val logger: Logger = LoggerFactory.getLogger(this::class.toString())

    override fun generate(bluePrintContext: BluePrintContext,
                          context : MutableMap<String, Any>): String {

        init(bluePrintContext)

        var swaggerContent = ""
        val swagger = Swagger().info(generateInfo())
        swagger.paths = generatePaths()
        swagger.definitions = generateDefinition()
        swaggerContent = Json.pretty(swagger)
        return swaggerContent
    }

    fun generatePaths(): MutableMap<String, Path> {
        var paths: MutableMap<String, Path> = hashMapOf()
        val path = Path()
        val post = Operation()
        post.operationId = "execute"
        post.consumes = arrayListOf("application/json", "application/xml")
        post.produces = arrayListOf("application/json", "application/xml")

        val parameters: MutableList<Parameter> = arrayListOf()
        val request = BodyParameter().schema(RefModel("#/definitions/request"))
        request.required = true
        request.name = "request"
        parameters.add(request)
        post.parameters = parameters

        var responses: MutableMap<String, Response> = hashMapOf()
        val response = Response().description("Success")
        responses.put("200", response)

        val failureResponse = Response().description("Failure")
        responses.put("400", failureResponse)
        post.responses = responses

        path.post = post
        paths.put("/blueprint/execute", path)
        return paths
    }

    fun generateDefinition(): MutableMap<String, Model> {

        val models: MutableMap<String, Model> = hashMapOf()

        val url = Resources.getResource("templates/servicetemplate-input-pd.json")
        populateDefaultModels(models, url)

        val inputModel = ModelImpl()
        inputModel.title = "data"

        val inputs = serviceTemplate().topologyTemplate?.inputs

        inputs?.forEach { (propertyName, property) ->
            val defProperty = SpecGeneratorUtils.getPropery(propertyName, property)
            inputModel.property(propertyName, defProperty)
        }
        models.put("data", inputModel)

        val dataTypes = serviceTemplate().dataTypes

        // Populate Definitions for DataTypes
        populateModelsFromDataTypes(dataTypes, models)

        return models
    }
}
