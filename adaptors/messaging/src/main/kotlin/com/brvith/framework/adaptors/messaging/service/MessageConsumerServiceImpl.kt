package com.brvith.framework.adaptors.messaging.service

import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.IntegerDeserializer
import org.apache.kafka.common.serialization.StringDeserializer
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.kafka.receiver.KafkaReceiver
import reactor.kafka.receiver.ReceiverOptions
import reactor.kafka.receiver.ReceiverRecord
import java.util.*

@Service("defaultMessageConsumer")
class DefaultMessageConsumerService : com.brvith.framework.adaptors.messaging.MessageConsumerService {

    val logger: Logger = LoggerFactory.getLogger(this::class.toString())
    var properties: MutableMap<String, String> = hashMapOf()

    override fun setConsumerProperties(properties: MutableMap<String, String>) {
        this.properties = properties
        logger.info("Selector properties : {}", this.properties)
    }


    fun getReceiver():  Flux<ReceiverRecord<Integer, String>>  {

        val props :MutableMap<String, Any> = hashMapOf()
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, properties[com.brvith.framework.adaptors.messaging.MessagingConstants.BOOTSTRAP_SERVERS_CONFIG] as String)
        props.put(ConsumerConfig.CLIENT_ID_CONFIG, properties[com.brvith.framework.adaptors.messaging.MessagingConstants.CLIENT_ID_CONFIG] as String)
        props.put(ConsumerConfig.GROUP_ID_CONFIG, properties[com.brvith.framework.adaptors.messaging.MessagingConstants.GROUP_ID_CONFIG] as String)
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, IntegerDeserializer::class.java)
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer::class.java)
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")
        val receiverOptions : ReceiverOptions<Integer, String>  = ReceiverOptions.create<Integer, String>(props)


        val topic : String = properties[com.brvith.framework.adaptors.messaging.MessagingConstants.TOPIC]!!
        val options = receiverOptions.subscription(Collections.singleton(topic))
                .addAssignListener({ partitions -> logger.debug("onPartitionsAssigned {}", partitions) })
                .addRevokeListener({ partitions -> logger.debug("onPartitionsRevoked {}", partitions) })
        val kafkaFlux : Flux<ReceiverRecord<Integer, String>> = KafkaReceiver.create<Integer, String>(options).receive()

        return kafkaFlux
    }

}