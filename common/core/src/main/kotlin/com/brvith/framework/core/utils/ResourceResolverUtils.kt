package com.brvith.framework.core.utils

import com.brvith.framework.core.checkNotEmpty
import org.apache.commons.io.FileUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.net.URL
import java.nio.charset.Charset

object ResourceResolverUtils {
    private val logger: Logger = LoggerFactory.getLogger(this::class.toString())

    fun getFileContent(filename : String, basePath : String?): String {
        logger.trace("file ({}), basePath ({}) ", filename, basePath)
        try{
            var resolvedFileName : String = filename
            if(filename.startsWith("http", true)
                    || filename.startsWith("https", true)){
                val givenUrl : String = URL(filename).toString()
                val systemUrl : String = File(".").toURI().toURL().toString()
                logger.trace("givenUrl ({}), systemUrl ({}) ", givenUrl, systemUrl)
                if(givenUrl.startsWith(systemUrl)){

                }
            }else{
                if(!filename.startsWith("/")){
                    if (checkNotEmpty(basePath)) {
                        resolvedFileName = basePath.plus(File.separator).plus(filename)
                    }else{
                        resolvedFileName = javaClass.classLoader.getResource(".").path.plus(filename)
                    }
                }
            }
            return FileUtils.readFileToString(File(resolvedFileName), Charset.defaultCharset())
        }catch (e : Exception){
            throw com.brvith.framework.core.BluePrintException(e, "failed to file (%s), basePath (%s) ", filename, basePath)
        }
    }
}