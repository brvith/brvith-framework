package com.brvith.framework.core.serializer

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.NullNode
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.databind.node.TextNode
import org.slf4j.LoggerFactory
import java.io.IOException


open class YamlPropertyDefinitionSerializer : JsonSerializer<JsonNode>() {

    private val logger = LoggerFactory.getLogger(this.javaClass)

    @Throws(IOException::class)
    override fun serialize(value: JsonNode, jgen: JsonGenerator, provider: SerializerProvider) {
        logger.info(value.javaClass.toString() + " type of value " + value  )

        when (value){
            is ObjectNode -> {
//                jgen.writeStartObject()
//                value.fields().forEach { entity ->
//                    jgen.writeStringField(entity.key, entity.value.toString())
//                    //jgen.writeObjectField( entity.key, entity.value)
//                }
//                jgen.writeEndObject()
                jgen.writeString(value.toString())
            }
            is ArrayNode -> {
                jgen.writeString(value.toString())
            }
            is TextNode -> {
                jgen.writeString(value.textValue())
            }
            is NullNode ->{
                jgen.writeNull()
            }
            else -> {
            }
        }
    }
}
