package com.brvith.framework.adaptors.git.service

import org.apache.commons.io.FileUtils
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.api.MergeResult
import org.eclipse.jgit.api.TransportCommand
import org.eclipse.jgit.api.errors.GitAPIException
import org.eclipse.jgit.errors.RepositoryNotFoundException
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Path

class GitService {
    companion object {
        var logger: Logger = LoggerFactory.getLogger(com.brvith.framework.adaptors.git.service.GitService::class.java)

        /**
         *
         * @param targetDirectory The root directory that will contains the localDirectory in which to checkout the archives.
         * @param repositoryUrl The url of the repository to checkout or clone.
         * @param username The username to use for the repository connection.
         * @param password The password to use for the repository connection.
         * @param branch The branch to checkout or clone.
         * @param localDirectory The path, relative to targetDirectory, in which to checkout or clone the git directory.
         */
        fun cloneOrCheckout(targetDirectory: Path, repositoryUrl: String, username: String, password: String, branch: String, localDirectory: String): Git {
            try {
                Files.createDirectories(targetDirectory)
                val targetPath = targetDirectory.resolve(localDirectory)
                var repository: Git
                if (Files.exists(targetPath)) {
                    try {
                        repository = Git.open(targetPath.toFile())
                        com.brvith.framework.adaptors.git.service.GitService.Companion.checkoutRepository(repository, branch, username, password)
                    } catch (e: RepositoryNotFoundException) {
                        FileUtils.forceDelete(targetPath.toFile())
                        repository = com.brvith.framework.adaptors.git.service.GitService.Companion.cloneRepository(repositoryUrl, username, password, branch, targetPath)
                    }
                } else {
                    Files.createDirectories(targetPath)
                    repository = com.brvith.framework.adaptors.git.service.GitService.Companion.cloneRepository(repositoryUrl, username, password, branch, targetPath)
                }
                return repository
            } catch (e: IOException) {
                com.brvith.framework.adaptors.git.service.GitService.Companion.logger.error("Error while creating target directory ", e)
                throw com.brvith.framework.adaptors.git.GitException("Error while creating target directory ", e)
            }

        }

        private fun checkoutRepository(repository: Git, branch: String, username: String, password: String) {
            try {
                var checkoutCommand = repository.checkout()
                checkoutCommand.setName(branch)
                val ref = checkoutCommand.call()
                if (branch == ref.name) {
                    // failed to checkout the branch, let's fetch it
                    val fetchCommand = repository.fetch()
                    com.brvith.framework.adaptors.git.service.GitService.Companion.setCredentials(fetchCommand, username, password)
                    fetchCommand.call()
                    checkoutCommand = repository.checkout()
                    checkoutCommand.setName(branch)
                    checkoutCommand.call()
                }
            } catch (e: GitAPIException) {
                com.brvith.framework.adaptors.git.service.GitService.Companion.logger.error("Failed to pull git repository ", e)
                throw com.brvith.framework.adaptors.git.GitException("Failed to pull git repository", e)
            }

        }

        @Throws(IOException::class)
        private fun cloneRepository(url: String, username: String, password: String, branch: String, targetPath: Path): Git {
            com.brvith.framework.adaptors.git.service.GitService.Companion.logger.debug("Cloning from [{}] branch [{}] to [{}]", url, branch, targetPath.toString())
            val result: Git
            try {
                val cloneCommand = Git.cloneRepository().setURI(url).setBranch(branch).setDirectory(targetPath.toFile())
                com.brvith.framework.adaptors.git.service.GitService.Companion.setCredentials(cloneCommand, username, password)
                result = cloneCommand.call()
                com.brvith.framework.adaptors.git.service.GitService.Companion.logger.debug("Cloned repository to [{}]: ", result.repository.directory)
                return result
            } catch (e: GitAPIException) {
                FileUtils.forceDelete(targetPath.toFile())
                com.brvith.framework.adaptors.git.service.GitService.Companion.logger.error("Failed to clone git repository.", e)
                throw com.brvith.framework.adaptors.git.GitException("Failed to clone git repository", e)
            }
        }

        /**
         * Trigger a Pull Request on an existing repository.
         *
         * @param repository The git repository to pull.
         * @param username The username to use for the repository connection.
         * @param password The password to use for the repository connection.
         * @return True if the pull request has updated the data, false if the repository was already up to date.
         */
        fun pull(repository: Git, username: String, password: String): Boolean {
            try {
                val pullCommand = repository.pull()
                com.brvith.framework.adaptors.git.service.GitService.Companion.setCredentials(pullCommand, username, password)
                val result = pullCommand.call()

                val mergeResult = result.mergeResult
                return !(mergeResult != null && MergeResult.MergeStatus.ALREADY_UP_TO_DATE === mergeResult.mergeStatus)
            } catch (e: GitAPIException) {
                com.brvith.framework.adaptors.git.service.GitService.Companion.logger.error("Failed to pull git repository ", e)
                throw com.brvith.framework.adaptors.git.GitException("Failed to pull git repository", e)
            }
        }

        /**
         * Get the hash of the last commit on the current branch of the given repository.
         *
         * @param git The repository from which to get the last commit hash.
         * @return The hash of the last commit.
         */
        fun getLastHash(git: Git): String? {
            try {
                val revCommitIterator = git.log().setMaxCount(1).call().iterator()
                if (revCommitIterator.hasNext()) {
                    return revCommitIterator.next().name
                }
            } catch (e: GitAPIException) {
                com.brvith.framework.adaptors.git.service.GitService.Companion.logger.error("Failed to log git repository ", e)
                throw com.brvith.framework.adaptors.git.GitException("Failed to log git repository", e)
            }

            return null
        }

        private fun setCredentials(command: TransportCommand<*, *>, username: String?, password: String) {
            if (username != null) {
                command.setCredentialsProvider(UsernamePasswordCredentialsProvider(username, password))
            }
        }
    }
}