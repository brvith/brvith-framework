package com.brvith.framework.modules.db

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.ComponentScan

@ComponentScan
@ConditionalOnProperty(prefix = "orchestrator.modules.db", name = arrayOf("enabled"), havingValue = "true", matchIfMissing = false)
//@EnableConfigurationProperties(RestAdaptorProperty::class)
open class DatabaseAutoConfiguration