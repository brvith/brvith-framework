package com.brvith.framework.adaptors.messaging.service

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import kotlin.test.assertNotNull

@RunWith(SpringRunner::class)
@SpringBootTest(classes = arrayOf(com.brvith.framework.adaptors.messaging.MessagingAutoConfiguration::class))
class MessageConsumerServiceImplTest {

    @Autowired
    lateinit var messageConsumerFactory : com.brvith.framework.adaptors.messaging.MessageConsumerFactory

    @Test
    fun testSelectorProperties(): Unit {

        val messageConsumerService = messageConsumerFactory.getInstance("selector2")
        assertNotNull(messageConsumerService, "Failed to get Message Consumenr for selector2")
        //defaultMessageProducer.getSender()
        
    }

}