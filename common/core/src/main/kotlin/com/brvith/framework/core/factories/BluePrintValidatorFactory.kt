package com.brvith.framework.core.factories

import com.brvith.framework.core.BluePrintException

interface BluePrintValidatorService {

    @Throws(com.brvith.framework.core.BluePrintException::class)
    fun validateBlueprint()
}


object BluePrintValidatorFactory {

    var bluePrintValidatorServices: MutableMap<String, BluePrintValidatorService> = HashMap()

    fun register(key:String, bluePrintValidatorService:BluePrintValidatorService){
        bluePrintValidatorServices.put(key, bluePrintValidatorService)
    }

    fun instance(key : String) : BluePrintValidatorService?{
        return bluePrintValidatorServices.get(key)
    }

}