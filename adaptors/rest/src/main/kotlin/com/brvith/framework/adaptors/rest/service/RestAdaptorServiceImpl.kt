package com.brvith.framework.adaptors.rest.service

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.*
import reactor.core.publisher.Mono


abstract class AbstractRestAdaptorService: com.brvith.framework.adaptors.rest.RestAdaptorService {

    var logger: Logger = LoggerFactory.getLogger(this::class.java)

    var properties: MutableMap<String, String> = hashMapOf()

    override fun setRestAdaptorProperties(properties: MutableMap<String, String>) {
        this. properties =  properties
    }

    abstract override fun getWebClient(selectorName: String): WebClient

    fun logRequest(): ExchangeFilterFunction {
        return ExchangeFilterFunction.ofRequestProcessor { clientRequest ->
            logger.info("Request: method ({}), url ({}), headers ({})", clientRequest.method(), clientRequest.url(), clientRequest.headers())
            Mono.just<ClientRequest>(clientRequest)
        }
    }

    fun logResponse(): ExchangeFilterFunction {
        return ExchangeFilterFunction.ofResponseProcessor { clientResponse ->
            logger.info("Response status {}, headers ()", clientResponse.statusCode(), clientResponse.headers())
            Mono.just<ClientResponse>(clientResponse)
        }
    }

}

@Service("default")
class DefaultRestAdaptorService  : com.brvith.framework.adaptors.rest.service.AbstractRestAdaptorService(){

    override fun getWebClient(selectorName: String): WebClient {
        val type: String = properties.get(com.brvith.framework.adaptors.rest.RestAdaptorConstants.SERVICE_TYPE_PROPERTY) ?: throw com.brvith.framework.adaptors.rest.RestAdaptorException("type is missing")
        val baseUrl: String = properties.get(com.brvith.framework.adaptors.rest.RestAdaptorConstants.SERVICE_BASEURL_PROPERTY) ?: throw com.brvith.framework.adaptors.rest.RestAdaptorException("baseUrl is missing")

        var client: WebClient = WebClient.builder().baseUrl(baseUrl)
                .filter(logRequest())
                .filter(logResponse())
                .build()
        return client
    }
}

@Service("baseAuth")
class BaseAuthRestAdaptorService  : com.brvith.framework.adaptors.rest.service.AbstractRestAdaptorService(){

    override fun getWebClient(selectorName: String): WebClient {
        val type: String = properties.get(com.brvith.framework.adaptors.rest.RestAdaptorConstants.SERVICE_TYPE_PROPERTY) ?: throw com.brvith.framework.adaptors.rest.RestAdaptorException("type is missing")
        val baseUrl: String = properties.get(com.brvith.framework.adaptors.rest.RestAdaptorConstants.SERVICE_BASEURL_PROPERTY) ?: throw com.brvith.framework.adaptors.rest.RestAdaptorException("baseUrl is missing")
        val userName: String = properties.get(com.brvith.framework.adaptors.rest.RestAdaptorConstants.SERVICE_USER_PROPERTY) ?: throw com.brvith.framework.adaptors.rest.RestAdaptorException("type is missing")
        val token: String = properties.get(com.brvith.framework.adaptors.rest.RestAdaptorConstants.SERVICE_TOKEN_PROPERTY) ?: throw com.brvith.framework.adaptors.rest.RestAdaptorException("baseUrl is missing")

        var client: WebClient = WebClient.builder().baseUrl(baseUrl)
                .filter(ExchangeFilterFunctions.basicAuthentication(userName, token))
                .filter(logRequest())
                .filter(logResponse())
                .build()
        return client
    }
}