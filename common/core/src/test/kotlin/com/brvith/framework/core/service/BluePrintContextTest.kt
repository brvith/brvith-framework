package com.brvith.framework.core.service

import com.brvith.framework.core.factories.BluePrintParserFactory
import com.brvith.framework.core.utils.JacksonUtils
import org.apache.commons.io.FileUtils
import org.junit.Before
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.nio.charset.Charset
import kotlin.test.assertNotNull

class BluePrintContextTest {

    private val logger: Logger = LoggerFactory.getLogger(this::class.toString())

    lateinit var bluePrintContext: BluePrintContext

    @Before
    fun setUp(): Unit {

        val basepath = javaClass.classLoader.getResource(".").path + "/csar"

        bluePrintContext = BluePrintParserFactory.instance(com.brvith.framework.core.BluePrintConstants.TYPE_DEFAULT)!!
                .readBlueprintFile("Definitions/activation-blueprint.json", basepath)
        assertNotNull(bluePrintContext, "Failed to populate Blueprint context")
    }

    @Test
    fun testBluePrintContextFromContent() {
        val fileName = javaClass.classLoader.getResource(".").path + "/csar/Definitions/activation-blueprint.json"
        val content : String = FileUtils.readFileToString(File(fileName), Charset.defaultCharset())
        val bpContext  = BluePrintParserFactory.instance(com.brvith.framework.core.BluePrintConstants.TYPE_DEFAULT)!!
                .readBlueprint(content)
        assertNotNull(bpContext, "Failed to get blueprint content")
        assertNotNull(bpContext.serviceTemplate, "Failed to get blueprint content's service template")
    }

    @Test
    fun testChainedProperty() {
        val nodeType = bluePrintContext.nodeTypeChained("component-resource-assignment")
        assertNotNull(nodeType, "Failed to get chained node type")
        logger.trace("Properties {}", JacksonUtils.getJson(nodeType, true))
    }


}
