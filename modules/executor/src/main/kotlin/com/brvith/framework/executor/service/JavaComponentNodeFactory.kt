package com.brvith.framework.executor.service

import com.brvith.framework.core.interfaces.ComponentNode
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.stereotype.Service


@Service
class JavaComponentNodeFactory : ApplicationContextAware {
    val logger: Logger = LoggerFactory.getLogger(JavaComponentNodeFactory::class.toString())
    var componentNodes: MutableMap<String, ComponentNode> = hashMapOf()

    fun getInstance(instanceName: String): ComponentNode? {
        logger.trace("looking for Component Nodes : {}", instanceName)
        return componentNodes.get(instanceName)
    }

    fun injectInstance(instanceName: String, componentNode: ComponentNode): Unit {
        componentNodes[instanceName] = componentNode
    }

    override fun setApplicationContext(context: ApplicationContext) {
        componentNodes = context.getBeansOfType(ComponentNode::class.java)
        logger.info("Injected Component Nodes : {}", componentNodes)
    }
}