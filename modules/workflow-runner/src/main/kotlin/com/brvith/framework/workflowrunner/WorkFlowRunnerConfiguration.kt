package com.brvith.framework.workflowrunner

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing
import org.springframework.cloud.task.configuration.EnableTask
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.core.task.TaskExecutor
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor


@Configuration
@EnableBatchProcessing
@EnableTask
@ComponentScan
class WorkFlowRunnerConfiguration {

    @Bean
    fun workFlowTaskExecutor(): TaskExecutor {

        val workFlowRunnerTaskProperties: WorkFlowRunnerTaskProperties = WorkFlowRunnerTaskProperties()

        val taskExecutor = ThreadPoolTaskExecutor()
        taskExecutor.corePoolSize = workFlowRunnerTaskProperties.splitThreadCorePoolSize
        taskExecutor.maxPoolSize = workFlowRunnerTaskProperties.splitThreadMaxPoolSize
        taskExecutor.keepAliveSeconds = workFlowRunnerTaskProperties.splitThreadKeepAliveSeconds
        taskExecutor.setAllowCoreThreadTimeOut(workFlowRunnerTaskProperties.splitThreadAllowCoreThreadTimeout)
        taskExecutor.setQueueCapacity(workFlowRunnerTaskProperties.splitThreadQueueCapacity)
        taskExecutor.setWaitForTasksToCompleteOnShutdown(workFlowRunnerTaskProperties.splitThreadWaitForTasksToCompleteOnShutdown)
        return taskExecutor
    }
}