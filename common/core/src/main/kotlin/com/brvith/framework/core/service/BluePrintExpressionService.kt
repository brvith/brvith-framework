package com.brvith.framework.core.service

import com.brvith.framework.core.format
import com.brvith.framework.core.utils.JacksonUtils
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.ObjectNode
import org.slf4j.Logger
import org.slf4j.LoggerFactory

object BluePrintExpressionService {
    val logger: Logger = LoggerFactory.getLogger(this::class.toString())

    fun getExpressionData(propertyAssignment: Any): com.brvith.framework.core.ExpressionData {
        val propertyAssignmentNode: JsonNode = JacksonUtils.jsonNodeFromObject(propertyAssignment)
        return getExpressionData(propertyAssignmentNode)
    }

    fun getExpressionData(propertyAssignmentNode: JsonNode): com.brvith.framework.core.ExpressionData {
        logger.trace("Assignment Data/Expression : {}", propertyAssignmentNode)
        val expressionData = com.brvith.framework.core.ExpressionData(valueNode = propertyAssignmentNode)
        if (propertyAssignmentNode is ObjectNode) {
            val commands: Set<String> = propertyAssignmentNode.fieldNames().asSequence().toList().intersect(com.brvith.framework.core.BluePrintTypes.validCommands())
            if (commands.isNotEmpty()) {
                expressionData.isExpression = true
                expressionData.command = commands.first()
                expressionData.expressionNode = propertyAssignmentNode

                when (expressionData.command) {
                    com.brvith.framework.core.BluePrintConstants.EXPRESSION_GET_INPUT -> {
                        expressionData.inputExpression = populateInputExpression(propertyAssignmentNode)
                    }
                    com.brvith.framework.core.BluePrintConstants.EXPRESSION_GET_ATTRIBUTE -> {
                        expressionData.attributeExpression = populateAttributeExpression(propertyAssignmentNode)
                    }
                    com.brvith.framework.core.BluePrintConstants.EXPRESSION_GET_PROPERTY -> {
                        expressionData.propertyExpression = populatePropertyExpression(propertyAssignmentNode)
                    }
                    com.brvith.framework.core.BluePrintConstants.EXPRESSION_GET_OPERATION_OUTPUT -> {
                        expressionData.operationOutputExpression = populateOperationOutputExpression(propertyAssignmentNode)
                    }
                    com.brvith.framework.core.BluePrintConstants.EXPRESSION_GET_ARTIFACT -> {
                        expressionData.artifactExpression = populateArtifactExpression(propertyAssignmentNode)
                    }
                }
            }
        }
        return expressionData
    }

    fun populateInputExpression(jsonNode: JsonNode): com.brvith.framework.core.InputExpression {
        return com.brvith.framework.core.InputExpression(propertyName = jsonNode.first().textValue())
    }

    fun populatePropertyExpression(jsonNode: JsonNode): com.brvith.framework.core.PropertyExpression {
        val arrayNode: ArrayNode = jsonNode.first() as ArrayNode
        check(arrayNode.size() >= 2) {
            throw com.brvith.framework.core.BluePrintException(format("missing property expression, " +
                    "it should be [ <modelable_entity_name>, <optional_req_or_cap_name>, <property_name>, " +
                    "<nested_property_name_or_index_1>, ..., <nested_property_name_or_index_n> ] , but present {}", jsonNode))
        }
        var reqOrCapEntityName: String? = null
        var propertyName: String = ""
        var subProperty: String? = null
        if (arrayNode.size() == 2) {
            propertyName = arrayNode[1].textValue()
        } else if (arrayNode.size() == 3) {
            reqOrCapEntityName = arrayNode[1].textValue()
            propertyName = arrayNode[2].textValue()
        } else if (arrayNode.size() > 3) {
            reqOrCapEntityName = arrayNode[1].textValue()
            propertyName = arrayNode[2].textValue()
            val propertyPaths: List<String> = arrayNode.filterIndexed { index, obj ->
                index >= 3
            }.map { it.textValue() }
            subProperty = propertyPaths.joinToString("/")
        }

        return com.brvith.framework.core.PropertyExpression(modelableEntityName = arrayNode[0].asText(),
                reqOrCapEntityName = reqOrCapEntityName,
                propertyName = propertyName,
                subPropertyName = subProperty
        )
    }

    fun populateAttributeExpression(jsonNode: JsonNode): com.brvith.framework.core.AttributeExpression {
        val arrayNode: ArrayNode = jsonNode.first() as ArrayNode
        check(arrayNode.size() >= 3) {
            throw com.brvith.framework.core.BluePrintException(format("missing attribute expression, " +
                    "it should be [ <modelable_entity_name>, <optional_req_or_cap_name>, <attribute_name>," +
                    " <nested_attribute_name_or_index_1>, ..., <nested_attribute_name_or_index_n> ] , but present {}", jsonNode))
        }

        var reqOrCapEntityName: String? = null
        var propertyName: String = ""
        var subProperty: String? = null
        if (arrayNode.size() == 2) {
            propertyName = arrayNode[1].textValue()
        } else if (arrayNode.size() == 3) {
            reqOrCapEntityName = arrayNode[1].textValue()
            propertyName = arrayNode[2].textValue()
        } else if (arrayNode.size() > 3) {
            reqOrCapEntityName = arrayNode[1].textValue()
            propertyName = arrayNode[2].textValue()
            val propertyPaths: List<String> = arrayNode.filterIndexed { index, obj ->
                index >= 3
            }.map { it.textValue() }
            subProperty = propertyPaths.joinToString("/")
        }
        return com.brvith.framework.core.AttributeExpression(modelableEntityName = arrayNode[0].asText(),
                reqOrCapEntityName = reqOrCapEntityName,
                attributeName = propertyName,
                subAttributeName = subProperty
        )
    }

    fun populateOperationOutputExpression(jsonNode: JsonNode): com.brvith.framework.core.OperationOutputExpression {
        val arrayNode: ArrayNode = jsonNode.first() as ArrayNode

        check(arrayNode.size() >= 4) {
            throw com.brvith.framework.core.BluePrintException(format("missing operation output expression, " +
                    "it should be (<modelable_entity_name>, <interface_name>, <operation_name>, <output_variable_name>) , but present {}", jsonNode))
        }
        return com.brvith.framework.core.OperationOutputExpression(modelableEntityName = arrayNode[0].asText(),
                interfaceName = arrayNode[1].asText(),
                operationName = arrayNode[2].asText(),
                propertyName = arrayNode[3].asText()
        )
    }


    fun populateArtifactExpression(jsonNode: JsonNode): com.brvith.framework.core.ArtifactExpression {
        val arrayNode: ArrayNode = jsonNode.first() as ArrayNode

        check(arrayNode.size() >= 2) {
            throw com.brvith.framework.core.BluePrintException(format("missing artifact expression, " +
                    "it should be [ <modelable_entity_name>, <artifact_name>, <location>, <remove> ] , but present {}", jsonNode))
        }
        return com.brvith.framework.core.ArtifactExpression(modelableEntityName = arrayNode[0].asText(),
                artifactName = arrayNode[1].asText(),
                location = arrayNode[2]?.asText() ?: "LOCAL_FILE",
                remove = arrayNode[3]?.asBoolean() ?: false
        )
    }
}