package com.brvith.framework.spec.service

import com.brvith.framework.core.factories.BluePrintParserFactory
import com.brvith.framework.spec.SpecGeneratorFactory
import com.brvith.framework.spec.SpecGeneratorType
import org.apache.commons.io.FileUtils
import org.junit.Before
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.nio.charset.Charset
import kotlin.test.assertNotNull

class ServiceTemplateSpecGeneratorServiceTest {

    private val logger: Logger = LoggerFactory.getLogger(this::class.toString())

    @Before
    fun setUp(): Unit {

    }

    @Test
    fun testGeneration(): Unit {

        val basepath = javaClass.classLoader.getResource(".").path + "/activation-blueprint.json"
        val specPath = javaClass.classLoader.getResource(".").path + "/spec.json"
        val content = FileUtils.readFileToString(File(basepath), Charset.defaultCharset())

        val bluePrintContext = BluePrintParserFactory.instance(com.brvith.framework.core.BluePrintConstants.TYPE_DEFAULT)!!
                .readBlueprint(content)
        assertNotNull(bluePrintContext, "Failed to populate Blueprint context")

        val context : MutableMap<String, Any> = hashMapOf()

        val swaggerContent =  SpecGeneratorFactory.instance(SpecGeneratorType.WORKFLOW_INPUTS)
                .generate(bluePrintContext, context)
        assertNotNull(swaggerContent, "Failed to generate swagger content")
        FileUtils.write(File(specPath), swaggerContent,Charset.defaultCharset())
        //logger.debug(swaggerContent)
    }
}