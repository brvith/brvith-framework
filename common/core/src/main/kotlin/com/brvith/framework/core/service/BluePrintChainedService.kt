package com.brvith.framework.core.service

import com.brvith.framework.core.*

class BluePrintChainedService {
    var bpc : BluePrintContext

    constructor(bpc : BluePrintContext){
        this.bpc = bpc
    }

    fun nodeTypeChained(nodeTypeName: String): NodeType {

        var nodeType: NodeType = bpc.nodeTypeByName(nodeTypeName)
        val attributes = hashMapOf<String, AttributeDefinition>()
        val properties = hashMapOf<String, PropertyDefinition>()
        val requirements = hashMapOf<String, RequirementDefinition>()
        val capabilities = hashMapOf<String, CapabilityDefinition>()
        val interfaces = hashMapOf<String, InterfaceDefinition>()
        val artifacts = hashMapOf<String, ArtifactDefinition>()

        recNodeTypesChained(nodeTypeName).forEach { nodeType ->

            val subAttributes =  bpc.nodeTypeByName(nodeType.id!!).attributes
            if (subAttributes != null) {
                attributes.putAll(subAttributes)
            }

            val subProperties =  bpc.nodeTypeByName(nodeType.id!!).properties
            if (subProperties != null) {
                properties.putAll(subProperties)
            }

            val subRequirements =  bpc.nodeTypeByName(nodeType.id!!).requirements
            if (subRequirements != null) {
                requirements.putAll(subRequirements)
            }
            val subCapabilities =  bpc.nodeTypeByName(nodeType.id!!).capabilities
            if (subCapabilities != null) {
                capabilities.putAll(subCapabilities)
            }
            val subInterfaces =  bpc.nodeTypeByName(nodeType.id!!).interfaces
            if (subInterfaces != null) {
                interfaces.putAll(subInterfaces)
            }

            val subArtifacts =  bpc.nodeTypeByName(nodeType.id!!).artifacts
            if (subArtifacts != null) {
                artifacts.putAll(subArtifacts)
            }
        }
        nodeType.attributes = attributes
        nodeType.properties = properties
        nodeType.requirements = requirements
        nodeType.capabilities = capabilities
        nodeType.interfaces = interfaces
        nodeType.artifacts = artifacts
        return nodeType
    }

    fun nodeTypeChainedProperties(nodeTypeName: String): MutableMap<String, PropertyDefinition>? {
        val nodeType =  bpc.nodeTypeByName(nodeTypeName)
        val properties = hashMapOf<String, PropertyDefinition>()

        recNodeTypesChained(nodeTypeName).forEach { nodeType ->
            val subProperties =  bpc.nodeTypeByName(nodeType.id!!).properties
            if (subProperties != null) {
                properties.putAll(subProperties)
            }
        }
        return properties
    }

    private fun recNodeTypesChained(nodeTypeName: String, nodeTypes: MutableList<NodeType>? = arrayListOf()): MutableList<NodeType> {
        val nodeType: NodeType =  bpc.nodeTypeByName(nodeTypeName)
        nodeType.id = nodeTypeName
        val derivedFrom: String = nodeType.derivedFrom
        if (!BluePrintTypes.rootNodeTypes().contains(derivedFrom)) {
            recNodeTypesChained(derivedFrom, nodeTypes)
        }
        nodeTypes!!.add(nodeType)
        return nodeTypes
    }

    private fun recDataTypesChained(dataTypeName: String, dataTypes: MutableList<DataType>? = arrayListOf()): MutableList<DataType> {
        val dataType: DataType =  bpc.dataTypeByName(dataTypeName)!!
        dataType.id = dataTypeName
        val derivedFrom: String = dataType.derivedFrom
        if (!BluePrintTypes.rootDataTypes().contains(derivedFrom)) {
            recDataTypesChained(derivedFrom, dataTypes)
        }
        dataTypes!!.add(dataType)
        return dataTypes
    }

}