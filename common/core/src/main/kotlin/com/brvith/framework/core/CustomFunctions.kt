package com.brvith.framework.core

import org.slf4j.helpers.MessageFormatter
import java.io.File
import java.io.InputStream
import kotlin.reflect.KClass


fun format(message: String, vararg args: Any?): String {
    if (args != null && args.isNotEmpty()) {
        return MessageFormatter.arrayFormat(message, args).message
    }
    return message
}

fun <T : Any> MutableMap<String, *>.getCastOptionalValue(key: String, valueType: KClass<T>): T? {
    if (containsKey(key)) {
        return get(key) as? T
    } else {
        return null
    }
}

fun <T : Any> MutableMap<String, *>.getCastValue(key: String, valueType: KClass<T>): T {
    if (containsKey(key)) {
        return get(key) as T
    } else {
        throw com.brvith.framework.core.BluePrintException("couldn't find the key " + key)
    }
}

fun checkNotEmpty(value: String?): Boolean {
    return value != null && value.isNotEmpty()
}

fun checkNotEmptyNThrow(value: String?, message: String? = value.plus(" is null/empty ")): Boolean {
    val notEmpty = value != null && value.isNotEmpty()
    if (!notEmpty) {
        throw com.brvith.framework.core.BluePrintException(message!!)
    }
    return notEmpty
}

fun <T> nullOrEmpty(collection: Collection<T>?): Boolean {
    return collection == null || collection.isEmpty()
}

fun <T> notNullAndNotEmpty(collection: Collection<T>?): Boolean {
    return collection != null && collection.isNotEmpty()
}

fun InputStream.toFile(path: String): File {
    val file = File(path)
    file.outputStream().use { this.copyTo(it) }
    return file
}






