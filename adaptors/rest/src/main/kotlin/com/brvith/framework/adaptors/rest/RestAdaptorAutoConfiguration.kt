package com.brvith.framework.adaptors.rest

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration


@Configuration
@ComponentScan
@ConditionalOnProperty(prefix = "orchestrator.adaptors.rest", name = arrayOf("enabled"), havingValue = "true", matchIfMissing = false)
@EnableConfigurationProperties(com.brvith.framework.adaptors.rest.RestAdaptorProperty::class)
class RestAdaptorAutoConfiguration