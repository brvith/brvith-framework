package com.brvith.framework.core.service

import com.brvith.framework.core.format
import com.brvith.framework.core.utils.JacksonUtils
import com.fasterxml.jackson.databind.JsonNode
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class BluePrintContext(serviceTemplate: com.brvith.framework.core.ServiceTemplate) {

    private val logger: Logger = LoggerFactory.getLogger(this::class.toString())

    val serviceTemplate: com.brvith.framework.core.ServiceTemplate = serviceTemplate

    val imports: List<com.brvith.framework.core.ImportDefinition>? = serviceTemplate.imports

    val metadata: MutableMap<String, String>? = serviceTemplate.metadata

    val dataTypes: MutableMap<String, com.brvith.framework.core.DataType>? = serviceTemplate.dataTypes

    val inputs: MutableMap<String, com.brvith.framework.core.PropertyDefinition>? = serviceTemplate.topologyTemplate?.inputs

    val workflows: MutableMap<String, com.brvith.framework.core.Workflow>? = serviceTemplate.topologyTemplate?.workflows

    fun blueprintJson(pretty: Boolean = false): String = print("json", pretty)

    fun blueprintYaml(pretty: Boolean = false): String = print("yaml", pretty)

    private fun print(type: String? = "json", pretty: Boolean = false): String {
        if (type.equals("yaml")) {
            return JacksonUtils.getYaml(serviceTemplate, pretty)
        } else {
            return JacksonUtils.getJson(serviceTemplate, pretty)
        }
    }

    // Workflow
    fun workflowByName(name: String): com.brvith.framework.core.Workflow? = workflows?.get(name)

    // Data Type
    fun dataTypeByName(name: String): com.brvith.framework.core.DataType? = dataTypes?.get(name)

    // Artifact Type
    val artifactTypes: MutableMap<String, com.brvith.framework.core.ArtifactType>? = serviceTemplate.artifactTypes

    // Node Type Methods
    val nodeTypes: MutableMap<String, com.brvith.framework.core.NodeType>? = serviceTemplate.nodeTypes

    fun nodeTypeByName(name: String): com.brvith.framework.core.NodeType =
            nodeTypes?.get(name) ?: throw com.brvith.framework.core.BluePrintException(format("Failed to get node type for the name : %s", name))

    fun nodeTypeDerivedFrom(name: String): MutableMap<String, com.brvith.framework.core.NodeType>? {
        return nodeTypes?.filterValues { nodeType -> nodeType.derivedFrom == name }?.toMutableMap()
    }

    fun nodeTypeInterface(nodeTypeName: String, interfaceName: String): com.brvith.framework.core.InterfaceDefinition? {
        return nodeTypeByName(nodeTypeName).interfaces?.values?.first()
    }

    fun nodeTypeInterfaceOperation(nodeTypeName: String, interfaceName: String, operationName: String): com.brvith.framework.core.OperationDefinition? {
        return nodeTypeInterface(nodeTypeName, interfaceName)?.operations?.get(operationName)
    }

    fun interfaceNameForNodeType(nodeTypeName: String): String? {
        return nodeTypeByName(nodeTypeName).interfaces?.keys?.first()
    }

    fun nodeTypeInterfaceOperationInputs(nodeTypeName: String, interfaceName: String, operationName: String): MutableMap<String, com.brvith.framework.core.PropertyDefinition>? {
        return nodeTypeInterfaceOperation(nodeTypeName, interfaceName, operationName)?.inputs
    }

    fun nodeTypeInterfaceOperationOutputs(nodeTypeName: String, interfaceName: String, operationName: String): MutableMap<String, com.brvith.framework.core.PropertyDefinition>? {
        return nodeTypeInterfaceOperation(nodeTypeName, interfaceName, operationName)?.outputs
    }

    // Node Template Methods
    val nodeTemplates: MutableMap<String, com.brvith.framework.core.NodeTemplate>? = serviceTemplate.topologyTemplate?.nodeTemplates

    fun nodeTemplateByName(name: String): com.brvith.framework.core.NodeTemplate =
            nodeTemplates?.get(name) ?: throw com.brvith.framework.core.BluePrintException("Failed to get node template for the name " + name)

    fun nodeTemplateForNodeType(name: String): MutableMap<String, com.brvith.framework.core.NodeTemplate>? {
        return nodeTemplates?.filterValues { nodeTemplate -> nodeTemplate.type == name }?.toMutableMap()
    }

    fun nodeTemplateNodeType(nodeTemplateName: String): com.brvith.framework.core.NodeType {
        val nodeTemplateType: String = nodeTemplateByName(nodeTemplateName).type
        return nodeTypeByName(nodeTemplateType)
    }

    fun nodeTemplateProperty(nodeTemplateName: String, propertyName: String): Any? {
        return nodeTemplateByName(nodeTemplateName).properties?.get(propertyName)
    }

    fun nodeTemplateArtifacts(nodeTemplateName: String): MutableMap<String, com.brvith.framework.core.ArtifactDefinition>? {
        return nodeTemplateByName(nodeTemplateName).artifacts
    }

    fun nodeTemplateArtifact(nodeTemplateName: String, artifactName: String): com.brvith.framework.core.ArtifactDefinition? {
        return nodeTemplateArtifacts(nodeTemplateName)?.get(artifactName)
    }

    fun nodeTemplateFirstInterface(nodeTemplateName: String): com.brvith.framework.core.InterfaceAssignment? {
        return nodeTemplateByName(nodeTemplateName).interfaces?.values?.first()
    }

    fun nodeTemplateFirstInterfaceName(nodeTemplateName: String): String? {
        return nodeTemplateByName(nodeTemplateName).interfaces?.keys?.first()
    }

    fun nodeTemplateFirstInterfaceFirstOperationName(nodeTemplateName: String): String? {
        return nodeTemplateFirstInterface(nodeTemplateName)?.operations?.keys?.first()
    }

    fun nodeTemplateInterfaceOperationInputs(nodeTemplateName: String, interfaceName: String, operationName: String): MutableMap<String, JsonNode>? {
        return nodeTemplateByName(nodeTemplateName).interfaces?.get(interfaceName)?.operations?.get(operationName)?.inputs
    }

    fun nodeTemplateInterfaceOperationOutputs(nodeTemplateName: String, interfaceName: String, operationName: String): MutableMap<String, JsonNode>? {
        return nodeTemplateByName(nodeTemplateName).interfaces?.get(interfaceName)?.operations?.get(operationName)?.outputs
    }

    fun nodeTemplateInterface(nodeTemplateName: String, interfaceName: String): com.brvith.framework.core.InterfaceAssignment? {
        return nodeTemplateByName(nodeTemplateName).interfaces?.get(interfaceName)
    }


    fun nodeTemplateInterfaceOperation(nodeTemplateName: String, interfaceName: String, operationName: String): com.brvith.framework.core.OperationAssignment? {
        return nodeTemplateInterface(nodeTemplateName, interfaceName)?.operations?.get(operationName)
    }

    fun nodeTemplateCapability(nodeTemplateName: String, capabilityName: String): com.brvith.framework.core.CapabilityAssignment? {
        return nodeTemplateByName(nodeTemplateName).capabilities?.get(capabilityName)
    }

    fun nodeTemplateRequirement(nodeTemplateName: String, requirementName: String): com.brvith.framework.core.RequirementAssignment? {
        return nodeTemplateByName(nodeTemplateName).requirements?.get(requirementName)
    }

    fun nodeTemplateRequirementNode(nodeTemplateName: String, requirementName: String): com.brvith.framework.core.NodeTemplate {
        val nodeTemplateName: String = nodeTemplateByName(nodeTemplateName).requirements?.get(requirementName)?.node
                ?: throw com.brvith.framework.core.BluePrintException(String.format("failed to get node name for node template's (%s) requirement's (%s) " + nodeTemplateName, requirementName))
        return nodeTemplateByName(nodeTemplateName)
    }

    fun nodeTemplateCapabilityProperty(nodeTemplateName: String, capabilityName: String, propertyName: String): Any? {
        return nodeTemplateCapability(nodeTemplateName, capabilityName)?.properties?.get(propertyName)
    }

    // Chained Functions

    fun nodeTypeChained(nodeTypeName: String): com.brvith.framework.core.NodeType {
        return BluePrintChainedService(this).nodeTypeChained(nodeTypeName)
    }

    fun nodeTypeChainedProperties(nodeTypeName: String): MutableMap<String, com.brvith.framework.core.PropertyDefinition>? {
        return BluePrintChainedService(this).nodeTypeChainedProperties(nodeTypeName)
    }

}