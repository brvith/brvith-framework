package com.brvith.framework.adaptors.messaging


object MessagingConstants {
    const val BOOTSTRAP_SERVERS_CONFIG : String = "bootstrap-server-config"
    const val CLIENT_ID_CONFIG : String = "client-id-config"
    const val GROUP_ID_CONFIG : String = "group-id-config"
    const val TOPIC : String = "topic"
}


