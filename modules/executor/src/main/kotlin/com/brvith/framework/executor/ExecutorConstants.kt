package com.brvith.framework.executor

object ExecutorConstants {
    const val PROPERTY_SCRIPT_TYPE: String = "script-type"
    const val PROPERTY_FILE: String = "file"
    const val PROPERTY_EXECUTE_PATH: String = "execute-path"
    const val PROPERTY_CONTENT: String = "content"
    const val PROPERTY_INCLUDE_PATHS: String = "include-paths"
    const val PROPERTY_ARGS: String = "args"
    const val PROPERTY_OUTPUT_NAMES: String = "output-names"

}