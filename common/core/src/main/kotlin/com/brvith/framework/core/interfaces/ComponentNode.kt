package com.brvith.framework.core.interfaces

import com.brvith.framework.core.BlueprintProcessException

interface ComponentNode {

    @Throws(com.brvith.framework.core.BlueprintProcessException::class)
    fun prepare(context: MutableMap<String, Any>, componentContext: MutableMap<String, Any?>)


    @Throws(com.brvith.framework.core.BlueprintProcessException::class)
    fun process(context: MutableMap<String, Any>, componentContext: MutableMap<String, Any?>)
}