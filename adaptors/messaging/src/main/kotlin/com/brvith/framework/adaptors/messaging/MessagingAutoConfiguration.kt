package com.brvith.framework.adaptors.messaging

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan
@ConditionalOnProperty(prefix = "orchestrator.adaptors.messaging", name = arrayOf("enabled"), havingValue = "true", matchIfMissing = false)
@EnableConfigurationProperties(com.brvith.framework.adaptors.messaging.MessagingProperty::class)
class MessagingAutoConfiguration