package com.brvith.framework.adaptors.messaging

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.stereotype.Service


interface MessageConsumerService {

    open fun setConsumerProperties(properties: MutableMap<String, String>)

}

@Service
class MessageConsumerFactory : ApplicationContextAware {
    val logger: Logger = LoggerFactory.getLogger(this::class.toString())
    var messageConsumers: MutableMap<String, com.brvith.framework.adaptors.messaging.MessageConsumerService> = hashMapOf()

    @Autowired
    lateinit var messagingProperty : com.brvith.framework.adaptors.messaging.MessagingProperty

    fun getInstance(selector: String): com.brvith.framework.adaptors.messaging.MessageConsumerService? {
        val instanceNameProperty= selector+".instance"

        val instanceName = messagingProperty.properties[instanceNameProperty]
                ?: throw com.brvith.framework.adaptors.messaging.MessagingException(String.format("failed to get instance name for orchestrator selector (%s)", selector))

        val messageConsumerService : com.brvith.framework.adaptors.messaging.MessageConsumerService =  messageConsumers.get(instanceName)
                ?: throw com.brvith.framework.adaptors.messaging.MessagingException(String.format("failed to get message producer instance (%s)", instanceName))

        messageConsumerService.setConsumerProperties(messagingProperty.getSelectorProperties(selector))
        return messageConsumerService
    }

    fun injectInstance(instanceName: String, componentNode : com.brvith.framework.adaptors.messaging.MessageConsumerService): Unit {
        messageConsumers[instanceName] = componentNode
    }

    override fun setApplicationContext(context: ApplicationContext) {
        messageConsumers = context.getBeansOfType(com.brvith.framework.adaptors.messaging.MessageConsumerService::class.java)
        logger.info("Injected Message Consumer Nodes : {}", messageConsumers)
    }
}
