package com.brvith.framework.controller


import com.brvith.framework.controller.data.ExecutionRequest
import com.brvith.framework.controller.data.SpecGenerationRequest
import com.brvith.framework.controller.service.BluePrintDeployService
import com.brvith.framework.controller.service.BluePrintExecutionService
import com.brvith.framework.controller.service.SpecGeneratorService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.ServerResponse.ok
import org.springframework.web.reactive.function.server.body
import org.springframework.web.reactive.function.server.router
import reactor.core.publisher.Mono


@Configuration
class RoutingConfiguration {
    val logger: Logger = LoggerFactory.getLogger(this::class.toString())

    @Bean
    fun routerFunction(
            bluePrintDeployService: BluePrintDeployService,
            specGeneratorService: SpecGeneratorService, bluePrintExecutionService: BluePrintExecutionService
    ): RouterFunction<ServerResponse> = router {

        ("/blueprint").nest {
            GET("/ping") { request ->
                logger.info(" Security Info : {} ", request.principal())
                ok().contentType(MediaType.APPLICATION_JSON_UTF8)
                        .body(Mono.just("{\"status\" : \"success\"}"))
            }
            POST("/deploy") { req ->
                bluePrintDeployService.deployBlueprint(req)
            }
            POST("/generatespec") { req ->
                val requestBodyMono = req.bodyToMono(SpecGenerationRequest::class.java)
                ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(specGeneratorService.generateBlueprintSpec(requestBodyMono.block()!!))
            }
            POST("/execute") { req ->
                val requestBodyFlux = req.bodyToFlux(ExecutionRequest::class.java)
                ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(bluePrintExecutionService.execute(requestBodyFlux.blockFirst()!!))
            }
            DELETE("/undeploy/{id}") { req ->
                bluePrintDeployService.unDeployBlueprint(req)
            }

        }
    }
}