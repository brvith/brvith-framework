package com.brvith.framework.workflowrunner.service

import com.brvith.framework.workflowrunner.WorkFlowRunnerJobProperties
import com.brvith.framework.workflowrunner.factory.WorkFlowRunnerStepFactory
import com.brvith.framework.workflowrunner.listener.WorkFlowJobExecutionListener
import org.slf4j.LoggerFactory
import org.springframework.batch.core.Job
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory
import org.springframework.batch.core.job.builder.FlowBuilder
import org.springframework.batch.core.job.builder.FlowJobBuilder
import org.springframework.batch.core.job.flow.Flow
import org.springframework.batch.core.launch.support.RunIdIncrementer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.config.ConfigurableBeanFactory
import org.springframework.cloud.dataflow.core.dsl.*
import org.springframework.context.annotation.Scope
import org.springframework.core.task.TaskExecutor
import org.springframework.stereotype.Service
import org.springframework.util.Assert
import java.util.*

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
class WorkFlowRunnerJobBuilderService {
    private val log = LoggerFactory.getLogger(WorkFlowRunnerJobBuilderService::class.java)

//    @Autowired
//    private lateinit var context: ApplicationContext
    @Autowired
    private lateinit var workFlowRunnerJobProperties: WorkFlowRunnerJobProperties
    @Autowired
    private lateinit var taskExecutor: TaskExecutor
    @Autowired
    private lateinit var jobBuilderFactory: JobBuilderFactory
    @Autowired
    private lateinit var workFlowRunnerStepFactory: WorkFlowRunnerStepFactory

    private val taskBeanSuffixes: MutableMap<String, Int> = hashMapOf()
    private val jobDeque: Deque<Flow> = LinkedList()
    private var visitorDeque: Deque<LabelledTaskNode> = LinkedList()
    private val executionDeque: Deque<Flow> = LinkedList()

    private var splitFlows = 1
    private var hasNestedSplit = false

    private var flowBuilder: FlowBuilder<Flow> = FlowBuilder(UUID.randomUUID().toString())


    private val WILD_CARD = "*"

    @Throws(Exception::class)
    fun getWorkFlowJob(jobName: String, dsl: String): Job {

        val stepVisitor = WorkFlowStepVisitor()
        val taskParser = TaskParser(jobName, dsl, false, true)
        taskParser.parse().accept(stepVisitor)

        this.visitorDeque = stepVisitor.getFlow()

        val workFlowJobExecutionListener = WorkFlowJobExecutionListener()

        val builder = this.jobBuilderFactory.get(jobName)
                .start(this.flowBuilder.start(this.createFlow()).end() as Flow)
                .end()
                .listener(workFlowJobExecutionListener) as FlowJobBuilder

        builder.incrementer(RunIdIncrementer())
        return builder.build()
    }


    private fun createFlow(): Flow {

        while (!this.visitorDeque.isEmpty()) {

            if (this.visitorDeque.peek() is TaskAppNode) {
                val taskAppNode = this.visitorDeque.pop() as TaskAppNode

                if (taskAppNode.hasTransitions()) {
                    handleTransition(this.executionDeque, taskAppNode)
                } else {
                    this.executionDeque.push(getTaskAppFlow(taskAppNode))
                }

            } else if (this.visitorDeque.peek() is SplitNode) {
                val splitNodeDeque = LinkedList<LabelledTaskNode>()
                val splitNode = this.visitorDeque.pop() as SplitNode
                splitNodeDeque.push(splitNode)

                while (!this.visitorDeque.isEmpty() && !this.visitorDeque.peek().equals(splitNode)) {
                    splitNodeDeque.push(this.visitorDeque.pop())
                }
                splitNodeDeque.push(this.visitorDeque.pop())
                handleSplit(splitNodeDeque, splitNode)

            } else if (this.visitorDeque.peek() is FlowNode) {
                handleFlow(this.executionDeque)
            }//When start marker of a DSL flow is found, process it.
            //When end marker of a split is found, process the split
        }

        return this.jobDeque.pop()
    }

    private fun handleFlow(executionDeque: Deque<Flow>) {
        if (!executionDeque.isEmpty()) {
            this.flowBuilder.start(executionDeque.pop())
        }

        while (!executionDeque.isEmpty()) {
            this.flowBuilder.next(executionDeque.pop())
        }

        this.visitorDeque.pop()
        this.jobDeque.push(this.flowBuilder.end())
    }

    private fun handleSplit(visitorDeque: Deque<LabelledTaskNode>, splitNode: SplitNode) {
        this.executionDeque.push(processSplitNode(visitorDeque, splitNode))
    }

    private fun processSplitNode(visitorDeque: Deque<LabelledTaskNode>, splitNode: SplitNode): Flow {
        val flows = LinkedList<Flow>()
        //For each node in the split process it as a DSL flow.
        for (taskNode in splitNode.series) {
            val resultFlowDeque = LinkedList<Flow>()
            flows.addAll(processSplitFlow(taskNode, resultFlowDeque))
        }
        removeProcessedNodes(visitorDeque, splitNode)

        val nestedSplitFlow = FlowBuilder.SplitBuilder(FlowBuilder<Flow>("Split" + UUID.randomUUID().toString()), taskExecutor)
                .add(*flows.toTypedArray())
                .build()

        val taskAppFlowBuilder = FlowBuilder<Flow>("Flow" + UUID.randomUUID().toString())

        if (this.hasNestedSplit) {
            this.splitFlows = flows.size
            val threadCorePoolSize = this.workFlowRunnerJobProperties.splitThreadCorePoolSize

            Assert.isTrue(threadCorePoolSize >= this.splitFlows,
                    "Split thread core pool size " + threadCorePoolSize + " should be equal or greater "
                            + "than the depth of split flows " + (this.splitFlows + 1) + ""
                            + " Try setting the composed function property `splitThreadCorePoolSize`")
        }
        return taskAppFlowBuilder.start(nestedSplitFlow).end()
    }

    private fun removeProcessedNodes(visitorDeque: Deque<LabelledTaskNode>, splitNode: SplitNode) {
        //remove the nodes of the split since it has already been processed
        while (visitorDeque.peek() != null && visitorDeque.peek() != splitNode) {
            visitorDeque.pop()
        }
        // pop the SplitNode that marks the beginning of the split from the deque
        if (visitorDeque.peek() != null) {
            visitorDeque.pop()
        }
    }

    /**
     * Processes each node in split as a  DSL Flow.
     * @param node represents a single node in the split.
     * @return Deque of Job Flows that was obtained from the Node.
     */
    private fun processSplitFlow(node: LabelledTaskNode, resultFlowDeque: Deque<Flow>): Deque<Flow> {

        val taskParser = TaskParser("split_flow" + UUID.randomUUID().toString(), node.stringify(),
                false, true)

        val splitElementVisitor = WorkFlowStepVisitor()
        taskParser.parse().accept(splitElementVisitor)

        val splitElementDeque = splitElementVisitor.getFlow()
        val elementFlowDeque = LinkedList<Flow>()

        while (!splitElementDeque.isEmpty()) {

            if (splitElementDeque.peek() is TaskAppNode) {

                val taskAppNode = splitElementDeque.pop() as TaskAppNode

                if (taskAppNode.hasTransitions()) {
                    handleTransition(elementFlowDeque, taskAppNode)
                } else {
                    elementFlowDeque.push(
                            getTaskAppFlow(taskAppNode))
                }

            } else if (splitElementDeque.peek() is FlowNode) {
                resultFlowDeque.push(handleFlowForSegment(elementFlowDeque))
                splitElementDeque.pop()

            } else if (splitElementDeque.peek() is SplitNode) {
                this.hasNestedSplit = true
                val splitNodeDeque: Deque<LabelledTaskNode> = LinkedList()
                val splitNode = splitElementDeque.pop() as SplitNode
                splitNodeDeque.push(splitNode)

                while (!splitElementDeque.isEmpty() && splitElementDeque.peek() != splitNode) {
                    splitNodeDeque.push(splitElementDeque.pop())
                }

                splitNodeDeque.push(splitElementDeque.pop())
                elementFlowDeque.push(processSplitNode(splitNodeDeque, splitNode))

            }
        }
        return resultFlowDeque
    }

    private fun handleFlowForSegment(resultFlowDeque: Deque<Flow>): Flow {
        val localTaskAppFlowBuilder = FlowBuilder<Flow>("Flow" + UUID.randomUUID().toString())

        if (!resultFlowDeque.isEmpty()) {
            localTaskAppFlowBuilder.start(resultFlowDeque.pop())

        }

        while (!resultFlowDeque.isEmpty()) {
            localTaskAppFlowBuilder.next(resultFlowDeque.pop())
        }

        return localTaskAppFlowBuilder.end()
    }

    private fun handleTransition(resultFlowDeque: Deque<Flow>,
                                 taskAppNode: TaskAppNode) {
        val beanName = getBeanName(taskAppNode)

        //val currentStep = this.context.getBean(beanName, Step::class.java)

        val currentStep = workFlowRunnerStepFactory.getWorkFlowStep(beanName)

        val builder = FlowBuilder<Flow>(beanName).from(currentStep)

        var wildCardPresent = false

        for (transitionNode in taskAppNode.transitions) {
            val transitionBeanName = getBeanName(transitionNode)

            wildCardPresent = transitionNode.statusToCheck == WILD_CARD

            //val transitionStep = this.context.getBean(transitionBeanName, Step::class.java)

            val transitionStep = workFlowRunnerStepFactory.getWorkFlowStep(transitionBeanName)


            builder.on(transitionNode.statusToCheck).to(transitionStep)
                    .from(currentStep)
        }

        if (wildCardPresent && !resultFlowDeque.isEmpty()) {
            throw IllegalStateException(
                    "Invalid flow following '*' specifier.")
        } else {
            //if there are nodes are in the execution Deque.  Make sure that
            //they are processed as a target of the wildcard instead of the
            //whole transition.
            if (!resultFlowDeque.isEmpty()) {
                builder.on(WILD_CARD).to(handleFlowForSegment(resultFlowDeque)).from(currentStep)
            }
        }

        resultFlowDeque.push(builder.end())
    }

    private fun getBeanName(transition: TransitionNode): String {
        return if (transition.targetLabel != null) {
            transition.targetLabel
        } else getBeanName(transition.targetApp)

    }


    private fun getBeanName(taskApp: TaskAppNode): String {
        if (taskApp.label != null) {
            return taskApp.label.stringValue()
        }

        var taskName = taskApp.name

        if (taskName.contains("->")) {
            taskName = taskName.substring(taskName.indexOf("->") + 2)
        }

        return getBeanName(taskName)
    }

    private fun getBeanName(taskName: String): String {
        var taskSuffix = 0

        if (this.taskBeanSuffixes.containsKey(taskName)) {
            taskSuffix = this.taskBeanSuffixes[taskName]!!
        }


        val result = String.format("%s_%s", taskName, taskSuffix++)
        this.taskBeanSuffixes[taskName] = taskSuffix

        return result
    }

    private fun getTaskAppFlow(taskApp: TaskAppNode): Flow {
        val beanName = getBeanName(taskApp)
        //val currentStep = this.context.getBean(beanName, Step::class.java)
        //TODO
        val currentStep = workFlowRunnerStepFactory.getWorkFlowStep(beanName)

        return FlowBuilder<Flow>(beanName).from(currentStep).end()
    }
}