package com.brvith.framework.core.factories

import com.brvith.framework.core.BluePrintException
import com.brvith.framework.core.service.BluePrintContext

interface BluePrintOptimizerService {

    @Throws(BluePrintException::class)
    fun optimize(bluePrintContext: BluePrintContext): BluePrintContext

    @Throws(BluePrintException::class)
    fun optimize(content: String): BluePrintContext

}


object BluePrintOptimizerFactory {

    var bluePrintOptimizerServices: MutableMap<String, BluePrintOptimizerService> = HashMap()

    fun register(key: String, bluePrintOptimizerService: BluePrintOptimizerService) {
        bluePrintOptimizerServices.put(key, bluePrintOptimizerService)
    }

    fun instance(key: String): BluePrintOptimizerService? {
        return bluePrintOptimizerServices.get(key)
    }

}