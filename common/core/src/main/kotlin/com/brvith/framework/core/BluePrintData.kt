package com.brvith.framework.core

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ObjectNode

data class ExpressionData(
        var isExpression: Boolean = false,
        var valueNode: JsonNode,
        var expressionNode: ObjectNode? = null,
        var inputExpression: com.brvith.framework.core.InputExpression? = null,
        var propertyExpression: com.brvith.framework.core.PropertyExpression? = null,
        var attributeExpression: com.brvith.framework.core.AttributeExpression? = null,
        var artifactExpression: com.brvith.framework.core.ArtifactExpression? = null,
        var operationOutputExpression: com.brvith.framework.core.OperationOutputExpression? = null,
        var command: String? = null
)

data class InputExpression(
        var propertyName: String
)

data class PropertyExpression(
        var modelableEntityName: String = "SELF",
        var reqOrCapEntityName: String? = null,
        var propertyName: String,
        var subPropertyName: String?  = null
)

data class AttributeExpression(
        var modelableEntityName: String = "SELF",
        var reqOrCapEntityName: String? = null,
        var attributeName: String,
        var subAttributeName: String? = null
)

data class ArtifactExpression(
        val modelableEntityName: String = "SELF",
        val artifactName: String,
        val location: String? =  "LOCAL_FILE",
        val remove: Boolean? = false
)

data class OperationOutputExpression(
        val modelableEntityName: String = "SELF",
        val interfaceName: String,
        val operationName: String,
        val propertyName: String
)


