package com.brvith.framework.workflowrunner.factory

import com.brvith.framework.workflowrunner.service.WorkFlowRunnerJobBuilderService
import org.slf4j.LoggerFactory
import org.springframework.batch.core.Job
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class WorkFlowRunnerJobFactory {

    private val log = LoggerFactory.getLogger(WorkFlowRunnerJobFactory::class.java)

    @Autowired
    private lateinit var workFlowRunnerJobBuilderService: WorkFlowRunnerJobBuilderService


    @Throws(Exception::class)
    fun getWorkFlowJob(jobName: String, dsl: String): Job {

        return workFlowRunnerJobBuilderService.getWorkFlowJob(jobName, dsl)
    }

}