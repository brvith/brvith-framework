package com.brvith.framework.workflowrunner

import org.springframework.context.annotation.Configuration

@Configuration
class WorkFlowRunnerTaskProperties {

    val MAX_WAIT_TIME_DEFAULT = 0
    val INTERVAL_TIME_BETWEEN_CHECKS_DEFAULT = 10000
    val SPLIT_THREAD_CORE_POOL_SIZE_DEFAULT = 1
    val SPLIT_THREAD_KEEP_ALIVE_SECONDS_DEFAULT = 60
    val SPLIT_THREAD_MAX_POOL_SIZE_DEFAULT = 2147483647
    val SPLIT_THREAD_QUEUE_CAPACITY_DEFAULT = 2147483647


    var maxWaitTime = 0
    var intervalTimeBetweenChecks = 10000
    var splitThreadAllowCoreThreadTimeout: Boolean = false
    //TODO
    var splitThreadCorePoolSize = 10
    var splitThreadKeepAliveSeconds = 60
    var splitThreadMaxPoolSize = 2147483647
    var splitThreadQueueCapacity = 2147483647
    var splitThreadWaitForTasksToCompleteOnShutdown: Boolean = false
    var incrementInstanceEnabled = true

}