package com.brvith.framework.core.service

import com.brvith.framework.core.factories.BluePrintParserService
import com.brvith.framework.core.utils.ServiceTemplateUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File

class BluePrintParserDefaultService : BluePrintParserService {

    private val logger: Logger = LoggerFactory.getLogger(this::class.toString())

    var basePath : String = javaClass.classLoader.getResource(".").path

    override fun readBlueprint(content: String): BluePrintContext {
        return BluePrintContext(ServiceTemplateUtils.getServiceTemplateFromContent(content))
    }

    override fun readBlueprintFile(fileName: String): BluePrintContext {
       return readBlueprintFile(fileName, basePath )
    }

    override fun readBlueprintFile(fileName: String, basePath : String): BluePrintContext {
        val rootFilePath: String = StringBuilder().append(basePath).append(File.separator).append(fileName).toString()
        val rootServiceTemplate : com.brvith.framework.core.ServiceTemplate = ServiceTemplateUtils.getServiceTemplate(rootFilePath)
        val schemaImportResolverUtils = BluePrintResolverService(rootServiceTemplate, basePath)
        val completeServiceTemplate : com.brvith.framework.core.ServiceTemplate = schemaImportResolverUtils.getImportResolvedServiceTemplate()
        return BluePrintContext(completeServiceTemplate)
    }


}

