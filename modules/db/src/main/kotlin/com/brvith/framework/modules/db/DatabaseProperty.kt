package com.brvith.framework.modules.db

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.stereotype.Component


@Component
@EnableConfigurationProperties
class DatabaseProperty {

    @Value("\${orchestrator.modules.db.initial-load-path}")
    lateinit var initialLoadPath: String
}