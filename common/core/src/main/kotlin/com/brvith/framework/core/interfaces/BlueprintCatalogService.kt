package com.brvith.framework.core.interfaces

import com.brvith.framework.core.BlueprintProcessException
import java.io.File


interface BlueprintCatalogService {

    @Throws(com.brvith.framework.core.BlueprintProcessException::class)
    fun prepareBlueprint(blueprintName: String, blueprintVersion: String,
                                  executionPath: String ): File

    @Throws(com.brvith.framework.core.BlueprintProcessException::class)
    fun cleanBlueprint(blueprintName: String, blueprintVersion: String,
                                  executionPath: String ): File

}