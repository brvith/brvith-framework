package com.brvith.framework.modules.db.service


import com.brvith.framework.core.*
import com.brvith.framework.core.service.BluePrintContext
import com.brvith.framework.core.utils.BluePrintMetadataUtils
import com.brvith.framework.core.utils.ZipUtils
import com.brvith.framework.modules.db.domain.BlueprintCatalog
import com.brvith.framework.modules.db.repository.BlueprintCatalogRepository
import org.apache.commons.io.FileUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.gridfs.GridFsTemplate
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.core.publisher.toMono
import java.io.File
import java.io.FileInputStream
import java.nio.charset.Charset
import java.util.*

@Service
class BlueprintCatalogDBServiceImpl : BlueprintCatalogDBService {
    val logger: Logger = LoggerFactory.getLogger(this::class.toString())
    var blueprintCatalogRepository: BlueprintCatalogRepository
    var gridFsTemplate: GridFsTemplate

    constructor(blueprintCatalogRepository: BlueprintCatalogRepository, gridFsTemplate: GridFsTemplate) {
        this.blueprintCatalogRepository = blueprintCatalogRepository
        this.gridFsTemplate = gridFsTemplate
    }

    override fun deployBlueprint(fileName: String, executionPath: String): Mono<String> {

        deCompressBlueprint(fileName, executionPath).block()

        val blueprintCatalog = prepareBlueprintCatalog(executionPath, fileName)

        return saveBlueprintCatalog(blueprintCatalog).map {
            it.id
        }

    }

    override fun getBlueprints(id: String): Flux<BlueprintCatalog> {
        TODO()
    }

    override fun unDeployBlueprint(id: String): Mono<Void> {
        return blueprintCatalogRepository.findById(id).flatMap { blueprintCatalog ->
            val artifactId = blueprintCatalog.id
            deleteGridFS(artifactId)
            logger.info("Deleted File Grid for ({})", artifactId)
            blueprintCatalogRepository.deleteById(artifactId)
        }
    }

    override fun prepareBlueprint(
            blueprintName: String, blueprintVersion: String,
            executionPath: String): Mono<File> {
        logger.info("Copying CSAR name {} version {} from DB to {}", blueprintName, blueprintVersion, executionPath)

        val downloadedFile = downloadBlueprintFromDB(blueprintName, blueprintVersion, executionPath).block()
        logger.info("Downloaded file {}", downloadedFile!!.absoluteFile)

        return deCompressBlueprint(downloadedFile.absolutePath, executionPath).doOnSuccess { decompressedFile ->
            logger.info("Decompressed file {}", decompressedFile.absoluteFile)
        }.onErrorMap { e ->
            com.brvith.framework.core.BlueprintProcessException(format("failed to download csar ({}), version ({})", blueprintName, blueprintVersion), e)
                }
    }

    override fun downloadBlueprintFromDB(
            blueprintName: String,
            blueprintVersion: String, executionPath: String): Mono<File> {

        FileUtils.forceMkdir(File(executionPath))

        val blueprintFileName: String = executionPath.plus(File.separator).plus(com.brvith.framework.core.BluePrintConstants.DEFAULT_CSAR_FILE_NAME)

        val blueprintCatalog = blueprintCatalogRepository.findByNameAndVersion(blueprintName, blueprintVersion)
                .onErrorMap { e ->
                    com.brvith.framework.core.BlueprintProcessException(e, format("failed to get blueprint ({}), version ({}) from DB", blueprintName, blueprintVersion))
                }
                .switchIfEmpty(Mono.error(com.brvith.framework.core.BlueprintProcessException(format("No record for blueprint ({}), version ({}) in DB", blueprintName, blueprintVersion))))
                .block()
        logger.info("Fetching blueprint file for id {}", blueprintCatalog!!.id)

        val resource = gridFsTemplate.getResource(blueprintCatalog.id)

        checkNotNull(resource) {
            Mono.error<com.brvith.framework.core.BlueprintProcessException>(com.brvith.framework.core.BlueprintProcessException(format("Failed to get Resource Info from DB for object id {}", blueprintCatalog.id)))
        }
        return resource.inputStream.toFile(blueprintFileName).toMono()
    }

    override fun deCompressBlueprint(csarFileName: String, executionPath: String): Mono<File> {
        val csarFile = File(csarFileName)
        val executionDir: File = File(executionPath)
        val file = ZipUtils.extract(executionDir, csarFile, Charset.defaultCharset().name(), true)
        logger.info("Decompressed csar file {} to execute path {}", csarFile.absolutePath, executionDir.absolutePath)
        return executionDir.toMono()
    }

    private fun prepareBlueprintCatalog(executionPath: String, fileName: String): BlueprintCatalog {
        val blueprintContext: BluePrintContext = BluePrintMetadataUtils.getBluePrintContext(executionPath)

        val metadata: MutableMap<String, String> = blueprintContext.metadata
                ?: throw com.brvith.framework.core.BlueprintProcessException(format("failed to get metadata data from the blueprint {}", executionPath))

        val email: String = metadata[com.brvith.framework.core.BluePrintConstants.METADATA_AUTHOR_EMAIL]!!
        val groups: String = metadata[com.brvith.framework.core.BluePrintConstants.METADATA_USER_GROUPS]!!
        val name: String = metadata[com.brvith.framework.core.BluePrintConstants.METADATA_BLUEPRINT_NAME]!!
        val version: String = metadata[com.brvith.framework.core.BluePrintConstants.METADATA_BLUEPRINT_VERSION]!!
        val tags: String = metadata[com.brvith.framework.core.BluePrintConstants.METADATA_BLUEPRINT_TAGS]!!

        val entityId: String = UUID.randomUUID().toString()

        deleteBlueprintCatalog(name, version)
        logger.info("3. saving grid catalog file ({})", entityId)

        val objectId = saveGridFS(entityId, fileName, metadata)

        logger.info("4. Saved catalog file ({})", objectId)

        return BlueprintCatalog(name = name, version = version, updatedBy = email,
                userGroups = groups.split(",") as MutableList<String>,
                artifactId = objectId,
                tags = tags,
                id = entityId)
    }

    @Throws(com.brvith.framework.core.BlueprintProcessException::class)
    fun saveBlueprintCatalog(blueprintCatalog: BlueprintCatalog): Mono<BlueprintCatalog> {

        return blueprintCatalogRepository.save(blueprintCatalog)
                .doOnError { e ->
                    Mono.error<com.brvith.framework.core.BlueprintProcessException>(com.brvith.framework.core.BlueprintProcessException(e, "failed to save blueprint catalog ({})", blueprintCatalog.id))
                }.map { it }
        logger.info("5. Saved catalog ({})", blueprintCatalog)

    }

    @Throws(com.brvith.framework.core.BlueprintProcessException::class)
    fun saveBlueprintCatalog1(blueprintCatalog: BlueprintCatalog): Mono<BlueprintCatalog>{

       return blueprintCatalogRepository.save(blueprintCatalog)
                .doOnError { e ->
                    Mono.error<com.brvith.framework.core.BlueprintProcessException>(com.brvith.framework.core.BlueprintProcessException(e, "failed to save blueprint catalog ({})", blueprintCatalog.id))
                }

    }

    fun deleteBlueprintCatalog(name: String, version: String) {
        logger.info("1. Deleting CSAR ({}), version ({})", name, version)

        blueprintCatalogRepository.findByNameAndVersion(name, version)
                .flatMap {
                    logger.info("2. Deleting File Grid  ({})", it.artifactId)
                    deleteGridFS(it.artifactId)
                    blueprintCatalogRepository.deleteById(it.id)
                }.subscribe()
        logger.info("2a. Deleting catalog  ({})", name)
    }

    private fun saveGridFS(entityId: String, file: String, metadata: MutableMap<String, String>): String {
        val ip = FileInputStream(File(file))
        val objectId = gridFsTemplate.store(ip, entityId, "application/zip").toString()
        ip.close()
        logger.info("3. Successfull saveGridFS  objectId({})", objectId)
        return objectId
    }

    private fun deleteGridFS(objectId: String): Unit {
        return gridFsTemplate.delete(Query(Criteria("_id").`is`(objectId)))
    }


}