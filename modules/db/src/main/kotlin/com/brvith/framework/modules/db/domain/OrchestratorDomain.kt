package com.brvith.framework.modules.db.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.util.*

@Document
class BlueprintCatalog(
        @Id
        var id: String = UUID.randomUUID().toString(),
        var name: String,
        var version: String,
        var updatedBy: String,
        var userGroups: MutableList<String>,
        var updatedDate: Date = Date(),
        var tags: String,
        var artifactId: String
)

@Document
class BlueprintModel(
        @Id
        var id: String,
        var modelName: String,
        var definitionType: String,
        var updatedDate: Date = Date(),
        var updatedBy: String,
        var tags: String,
        var definition: String
)

@Document
class TransactionLog(
        @Id
        var id: String,
        var requestId: String,
        var messageIype: String,
        var updatedDate: Date = Date(),
        var message: String
)
