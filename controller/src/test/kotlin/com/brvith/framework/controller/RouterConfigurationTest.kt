package com.brvith.framework.controller

import com.brvith.framework.controller.data.BlueprintIdentifier
import com.brvith.framework.controller.data.ExecutionRequest
import com.brvith.framework.controller.data.Header
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
//import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.core.io.FileSystemResource
import org.springframework.http.MediaType
import org.springframework.http.client.MultipartBodyBuilder
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.ServerResponse


@RunWith(SpringRunner::class)
@SpringBootTest(classes = arrayOf(ControllerAutoConfiguration::class))
class RouterConfigurationTest {

    val logger: Logger = LoggerFactory.getLogger(this::class.toString())
    @Autowired
    lateinit var routerFunction: RouterFunction<ServerResponse>

    @Before
    fun setUp(){
    }

    @Test
    fun testDeployBlueprintPing(){
        logger.info(" ***************** testDeployBlueprintPing *************** ")

        WebTestClient.bindToRouterFunction(routerFunction).build().get().uri("/blueprint/ping").exchange().expectStatus()
                .isOk
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody()

    }

    @Test
    fun testBlueprintDeploy(){
        logger.info(" ***************** testBlueprintDeploy *************** ")
        val builder: MultipartBodyBuilder = MultipartBodyBuilder()
        builder.part("csar-file-name", "sample-blueprint")
        builder.part("csar-file", FileSystemResource(javaClass.classLoader.getResource(".").path + "sample-blueprint/1.0.0/csar.zip"))
        val parts = builder.build()

        WebTestClient.bindToRouterFunction(routerFunction).build().post().uri("/blueprint/deploy").syncBody(parts).exchange().expectStatus()
                .isOk
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody()

    }

    @Test
    fun testBluePrintExecute(){
        logger.info(" ***************** testBluePrintExecute *************** ")
        val executionRequest: ExecutionRequest = ExecutionRequest(Header(requestId = "1234", sourceSystem = "system"),
                BlueprintIdentifier(executionMode = "sync", blueprintName = "sample-blueprint", blueprintVersion = "1.0.0", workflowName = "activate-process"))

        WebTestClient.bindToRouterFunction(routerFunction).build().post().uri("/blueprint/execute")
                .syncBody(executionRequest).exchange().expectStatus()
                .isOk
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody()

    }

}




