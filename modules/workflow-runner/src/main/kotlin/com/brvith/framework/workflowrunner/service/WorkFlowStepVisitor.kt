package com.brvith.framework.workflowrunner.service

import org.slf4j.LoggerFactory
import org.springframework.cloud.dataflow.core.dsl.*
import java.util.*

class WorkFlowStepVisitor : TaskVisitor() {

    private val log = LoggerFactory.getLogger(WorkFlowStepVisitor::class.java)

    private val flowDeque: Deque<LabelledTaskNode> = LinkedList()

    override fun preVisit(flow: FlowNode?): Boolean {
        log.debug("Pre Visit Flow:  " + flow!!)
        this.flowDeque.push(flow)
        return true
    }

    override fun visit(split: SplitNode?) {
        log.debug("Visit Split:  " + split!!)
        this.flowDeque.push(split)
    }

    override fun postVisit(split: SplitNode?) {
        log.debug("Post Visit Split:  " + split!!)
        this.flowDeque.push(split)
    }

    override fun visit(taskApp: TaskAppNode?) {
        log.debug("Visit taskApp:  " + taskApp!!)
        this.flowDeque.push(taskApp)
    }

    fun getFlow(): Deque<LabelledTaskNode> {
        return this.flowDeque
    }
}