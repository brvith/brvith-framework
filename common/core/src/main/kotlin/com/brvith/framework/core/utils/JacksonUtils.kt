package com.brvith.framework.core.utils

import com.brvith.framework.core.format
import com.brvith.framework.core.serializer.YamlPropertyDefinitionSerializer
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.apache.commons.io.FileUtils
import org.apache.commons.io.IOUtils
import java.io.File
import java.nio.charset.Charset

object JacksonUtils {

    inline fun <reified T : Any> readValue(content: String): T =
            jacksonObjectMapper().readValue(content, T::class.java)

    inline fun <reified T : Any> readYaml(content: String): T {
        val objectMapper = ObjectMapper(YAMLFactory())
        return objectMapper.readValue(content, T::class.java)
    }

    fun getYaml(any: Any, pretty: Boolean = false): String {
        val objectMapper = ObjectMapper(YAMLFactory())
        val module = SimpleModule()
        module.addSerializer(JsonNode::class.java, YamlPropertyDefinitionSerializer())
        objectMapper.registerModule(module)
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
        if (pretty) {
            objectMapper.enable(SerializationFeature.INDENT_OUTPUT)
        }
        return objectMapper.writeValueAsString(any)
    }

    fun <T> getListfromJson(content: String, valueType: Class<T>): List<T>? {
        val objectMapper = jacksonObjectMapper()
        val javaType = objectMapper.typeFactory.constructCollectionType(List::class.java, valueType)
        return objectMapper.readValue<List<T>>(content, javaType)
    }

    fun <T> getMapfromJson(content: String, valueType: Class<T>): MutableMap<String, T>? {
        val objectMapper = jacksonObjectMapper()
        val typeRef = object : TypeReference<MutableMap<String, T>>() {}
        return objectMapper.readValue(content, typeRef)
    }


    @JvmStatic
    fun <T> readValue(content: String, valueType: Class<T>): T? {
        return jacksonObjectMapper().readValue(content, valueType)
    }

    @JvmStatic
    fun <T> readValue(node: JsonNode, valueType: Class<T>): T? {
        return jacksonObjectMapper().treeToValue(node, valueType)
    }

    @JvmStatic
    fun getContent(fileName: String): String {
        return File(fileName).readText(Charsets.UTF_8)
    }

    @JvmStatic
    fun getClassPathFileContent(fileName: String): String {
        return IOUtils.toString(JacksonUtils::class.java.classLoader
                .getResourceAsStream(fileName), Charset.defaultCharset())
    }

    @JvmStatic
    fun <T> readValueFromFile(fileName: String, valueType: Class<T>): T? {
        val content: String = FileUtils.readFileToString(File(fileName), Charset.defaultCharset())
                ?: throw com.brvith.framework.core.BluePrintException(format("Failed to read json file : {}", fileName))
        return readValue(content, valueType)
    }

    @JvmStatic
    fun <T> readValueFromClassPathFile(fileName: String, valueType: Class<T>): T? {
        val content: String = getClassPathFileContent(fileName)
        return readValue(content, valueType)
    }

    @JvmStatic
    fun jsonNodeFromObject(from: kotlin.Any): JsonNode = jacksonObjectMapper().convertValue(from, JsonNode::class.java)

    @JvmStatic
    fun jsonNodeFromClassPathFile(fileName: String): JsonNode {
        val content: String = getClassPathFileContent(fileName)
        return jsonNode(content)
    }

    @JvmStatic
    fun jsonNodeFromFile(fileName: String): JsonNode {
        val content: String = FileUtils.readFileToString(File(fileName), Charset.defaultCharset())
                ?: throw com.brvith.framework.core.BluePrintException(format("Failed to read json file : {}", fileName))
        return jsonNode(content)
    }

    @JvmStatic
    fun jsonNode(content: String): JsonNode {
        return jacksonObjectMapper().readTree(content)
    }

    @JvmStatic
    fun getJson(any: kotlin.Any): String {
        return getJson(any, false)
    }

    @JvmStatic
    fun getJson(any: kotlin.Any, pretty: Boolean = false): String {
        val objectMapper = jacksonObjectMapper()
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
        if (pretty) {
            objectMapper.enable(SerializationFeature.INDENT_OUTPUT)
        }
        return objectMapper.writeValueAsString(any)
    }

    @JvmStatic
    fun <T> getListFromJson(content: String, valueType: Class<T>): List<T>? {
        val objectMapper = jacksonObjectMapper()
        val javaType = objectMapper.typeFactory.constructCollectionType(List::class.java, valueType)
        return objectMapper.readValue<List<T>>(content, javaType)
    }

    @JvmStatic
    fun <T> getListFromFile(fileName: String, valueType: Class<T>): List<T>? {
        val content: String = FileUtils.readFileToString(File(fileName), Charset.defaultCharset())
                ?: throw com.brvith.framework.core.BluePrintException(format("Failed to read json file : {}", fileName))
        return getListFromJson(content, valueType)
    }

    @JvmStatic
    fun <T> getListFromClassPathFile(fileName: String, valueType: Class<T>): List<T>? {
        val content: String = getClassPathFileContent(fileName)
        return getListFromJson(content, valueType)
    }

    @JvmStatic
    fun <T> getMapFromJson(content: String, valueType: Class<T>): MutableMap<String, T>? {
        val objectMapper = jacksonObjectMapper()
        val typeRef = object : TypeReference<MutableMap<String, T>>() {}
        return objectMapper.readValue(content, typeRef)
    }


}