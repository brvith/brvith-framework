package com.brvith.framework.core.utils

import com.brvith.framework.core.service.BluePrintContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.NullNode
import org.slf4j.Logger
import org.slf4j.LoggerFactory

object BluePrintRuntimeUtils {
    private val logger: Logger = LoggerFactory.getLogger(this::class.toString())

    fun assignInputs(bluePrintContext: BluePrintContext, jsonNode: JsonNode, context: MutableMap<String, Any>): Unit {
        logger.info("assignInputs from input JSON ({})", jsonNode.toString())
        bluePrintContext.inputs?.forEach { propertyName, property ->
            val valueNode: JsonNode = jsonNode.at("/" + propertyName) ?: NullNode.getInstance()

            val path = StringBuilder(com.brvith.framework.core.BluePrintConstants.PATH_INPUTS)
                    .append(com.brvith.framework.core.BluePrintConstants.PATH_DIVIDER).append(propertyName).toString()
            logger.trace("setting input path ({}), values ({})", path, valueNode)
            context[path] = valueNode
        }
    }

    fun assignInputsFromFile(bluePrintContext: BluePrintContext, fileName: String, context: MutableMap<String, Any>): Unit {
        val jsonNode: JsonNode = JacksonUtils.jsonNodeFromFile(fileName)
        return assignInputs(bluePrintContext, jsonNode, context
        )
    }

    fun assignInputsFromClassPathFile(bluePrintContext: BluePrintContext, fileName: String, context: MutableMap<String,
            Any>): Unit {
        val jsonNode: JsonNode = JacksonUtils.jsonNodeFromClassPathFile(fileName)
        return assignInputs(bluePrintContext, jsonNode, context
        )
    }

}