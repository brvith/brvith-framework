package com.brvith.framework.modules.db

import com.mongodb.reactivestreams.client.MongoClient
import com.mongodb.reactivestreams.client.MongoClients
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.MongoDbFactory
import org.springframework.data.mongodb.config.AbstractReactiveMongoConfiguration
import org.springframework.data.mongodb.core.SimpleMongoDbFactory
import org.springframework.data.mongodb.core.mapping.event.LoggingEventListener
import org.springframework.data.mongodb.gridfs.GridFsTemplate
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories

@Configuration
@EnableReactiveMongoRepositories
//@AutoConfigureAfter(EmbeddedMongoAutoConfiguration::class)
class DatabaseConfiguration : AbstractReactiveMongoConfiguration() {
    var logger: Logger = LoggerFactory.getLogger(this::class.java)

    @Value("\${spring.data.mongodb.database}")
    lateinit var database: String

    @Value("\${spring.data.mongodb.grid-fs-database}")
    lateinit var gridFsdatabase: String

    @Value("\${spring.data.mongodb.host}")
    lateinit var host: String

    @Value("\${spring.data.mongodb.port}")
    lateinit var port: String

    @Bean
    fun mongoEventListener(): LoggingEventListener {
        return LoggingEventListener()
    }

    @Bean
   // @DependsOn("embeddedMongoServer")
    override fun reactiveMongoClient(): MongoClient {
        return MongoClients.create(String.format("mongodb://%s:%s",host, port))
    }

    override fun getDatabaseName(): String {
        logger.info("Setting database name : {}", database)
       return database
    }

    @Bean
    fun gridFsMongoDbFactory(): MongoDbFactory {
        logger.info("Setting gridfs database name : {}", this.gridFsdatabase)
        return SimpleMongoDbFactory(com.mongodb.MongoClient(), this.gridFsdatabase)
    }

    @Bean
    fun gridFsTemplate(): GridFsTemplate {
        return GridFsTemplate(gridFsMongoDbFactory(), mappingMongoConverter())
    }
}