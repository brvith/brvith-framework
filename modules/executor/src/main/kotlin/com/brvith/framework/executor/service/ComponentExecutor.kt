package com.brvith.framework.executor.service

import com.brvith.framework.core.*
import com.brvith.framework.core.interfaces.ComponentNode
import com.brvith.framework.core.service.BluePrintContext
import com.brvith.tosca.model.service.BluePrintRuntimeService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Service

interface ComponentExecutor {
    @Throws(com.brvith.framework.core.BlueprintProcessException::class)
    fun execute(context: MutableMap<String, Any>)
}

@Service
@Scope("prototype")
class ComponentExecutorImpl(private val javaComponentNodeFactory: JavaComponentNodeFactory) : ComponentExecutor {
    val logger: Logger = LoggerFactory.getLogger(this::class.java)


    override fun execute(context: MutableMap<String, Any>) {
        logger.trace("Invoking context : {}", context)

        val requestId: String = context.getCastValue(com.brvith.framework.core.BluePrintConstants.PROPERTY_BLUEPRINT_PROCESS_ID, String::class)

        try {
            val bluePrintContext: BluePrintContext = context.getCastValue(com.brvith.framework.core.BluePrintConstants.PROPERTY_BLUEPRINT_CONTEXT, BluePrintContext::class)

            val bluePrintRuntimeService = BluePrintRuntimeService(bluePrintContext, context)

            logger.info("processing request id ({})", requestId)

            val nodeTemplateName: String = context.getCastValue(com.brvith.framework.core.BluePrintConstants.SELECTOR, String::class)

            val nodeTemplate: com.brvith.framework.core.NodeTemplate = bluePrintContext.nodeTemplateByName(nodeTemplateName)

            val nodeType: com.brvith.framework.core.NodeType = bluePrintContext.nodeTypeByName(nodeTemplate.type)

            val selectedInterfaceName: String? = context.getCastOptionalValue(com.brvith.framework.core.BluePrintConstants.PROPERTY_CURRENT_INTERFACE, String::class)

            val interfaceName: String = selectedInterfaceName
                    ?: bluePrintContext.nodeTemplateFirstInterfaceName(nodeTemplateName)
                    ?: throw com.brvith.framework.core.BlueprintProcessException(String.format("failed to get interface name first interface for node template (%s)", nodeTemplateName))

            val componentContext =
                    bluePrintRuntimeService.resolveNodeTemplateInterfaceOperationInputs(nodeTemplateName, interfaceName,
                            com.brvith.framework.core.BluePrintConstants.OPERATION_PROCESS)

            componentContext[com.brvith.framework.core.BluePrintConstants.PROPERTY_BLUEPRINT_PROCESS_ID] = requestId
            componentContext[com.brvith.framework.core.BluePrintConstants.SELECTOR] = nodeTemplateName
            componentContext[com.brvith.framework.core.BluePrintConstants.PROPERTY_CURRENT_INTERFACE] = interfaceName
            componentContext[com.brvith.framework.core.BluePrintConstants.PROPERTY_CURRENT_OPERATION] = com.brvith.framework.core.BluePrintConstants.OPERATION_PROCESS

            val operationAssignment: com.brvith.framework.core.OperationAssignment? = bluePrintContext
                    .nodeTemplateInterfaceOperation(nodeTemplateName, interfaceName, com.brvith.framework.core.BluePrintConstants.OPERATION_PROCESS)

            var componentInstance: ComponentNode? = null
            logger.info("Searching ({}) Component ({})", nodeType.derivedFrom, interfaceName)
            when (nodeType.derivedFrom) {

                com.brvith.framework.core.BluePrintConstants.CUSTOM_NODETYPE_COMPONENT_SCRIPT -> {

                }
                com.brvith.framework.core.BluePrintConstants.CUSTOM_NODETYPE_COMPONENT_PYTHON -> {
                    val pythonComponentNode = JythonComponentExecutorService()
                    componentInstance = pythonComponentNode.getComponentNode(nodeTemplateName, interfaceName,
                            nodeType, nodeTemplate, operationAssignment!!, bluePrintRuntimeService, context)
                }
                else -> {
                    componentInstance = javaComponentNodeFactory.getInstance(interfaceName)
                }
            }

            checkNotNull(componentInstance) {
                throw com.brvith.framework.core.BlueprintProcessException("failed to get component node instance for name " + interfaceName)
            }

            componentInstance!!.process(context, componentContext)
            // Store the Output properties in context
            bluePrintRuntimeService.resolveNodeTemplateInterfaceOperationOutputs(nodeTemplateName, interfaceName,
                    com.brvith.framework.core.BluePrintConstants.OPERATION_PROCESS, componentContext)

            logger.info("Response component context : {}", componentContext)

        } catch (e: Exception) {
            throw com.brvith.framework.core.BlueprintProcessException(String.format("failed to execute delegate request (%s), %s", requestId, e.message), e)
        }
    }
}