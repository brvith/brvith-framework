package com.brvith.framework.core.service

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class BluePrintExpressionServiceTest {
    @Test
    fun testInputExpression() {
        val node : JsonNode = jacksonObjectMapper().readTree("{ \"get_input\" : \"input-name\" }")
        val expressionData : com.brvith.framework.core.ExpressionData = BluePrintExpressionService.getExpressionData(node)
        assertNotNull(expressionData, " Failed to populate expression data")
        assertEquals(expressionData.isExpression, true, "Failed to identify as expression")
        assertNotNull(expressionData.inputExpression, " Failed to populate input expression data")
        assertEquals("input-name", expressionData.inputExpression?.propertyName, "Failed to get propertyName from expression data")
    }

    @Test
    fun testPropertyExpression() {
        val node : JsonNode = jacksonObjectMapper().readTree("{ \"get_property\" : [\"SELF\", \"property-name\"] }")
        val expressionData : com.brvith.framework.core.ExpressionData = BluePrintExpressionService.getExpressionData(node)
        assertNotNull(expressionData, " Failed to populate expression data")
        assertEquals(expressionData.isExpression, true, "Failed to identify as expression")
        assertNotNull(expressionData.propertyExpression, " Failed to populate property expression data")
        assertEquals("SELF", expressionData.propertyExpression?.modelableEntityName, " Failed to get expected modelableEntityName")
        assertEquals("property-name", expressionData.propertyExpression?.propertyName, " Failed to get expected propertyName")

        val node1 : JsonNode = jacksonObjectMapper().readTree("{ \"get_property\" : [\"SELF\", \"\",\"property-name\", \"resource\", \"name\"] }")
        val expressionData1  : com.brvith.framework.core.ExpressionData = BluePrintExpressionService.getExpressionData(node1)
        assertNotNull(expressionData1, " Failed to populate expression data")
        assertEquals(expressionData1.isExpression, true, "Failed to identify as nested property expression")
        assertNotNull(expressionData1.propertyExpression, " Failed to populate nested property expression data")
        assertEquals("SELF", expressionData1.propertyExpression?.modelableEntityName, " Failed to get expected modelableEntityName")
        assertEquals("property-name", expressionData1.propertyExpression?.propertyName, " Failed to get expected propertyName")
        assertEquals("resource/name",expressionData1.propertyExpression?.subPropertyName, " Failed to populate nested subPropertyName expression data")
    }

    @Test
    fun testAttributeExpression() {
        val node : JsonNode = jacksonObjectMapper().readTree("{ \"get_attribute\" : [\"SELF\", \"\",\"attribute-name\", \"resource\", \"name\"] }")
        val expressionData : com.brvith.framework.core.ExpressionData = BluePrintExpressionService.getExpressionData(node)
        assertNotNull(expressionData, " Failed to populate expression data")
        assertEquals(expressionData.isExpression, true, "Failed to identify as expression")
        assertNotNull(expressionData.attributeExpression, " Failed to populate attribute expression data")
        assertEquals("SELF", expressionData.attributeExpression?.modelableEntityName, " Failed to get expected modelableEntityName")
        assertEquals("attribute-name", expressionData.attributeExpression?.attributeName, " Failed to get expected attributeName")
        assertEquals("resource/name",expressionData.attributeExpression?.subAttributeName, " Failed to populate nested subAttributeName expression data")
    }


    @Test
    fun testOutputOperationExpression() {
        val node : JsonNode = jacksonObjectMapper().readTree("{ \"get_operation_output\": [\"SELF\", \"interface-name\", \"operation-name\", \"output-property-name\"] }")
        val expressionData : com.brvith.framework.core.ExpressionData = BluePrintExpressionService.getExpressionData(node)
        assertNotNull(expressionData, " Failed to populate expression data")
        assertEquals(expressionData.isExpression, true, "Failed to identify as expression")
        assertNotNull(expressionData.operationOutputExpression, " Failed to populate output expression data")
        assertEquals("SELF", expressionData.operationOutputExpression?.modelableEntityName, " Failed to get expected modelableEntityName")
        assertEquals("interface-name", expressionData.operationOutputExpression?.interfaceName, " Failed to get expected interfaceName")
        assertEquals("operation-name", expressionData.operationOutputExpression?.operationName, " Failed to get expected operationName")
        assertEquals("output-property-name", expressionData.operationOutputExpression?.propertyName, " Failed to get expected propertyName")
    }


    @Test
    fun testArtifactExpression() {
        val node : JsonNode = jacksonObjectMapper().readTree("{ \"get_artifact\" : [\"SELF\", \"artifact-template\"] }")
        val expressionData : com.brvith.framework.core.ExpressionData = BluePrintExpressionService.getExpressionData(node)
        assertNotNull(expressionData, " Failed to populate expression data")
        assertEquals(expressionData.isExpression, true, "Failed to identify as expression")
        assertNotNull(expressionData.artifactExpression, " Failed to populate Artifact expression data")
        assertEquals("SELF", expressionData.artifactExpression?.modelableEntityName, " Failed to get expected modelableEntityName")
        assertEquals("artifact-template", expressionData.artifactExpression?.artifactName, " Failed to get expected artifactName")


        val node1 : JsonNode = jacksonObjectMapper().readTree("{ \"get_artifact\" : [\"SELF\", \"artifact-template\", \"location\", true] }")
        val expressionData1 : com.brvith.framework.core.ExpressionData = BluePrintExpressionService.getExpressionData(node1)
        assertNotNull(expressionData1, " Failed to populate expression data")
        assertEquals(expressionData1.isExpression, true, "Failed to identify as expression")
        assertNotNull(expressionData1.artifactExpression, " Failed to populate Artifact expression data")
        assertEquals("SELF", expressionData1.artifactExpression?.modelableEntityName, " Failed to get expected modelableEntityName")
        assertEquals("artifact-template", expressionData1.artifactExpression?.artifactName, " Failed to get expected artifactName")
        assertEquals("location", expressionData1.artifactExpression?.location, " Failed to get expected location")
        assertEquals(true, expressionData1.artifactExpression?.remove, " Failed to get expected remove")
    }
}