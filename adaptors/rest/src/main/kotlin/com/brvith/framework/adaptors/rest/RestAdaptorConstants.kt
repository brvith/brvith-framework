package com.brvith.framework.adaptors.rest

object RestAdaptorConstants {

    const val REST_ADAPTOR_TYPE_GENERIC = "generic"
    const val SERVICE_TYPE_PROPERTY = ".type"
    const val SERVICE_BASEURL_PROPERTY = ".url"
    const val SERVICE_USER_PROPERTY = ".user"
    const val SERVICE_TOKEN_PROPERTY = ".passwd"

    const val PROXY_URL_KEY = "proxyUrl"
    const val PROXY_URLS_VALUE_SEPARATOR = ","
    const val AAF_USERNAME_KEY = "aafUserName"
    const val AAF_PASSWORD_KEY = "aafPassword"
    const val COMMON_SERVICE_VERSION_KEY = "commonServiceVersion"
    const val PARTNER_KEY = "partner"


}