package com.brvith.framework.core.factories

import com.brvith.framework.core.BluePrintException
import reactor.core.publisher.Mono
import java.io.File


interface BluePrintProcessService {

    @Throws(com.brvith.framework.core.BluePrintException::class)
    fun loadProcessFiles(tenantId: String, workflowDir: String): Mono<MutableList<File>>

    @Throws(com.brvith.framework.core.BluePrintException::class)
    fun executeProcessByKey(tenantId: String, key: String, inputs: MutableMap<String, Any>): Mono<MutableMap<String, Any>>

    @Throws(com.brvith.framework.core.BluePrintException::class)
    fun undeployProcess(tenantId: String): Mono<MutableList<String>>

}


object BluePrintProcessFactory {

    var bluePrintProcessServices: MutableMap<String, BluePrintProcessService> = HashMap()

    fun register(key: String, bluePrintProcessService: BluePrintProcessService) {
        bluePrintProcessServices.put(key, bluePrintProcessService)
    }

    fun instance(key: String): BluePrintProcessService? {
        return bluePrintProcessServices.get(key)
    }

}