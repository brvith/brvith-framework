package com.brvith.framework.core

object BluePrintTypes {

    fun validModelTypes(): List<String> {
        var validTypes: MutableList<String> = arrayListOf()
        validTypes.add(com.brvith.framework.core.BluePrintConstants.MODEL_DEFINITION_TYPE_DATA_TYPE)
        validTypes.add(com.brvith.framework.core.BluePrintConstants.MODEL_DEFINITION_TYPE_ARTIFACT_TYPE)
        validTypes.add(com.brvith.framework.core.BluePrintConstants.MODEL_DEFINITION_TYPE_NODE_TYPE)
        validTypes.add(com.brvith.framework.core.BluePrintConstants.MODEL_DEFINITION_TYPE_CAPABILITY_TYPE)
        validTypes.add(com.brvith.framework.core.BluePrintConstants.MODEL_DEFINITION_TYPE_RELATIONSHIP_TYPE)
        return validTypes
    }

    fun validPropertyTypes(): List<String> {
        var validTypes: MutableList<String> = arrayListOf()
        validTypes.add(com.brvith.framework.core.BluePrintConstants.DATA_TYPE_STRING)
        validTypes.add(com.brvith.framework.core.BluePrintConstants.DATA_TYPE_INTEGER)
        validTypes.add(com.brvith.framework.core.BluePrintConstants.DATA_TYPE_FLOAT)
        validTypes.add(com.brvith.framework.core.BluePrintConstants.DATA_TYPE_BOOLEAN)
        validTypes.add(com.brvith.framework.core.BluePrintConstants.DATA_TYPE_TIMESTAMP)
        validTypes.add(com.brvith.framework.core.BluePrintConstants.DATA_TYPE_NULL)
        validTypes.add(com.brvith.framework.core.BluePrintConstants.DATA_TYPE_LIST)
        return validTypes
    }

    fun validPrimitiveTypes(): List<String> {
        var validTypes: MutableList<String> = arrayListOf()
        validTypes.add(com.brvith.framework.core.BluePrintConstants.DATA_TYPE_STRING)
        validTypes.add(com.brvith.framework.core.BluePrintConstants.DATA_TYPE_INTEGER)
        validTypes.add(com.brvith.framework.core.BluePrintConstants.DATA_TYPE_FLOAT)
        validTypes.add(com.brvith.framework.core.BluePrintConstants.DATA_TYPE_BOOLEAN)
        validTypes.add(com.brvith.framework.core.BluePrintConstants.DATA_TYPE_TIMESTAMP)
        validTypes.add(com.brvith.framework.core.BluePrintConstants.DATA_TYPE_NULL)
        return validTypes
    }

    fun validColletionTypes(): List<String> {
        var validTypes: MutableList<String> = arrayListOf()
        validTypes.add(com.brvith.framework.core.BluePrintConstants.DATA_TYPE_LIST)
        validTypes.add(com.brvith.framework.core.BluePrintConstants.DATA_TYPE_MAP)
        return validTypes
    }

    fun validCommands(): List<String> {
        return listOf(com.brvith.framework.core.BluePrintConstants.EXPRESSION_GET_INPUT,
                com.brvith.framework.core.BluePrintConstants.EXPRESSION_GET_ATTRIBUTE,
                com.brvith.framework.core.BluePrintConstants.EXPRESSION_GET_PROPERTY,
                com.brvith.framework.core.BluePrintConstants.EXPRESSION_GET_ARTIFACT,
                com.brvith.framework.core.BluePrintConstants.EXPRESSION_GET_OPERATION_OUTPUT,
                com.brvith.framework.core.BluePrintConstants.EXPRESSION_GET_NODE_OF_TYPE)
    }

    fun rootNodeTypes(): List<String> {
        return listOf(com.brvith.framework.core.BluePrintConstants.ROOT_NODES)
    }

    fun rootDataTypes(): List<String> {
        return listOf(com.brvith.framework.core.BluePrintConstants.ROOT_DATATYPE)
    }


}