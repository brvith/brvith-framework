from com.brvith.framework.core.interfaces import ComponentNode

class SamplePythonComponentNode(ComponentNode):
    def __init__(self):
        return None

    def prepare(self, context, componentContext):
        logger.info("prepare Context {}", context)
        return None

    def process(self, context, componentContext):
        logger.info("process Context {}", context)
        componentContext.put("output", "Success")
        return None
