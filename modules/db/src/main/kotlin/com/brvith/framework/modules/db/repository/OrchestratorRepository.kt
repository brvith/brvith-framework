package com.brvith.framework.modules.db.repository

import com.brvith.framework.modules.db.domain.BlueprintCatalog
import com.brvith.framework.modules.db.domain.BlueprintModel
import com.brvith.framework.modules.db.domain.TransactionLog
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface BlueprintCatalogRepository : ReactiveCrudRepository<BlueprintCatalog, String> {
    fun findByNameAndVersion(name: String, version: String): Mono<BlueprintCatalog>
}

interface BlueprintModelRepository : ReactiveCrudRepository<BlueprintModel, String> {
    fun findByDefinitionType(definitionType: String): Flux<BlueprintModel>
}

interface TransactionLogRepository : ReactiveCrudRepository<TransactionLog, String>