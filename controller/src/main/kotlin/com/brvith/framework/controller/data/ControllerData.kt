package com.brvith.framework.controller.data

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDateTime

data class SpecGenerationRequest(
        @get:JsonProperty("common-header")
        val header: Header,
        @get:JsonProperty("blueprint-name")
        val blueprintName: String,
        @get:JsonProperty("blueprint-version")
        val blueprintVersion: String,
        var data: Any = hashMapOf<String, String>())

data class SpecGenerationResponse(
        var status: Status,
        var spec: Any = hashMapOf<String, String>())

data class ExecutionRequest(
        @get:JsonProperty("header")
        val header: Header,

        @get:JsonProperty("blueprint-identifier")
        val blueprintIdentifier: BlueprintIdentifier,

        var data: Any = hashMapOf<String, String>())

data class ExecutionResponse(
        @get:JsonProperty("header")
        val header: Header,

        @get:JsonProperty("blueprint-identifier")
        val blueprintIdentifier: BlueprintIdentifier,

        var data: Any = hashMapOf<String, String>(),

        var status: Status)

data class Header(
        @get:JsonProperty("request-id")
        val requestId: String,

        @get:JsonProperty("source-system")
        val sourceSystem: String,

        @get:JsonProperty("time-stamp")
        val timeStamp: LocalDateTime? = LocalDateTime.now())

data class BlueprintIdentifier(
        @get:JsonProperty("blueprint-name")
        val blueprintName: String,

        @get:JsonProperty("blueprint-version")
        val blueprintVersion: String,

        @get:JsonProperty("workflow-name")
        val workflowName: String,

        @get:JsonProperty("mode")
        val executionMode: String)

data class Status(var code: String, var message: String,
                  @get:JsonProperty("error-message")
                  var errorMessage: String? = null)