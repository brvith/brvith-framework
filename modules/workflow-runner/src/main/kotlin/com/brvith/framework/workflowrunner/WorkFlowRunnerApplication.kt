package com.brvith.framework.workflowrunner

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


@SpringBootApplication
class WorkFlowRunnerApplication {

    fun main(args: Array<String>) {
        runApplication<WorkFlowRunnerApplication>(*args)
    }
}
