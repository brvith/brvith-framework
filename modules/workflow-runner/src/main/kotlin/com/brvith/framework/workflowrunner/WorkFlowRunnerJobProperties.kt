package com.brvith.framework.workflowrunner

import org.springframework.context.annotation.Configuration

@Configuration
class WorkFlowRunnerJobProperties {
    var splitThreadCorePoolSize = 10
}