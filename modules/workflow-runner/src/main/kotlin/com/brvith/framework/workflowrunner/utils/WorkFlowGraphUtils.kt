package com.brvith.framework.workflowrunner.utils

import com.brvith.framework.core.notNullAndNotEmpty
import com.brvith.framework.core.nullOrEmpty
import org.slf4j.LoggerFactory
import org.springframework.cloud.dataflow.core.dsl.TaskNode
import org.springframework.cloud.dataflow.core.dsl.TaskParser
import org.springframework.cloud.dataflow.core.dsl.graph.Graph
import org.springframework.cloud.dataflow.core.dsl.graph.Link
import org.springframework.cloud.dataflow.core.dsl.graph.Node

class WorkFlowGraphUtils(private val workflowName: String, private val workFlow: com.brvith.framework.core.Workflow) {
    private val log = LoggerFactory.getLogger(WorkFlowGraphUtils::class.java)

    private val NODE_START = "START"
    private val NODE_END = "END"
    private val LINK_SUCCESS = "SUCCESS"
    private val LINK_FAILURE = "FAILURE"

    fun printGraph(graph: Graph) {
        graph.links?.forEach {
            log.info("Graph : {}", it)
        }
        log.info("DSL : {}", graph.toDSLText())
    }

    fun generateTaskNode(jobName: String): TaskNode {
        val graph = generateGraph()
        val taskParser = TaskParser(jobName, graph.toDSLText(), true, true)
        return taskParser.parse()
    }

    fun generateGraph(): Graph {
        // Populate Start
        val nodes: MutableList<Node> = arrayListOf()

        nodes.add(Node(NODE_START, NODE_START))
        nodes.add(Node(NODE_END, NODE_END))
        // Populate Start
        val stepNodes: MutableList<Node> = workFlow.steps?.map { it.key }?.map {
            Node(it, it)
        }?.toMutableList()!!

        nodes.addAll(stepNodes)

        val links: MutableList<Link> = arrayListOf()
        enhanceStartStep(links)
        enhanceEndStep(links)
        enhanceTransitions(links)

        val graph = Graph(nodes, links)
        //printGraph(graph)
        return graph
    }

    private fun enhanceStartStep(links: MutableList<Link>) {
        // Populate Start
        val stepNames = workFlow.steps?.map { it.key }

        val destinationSteps: MutableSet<String> = mutableSetOf()

        workFlow.steps?.forEach { _, step ->
            step.onSuccess?.let { destinationSteps.addAll(step.onSuccess!!) }
            step.onFailure?.let { destinationSteps.addAll(step.onFailure!!) }
        }

        val startNodes = stepNames?.filter { !destinationSteps.contains(it) }
        log.info("Start Steps  :{}", startNodes)

        startNodes?.forEach {
            val startStepLink = Link(NODE_START, it)
            links.add(startStepLink)
        }

    }

    private fun enhanceEndStep(links: MutableList<Link>) {

        workFlow.steps?.forEach { stepName, step ->

            if (nullOrEmpty(step.onSuccess) && nullOrEmpty(step.onFailure)) {
                // Populate End links
                val endStepLink = Link(stepName, NODE_END)
                links.add(endStepLink)
            }
        }

    }

    private fun enhanceTransitions(links: MutableList<Link>) {

        workFlow.steps?.forEach { stepName, step ->
            if (notNullAndNotEmpty(step.onSuccess) && notNullAndNotEmpty(step.onFailure)) {

                step.onSuccess?.forEach { onSuccessStep ->
                    val successStepLink = Link(stepName, onSuccessStep)
                    links.add(successStepLink)
                }

                step.onFailure?.forEach { onFailureStep ->
                    val failureStepLink = Link(stepName, onFailureStep, LINK_FAILURE)
                    links.add(failureStepLink)
                }

            } else if (notNullAndNotEmpty(step.onSuccess) && nullOrEmpty(step.onFailure)) {

                step.onSuccess?.forEach { onSuccessStep ->
                    val successStepLink = Link(stepName, onSuccessStep)
                    links.add(successStepLink)
                }

            } else if (nullOrEmpty(step.onSuccess) && notNullAndNotEmpty(step.onFailure)) {
                step.onFailure?.forEach { onFailureStep ->
                    val failureStepLink = Link(stepName, onFailureStep, LINK_FAILURE)
                    links.add(failureStepLink)
                }
            }
        }
    }

}