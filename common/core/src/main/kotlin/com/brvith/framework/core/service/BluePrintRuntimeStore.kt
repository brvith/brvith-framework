package com.brvith.framework.core.service

import java.util.*
import java.util.concurrent.ConcurrentHashMap

object BluePrintRuntimeStore {

    var store = ConcurrentHashMap<String, MutableMap<String, Any>>()

    fun register(context: MutableMap<String, Any> , id : String? = UUID.randomUUID().toString() ): String {
        store[id!!] = context
        return id
    }
}