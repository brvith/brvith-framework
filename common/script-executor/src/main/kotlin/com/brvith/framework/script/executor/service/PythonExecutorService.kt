package com.brvith.framework.script.executor.service

import com.brvith.framework.core.interfaces.ComponentNode
import com.brvith.framework.core.interfaces.ScriptExecutorService
import org.python.core.PyObject
import org.python.util.PythonInterpreter
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.util.*

open class PythonExecutorService : ScriptExecutorService {
    val logger: Logger = LoggerFactory.getLogger(this::class.toString())

    override fun execute(executePath: String, command: String, args: MutableList<String>?, returnParameters: MutableList<String>?): MutableMap<String, Any> {

        initPython(executePath, Collections.emptyList(), args!!)

        var pythonInterpreter = PythonInterpreter()
        pythonInterpreter.exec("import sys")
        pythonInterpreter.exec(command)

        val results: MutableMap<String, Any> = hashMapOf()
        returnParameters?.forEach { name ->
            val type = pythonInterpreter.get(name).type
            logger.info(" Output {}, type {}", name, type)
            results.put(name, pythonInterpreter.get(name))
        }
        return results
    }

    override fun executeFile(executePath: String, fileName: String, args: MutableList<String>?, returnParameters: MutableList<String>?): MutableMap<String, Any> {
        initPython(executePath, arrayListOf(), args!!)

        var pythonInterpreter = PythonInterpreter()
        pythonInterpreter.exec("import sys")

        val filePath = executePath.plus(File.separator).plus(fileName)
        pythonInterpreter.execfile(filePath)

        val results: MutableMap<String, Any> = hashMapOf()
        returnParameters?.forEach { name ->
            results.put(name, pythonInterpreter.get(name))
        }
        return results
    }

    override fun getComponent(executePath: String, pythonPath: MutableList<String>,
                              content: String?, componentName: String,
                              properties: MutableMap<String, Any>): ComponentNode {

        initPython(executePath, pythonPath, arrayListOf())
        var pythonInterpreter = PythonInterpreter()

        properties.forEach { (name, value) ->
            pythonInterpreter.set(name, value)
        }

        pythonInterpreter.exec("import sys")

        content?.let {
            pythonInterpreter.exec(content)
        }

        val initCommand = componentName.plus(" = ").plus(componentName).plus("()")
        pythonInterpreter.exec(initCommand)
        val pyObject: PyObject = pythonInterpreter.get(componentName)

        logger.info("Component Object {}",pyObject )

//        properties.forEach { (name, value) ->
//            val pyObject : PyObject = Py.java2py(value)
//            pyObject.__setattr__(name, pyObject)
//        }

        return pyObject.__tojava__(ComponentNode::class.java) as ComponentNode
    }


    private fun initPython(executablePath: String,
                           pythonPath: MutableList<String>, argv: MutableList<String>) {
        val props = Properties()
        // Build up the python.path
        val sb = StringBuilder()
        sb.append(System.getProperty("java.class.path"))
        for (p in pythonPath) {
            sb.append(":").append(p)
        }
        props.put("python.import.site", "false")
        props.setProperty("python.path", sb.toString())
        props.setProperty("python.verbose", "error")
        props.setProperty("python.executable", executablePath)

        PythonInterpreter.initialize(System.getProperties(), props, argv.toTypedArray())
    }
}