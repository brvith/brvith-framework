package com.brvith.framework.core.utils

import org.junit.Test

class SequencerUtilsTest {
    
    @Test
    fun testSorting(): Unit {
        var graph: SequencerUtils<String> = SequencerUtils<String>()
        graph.add("bundle-id", "bundle-mac")
        graph.add("bundle-id", "bundle-ip")
        graph.add("bundle-mac", "bundle-ip")
        graph.add("bundle-ip", "bundle-mac")

        println("The current graph: " + graph)
        println("In-degrees: " + graph.inDegree())
        println("Out-degrees: " + graph.outDegree())
        println("A topological sort of the vertices: " + graph.topSort())
    }
}