package com.brvith.framework.core.utils

import org.apache.commons.compress.archivers.zip.ZipFile
import org.apache.commons.io.FileUtils
import org.apache.commons.io.IOUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.*


object ZipUtils {
    val logger: Logger = LoggerFactory.getLogger(this::class.toString())

    fun extract1(outputDir: File, zipArchive: File, encoding: String, clean : Boolean? = false): Unit {

        ZipFile(zipArchive).use { zip ->
            zip.entries.asSequence().forEach { entry ->
                zip.getInputStream(entry).use { input ->
                    File(entry.name).outputStream().use { output ->
                        input.copyTo(output)
                    }
                }
            }
        }
    }

    /**
     * Takes in the outputDir(extracted files are placed here), zipArchive and
     * file encoding;
     *
     * @param outputDir
     * @param zipArchive
     * @param encoding
     * @return the output directory
     * @throws IOException
     */
    @Throws(IOException::class)
    fun extract(outputDir: File, zipArchive: File, encoding: String, clean : Boolean? = false): Unit {
        var inputStream: InputStream? = null
        var outputStream: OutputStream? = null
        var zipFile: ZipFile? = null
        try {
            if(clean!!){
                FileUtils.forceDeleteOnExit(outputDir)
                logger.info("cleaning output dir {}", outputDir.absolutePath)
                FileUtils.forceMkdir(outputDir)
            }
            zipFile = ZipFile(zipArchive, encoding)
            val zipArchiveEntries = zipFile.entries
            while (zipArchiveEntries.hasMoreElements()) {
                val zipArchiveEntry = zipArchiveEntries.nextElement()
                val entryName: String = zipArchiveEntry.name
                if (zipArchiveEntry.isDirectory) {
                    val outputDirName: String = StringBuilder().append(outputDir.absolutePath).append(File.separator).append(entryName).toString()
                    val outDir = File(outputDirName)
                    FileUtils.forceMkdir(outDir)
                    continue
                }
                val outputFileName: String = StringBuilder().append(outputDir.absolutePath).append(File.separator).append(entryName).toString()
                outputStream = FileOutputStream(File(outputFileName))

                inputStream = zipFile.getInputStream(zipArchiveEntry)
                IOUtils.copy(inputStream, outputStream)
            }
        } finally {
            if (inputStream != null) {
                inputStream.close()
            }
            if (outputStream != null) {
                outputStream.close()
            }
            if(zipFile != null){
                zipFile.close()
            }
        }
    }


}