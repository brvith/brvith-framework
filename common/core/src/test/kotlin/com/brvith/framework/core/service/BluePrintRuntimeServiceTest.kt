package com.brvith.framework.core.service

import com.brvith.framework.core.factories.BluePrintParserFactory
import com.brvith.framework.core.utils.BluePrintRuntimeUtils
import com.brvith.framework.core.utils.JacksonUtils
import com.brvith.framework.core.utils.JacksonUtils.jsonNodeFromObject
import com.brvith.tosca.model.service.BluePrintRuntimeService
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.NullNode
import org.junit.Before
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class BluePrintRuntimeServiceTest {
    private val logger: Logger = LoggerFactory.getLogger(this::class.toString())
    val basepath = javaClass.classLoader.getResource(".").path +"/csar"


    @Before
    fun setUp(): Unit {

    }

    @Test
    fun testResolveNodeTemplateProperties() {
        logger.info("************************ testResolveNodeTemplateProperties **********************")
        val bluePrintContext: BluePrintContext = BluePrintParserFactory.instance(com.brvith.framework.core.BluePrintConstants.TYPE_DEFAULT)!!
                .readBlueprintFile("Definitions/activation-blueprint.json", basepath)

        val context: MutableMap<String, Any> = hashMapOf()
        context[com.brvith.framework.core.BluePrintConstants.PROPERTY_BLUEPRINT_BASE_PATH] = javaClass.classLoader.getResource(".").path + "/csar"
        val bluePrintRuntimeService = BluePrintRuntimeService(bluePrintContext, context)

        val inputNode: JsonNode = JacksonUtils.jsonNodeFromClassPathFile("data/default-context.json")
        bluePrintRuntimeService.assignInputs(inputNode)

        val propContext: MutableMap<String, Any?> = bluePrintRuntimeService.resolveNodeTemplateProperties("activate-process")
        logger.info("Context {}" ,bluePrintRuntimeService.context)

        assertNotNull(propContext, "Failed to populate interface property values")
        assertEquals(propContext.get("process-name"), jsonNodeFromObject("sample-action"), "Failed to populate parameter process-name")
        assertEquals(propContext.get("version"), jsonNodeFromObject("sample-action"), "Failed to populate parameter version")
    }

    @Test
    fun testResolveNodeTemplateInterfaceOperationInputs() {
        logger.info("************************ testResolveNodeTemplateInterfaceOperationInputs **********************")
        val bluePrintContext: BluePrintContext = BluePrintParserFactory.instance(com.brvith.framework.core.BluePrintConstants.TYPE_DEFAULT)!!
                .readBlueprintFile("Definitions/activation-blueprint.json", basepath)
        assertNotNull(bluePrintContext, "Failed to populate Blueprint context")

        val context: MutableMap<String, Any> = hashMapOf()
        context[com.brvith.framework.core.BluePrintConstants.PROPERTY_BLUEPRINT_BASE_PATH] = javaClass.classLoader.getResource(".").path + "/csar"

        BluePrintRuntimeUtils.assignInputsFromClassPathFile(bluePrintContext, "data/default-context.json", context)


        val bluePrintRuntimeService = BluePrintRuntimeService(bluePrintContext, context)

        logger.info("Prepared Context {}" ,context)

        val inContext: MutableMap<String, Any?> = bluePrintRuntimeService.resolveNodeTemplateInterfaceOperationInputs("resource-assignment",
                "DefaultComponentNode", "process")

        logger.trace("In Context {}" ,inContext)

        assertNotNull(inContext, "Failed to populate interface input property values")
        assertEquals(inContext.get("action-name"), jsonNodeFromObject("sample-action"), "Failed to populate parameter action-name")
        assertEquals(inContext.get("request-id"), jsonNodeFromObject("12345"), "Failed to populate parameter action-name")
        assertEquals(inContext.get("template-content"), jsonNodeFromObject("This is Sample Velocity Template"), "Failed to populate parameter action-name")

    }

    @Test
    fun testResolveNodeTemplateInterfaceOperationOutputs() {
        logger.info("************************ testResolveNodeTemplateInterfaceOperationOutputs **********************")
        val bluePrintContext: BluePrintContext = BluePrintParserFactory.instance(com.brvith.framework.core.BluePrintConstants.TYPE_DEFAULT)!!
                .readBlueprintFile("Definitions/activation-blueprint.json", basepath)
        assertNotNull(bluePrintContext, "Failed to populate Blueprint context")

        val context: MutableMap<String, Any> = hashMapOf()
        context[com.brvith.framework.core.BluePrintConstants.PROPERTY_BLUEPRINT_BASE_PATH] = javaClass.classLoader.getResource(".").path + "/csar"

        val bluePrintRuntimeService = BluePrintRuntimeService(bluePrintContext, context)

        val componentContext: MutableMap<String, Any?> = hashMapOf()
        val successValue : JsonNode= jsonNodeFromObject("Success")
        componentContext["resource-assignment.DefaultComponentNode.process.status"] = successValue
        componentContext["resource-assignment.DefaultComponentNode.process.resource-assignment-params"] = null

        bluePrintRuntimeService.resolveNodeTemplateInterfaceOperationOutputs("resource-assignment",
                "DefaultComponentNode", "process", componentContext)

        assertEquals(NullNode.instance,
                context.get("node_templates/resource-assignment/interfaces/DefaultComponentNode/operations/process/properties/resource-assignment-params"),
                "Failed to get operation property resource-assignment-params")

        assertEquals(successValue,
                context.get("node_templates/resource-assignment/interfaces/DefaultComponentNode/operations/process/properties/status"),
                "Failed to get operation property status")


    }
}