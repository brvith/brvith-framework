package com.brvith.framework.core.interfaces

interface ScriptExecutorService {
    fun execute(executePath : String, command: String, args : MutableList<String>? = arrayListOf(),
                returnParameters : MutableList<String>? = arrayListOf()) : MutableMap<String, Any>

    fun executeFile(executePath : String, fileName: String,  args : MutableList<String>? = arrayListOf(),
                    returnParameters : MutableList<String>? = arrayListOf()) : MutableMap<String, Any>

    fun getComponent(executePath : String,  dependencyPaths: MutableList<String>,  content: String?,
                     componentName : String, properties: MutableMap<String, Any>) : ComponentNode
}