package com.brvith.framework.core.service

import com.brvith.framework.core.factories.BluePrintParserFactory
import org.junit.Before
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class BluePrintValidatorDefaultServiceTest {
    private val logger: Logger = LoggerFactory.getLogger(this::class.toString())
    val basepath = javaClass.classLoader.getResource(".").path +"/csar"


    @Before
    fun setUp(): Unit {

    }

    @Test
    fun testValidateBluePrint() {
        val bluePrintContext: BluePrintContext = BluePrintParserFactory.instance(com.brvith.framework.core.BluePrintConstants.TYPE_DEFAULT)!!
                .readBlueprintFile("Definitions/activation-blueprint.json", basepath)
        val properties : MutableMap<String, Any> = hashMapOf()
        val validatorService = BluePrintValidatorDefaultService(bluePrintContext, properties)
        validatorService.validateBlueprint()
        logger.info("Validation Message {}", properties)
    }
}