package com.brvith.framework.adaptors.rest

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.stereotype.Component

@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "orchestrator.adaptors.rest")
class RestAdaptorProperty {
    var properties: MutableMap<String, String> = hashMapOf()

    fun getSelectorProperties(selector : String): MutableMap<String, String> {
        val matchingKey : String = selector+"."
        return properties.filter { it -> it.key.startsWith(matchingKey) }
                .mapKeys { it.key.removePrefix(matchingKey) }.toMutableMap()
    }
}