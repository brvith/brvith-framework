package com.brvith.framework.security

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.ComponentScan

@ComponentScan
@ConditionalOnProperty(prefix = "orchestrator.modules.security", name = arrayOf("enabled"),
        havingValue = "true", matchIfMissing = false)
open class SecurityAutoConfiguration