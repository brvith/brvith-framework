package com.brvith.framework.core.interfaces

import reactor.core.publisher.Mono
import java.io.File

interface BlueprintStateMachineService {

    fun load(tenantId:String, bpmnDir: String) : Mono<MutableList<File>>
    fun execute(tenantId:String, key: String, inputs: MutableMap<String, Any>) :  Mono<MutableMap<String, Any>>
    fun unDeploy(tenantId:String): Mono<MutableList<String>>
}

