package com.brvith.framework.core.utils

import com.brvith.framework.core.service.BluePrintResolverService
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.apache.commons.io.IOUtils
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.nio.charset.Charset
import kotlin.test.assertNotNull


class BluePrintResolverServiceTest {
    private val logger: Logger = LoggerFactory.getLogger(this::class.toString())
    @Test
    fun testGetImportResolvedServiceTemplate(): Unit {

        val content : String = IOUtils.toString( javaClass.classLoader.
                getResource("csar/Definitions/activation-blueprint.json"), Charset.defaultCharset())

        val serviceTemplate : com.brvith.framework.core.ServiceTemplate = jacksonObjectMapper().readValue<com.brvith.framework.core.ServiceTemplate>(content)

        val schemaImportResolverUtils = BluePrintResolverService(serviceTemplate, javaClass.classLoader.getResource("csar").path)
        val parentServiceTemplate : com.brvith.framework.core.ServiceTemplate = schemaImportResolverUtils.getImportResolvedServiceTemplate()
        assertNotNull(parentServiceTemplate, "Failed to get resolved service template")

        //logger.trace("merged Service Template  : {}", JacksonUtils.getJson(parentServiceTemplate, true))

    }
}