package com.brvith.framework.controller.service


import com.brvith.framework.controller.ControllerAutoConfiguration
import com.brvith.framework.controller.data.BlueprintIdentifier
import com.brvith.framework.controller.data.ExecutionRequest
import com.brvith.framework.controller.data.Header
import org.junit.Test
import org.junit.runner.RunWith
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(classes = arrayOf(ControllerAutoConfiguration::class))
class BluePrintExecutionServiceTest {

    val logger: Logger = LoggerFactory.getLogger(this::class.toString())

    @Autowired
    lateinit var bluePrintExecutionService: BluePrintExecutionService

    @Test
    fun testExecuteBluePrint(): Unit {

        val executionRequest: ExecutionRequest = ExecutionRequest(Header(requestId = "1234", sourceSystem = "system"),
                BlueprintIdentifier(executionMode = "sync", blueprintName = "sample-blueprint", blueprintVersion = "1.0.0", workflowName = "activate-process"))

        bluePrintExecutionService.execute(executionRequest)
    }


//
//    fun returnDummyCSAR(): Mono<CSARInfoEntity> {
//        val file : File = File(javaClass.classLoader.getResource(".").path + "sample-blueprint/1.0.0/csar.zip")
//        logger.info("Getting dummy csar file : {}",file.absolutePath )
//
//        val stream : FileInputStream = FileInputStream(file)
//        val fileChannel : FileChannel = stream.channel
//        val byteBuffer : ByteBuffer =  fileChannel.map(FileChannel.MapMode.READ_ONLY,0,fileChannel.size())
//
//        val csarInfoEntity = CSARInfoEntity(name = "sample-blueprint", tags = "",
//                updatedBy = "Brinda Santh M <brindasanth@gmail.com>",
//                version = "1.0.0", content = byteBuffer)
//        return Mono.just(csarInfoEntity)
//    }
}