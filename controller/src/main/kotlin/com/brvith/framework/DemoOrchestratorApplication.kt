package com.brvith.framework

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DemoOrchestratorApplication

fun main(args: Array<String>) {
    runApplication<DemoOrchestratorApplication>(*args)
}
