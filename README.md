# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
    * Mongo DB docker start command.
    ```
     docker run --name orchestrator-db -p 27017:27017 -d mongo
    ```
* How to run tests
* Deployment instructions
    * Maven Version Change
    ```
    mvn versions:set -DnewVersion=x.x.x-SNAPSHOT
    ```
    * Maven deploy command
    ```
    mvn clean deploy
    ```
    

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact