package com.brvith.framework.adaptors.messaging

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.stereotype.Component

@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "orchestrator.adaptors.messaging")
class MessagingProperty {
    val logger: Logger = LoggerFactory.getLogger(this::class.toString())
    var properties: MutableMap<String, String> = hashMapOf()

    fun getSelectorProperties(selector : String): MutableMap<String, String> {
        val matchingKey : String = selector+"."
        logger.trace("Matching selector {}, properties {}", selector, properties)
        return properties.filter { it -> it.key.startsWith(matchingKey) }
                .mapKeys { it.key.removePrefix(matchingKey) }.toMutableMap()
    }

}