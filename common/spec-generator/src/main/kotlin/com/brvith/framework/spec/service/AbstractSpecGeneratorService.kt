package com.brvith.framework.spec.service

import com.brvith.framework.core.service.BluePrintContext
import com.brvith.framework.spec.SpecGeneratorService
import com.brvith.framework.spec.utils.SpecGeneratorUtils
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.swagger.models.Contact
import io.swagger.models.Info
import io.swagger.models.Model
import io.swagger.models.ModelImpl
import org.apache.commons.io.FileUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.net.URL
import java.nio.charset.Charset

abstract class AbstractSpecGeneratorService : SpecGeneratorService {
    private val logger: Logger = LoggerFactory.getLogger(this::class.toString())

    private var bpc: BluePrintContext? = null

    open fun init(bluePrintContext: BluePrintContext): Unit {
        this.bpc = bluePrintContext
    }

    fun serviceTemplate(): com.brvith.framework.core.ServiceTemplate = bpc!!.serviceTemplate
    fun bluePrintContext(): BluePrintContext = bpc!!

    fun generateInfo(): Info {
        val info = Info()
        val contact = Contact()
        val metadata = bluePrintContext().metadata!!

        contact.name = metadata.get(com.brvith.framework.core.BluePrintConstants.METADATA_AUTHOR_NAME)
        contact.email = metadata.get(com.brvith.framework.core.BluePrintConstants.METADATA_AUTHOR_EMAIL)
        info.contact = contact
        info.title = metadata.get(com.brvith.framework.core.BluePrintConstants.METADATA_BLUEPRINT_NAME)
        info.description = serviceTemplate().description ?: "Blueprint for ".plus(info.title)
        info.version = metadata.get(com.brvith.framework.core.BluePrintConstants.METADATA_BLUEPRINT_VERSION)
        return info
    }

    fun populateDefaultModels(models: MutableMap<String, Model>, url :URL ) {
        logger.info("Resource {}", url)
        val content = FileUtils.readFileToString(File(url.toURI()), Charset.defaultCharset())
        val defaultDataTypes: MutableMap<String, com.brvith.framework.core.DataType>? = getMapfromJson(content)
                as MutableMap<String, com.brvith.framework.core.DataType>
        populateModelsFromDataTypes(defaultDataTypes, models)
    }

    fun populateRequestResponseModels(models: MutableMap<String, Model>, url :URL, prefix  : String ) {
        logger.info("Resource {}", url)
        var content = FileUtils.readFileToString(File(url.toURI()), Charset.defaultCharset())
        content = content.replace("\${wf-name}",prefix )
        val defaultDataTypes: MutableMap<String, com.brvith.framework.core.DataType>? = getMapfromJson(content)
                as MutableMap<String, com.brvith.framework.core.DataType>
        populateModelsFromDataTypes(defaultDataTypes, models)
    }

    fun populateModelsFromDataTypes(dataTypes: MutableMap<String, com.brvith.framework.core.DataType>?, models: MutableMap<String, Model>): Unit {
        dataTypes?.forEach { (name, dataType) ->
            val model = ModelImpl()
            model.description = dataType.description

            val properties = dataType.properties

            properties?.forEach { (propertyName, property) ->
                val defProperty = SpecGeneratorUtils.getPropery(propertyName, property)
                model.addProperty(propertyName, defProperty)
            }
            models.put(name, model)
        }
    }

    inline fun getMapfromJson(content: String): MutableMap<String, com.brvith.framework.core.DataType>? {
        val objectMapper = jacksonObjectMapper()
        val typeRef = object : TypeReference<MutableMap<String, com.brvith.framework.core.DataType>>() {}
        return objectMapper.readValue(content, typeRef)
    }
}