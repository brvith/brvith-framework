package com.brvith.framework.modules.db.service

import com.brvith.framework.modules.db.domain.BlueprintCatalog
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.io.File


interface BlueprintCatalogDBService {

    fun deployBlueprint(fileName : String, executionPath: String): Mono<String>

    fun getBlueprints(id : String): Flux<BlueprintCatalog>

    fun unDeployBlueprint(id : String): Mono<Void>

    fun prepareBlueprint(blueprintName: String, blueprintVersion: String,
                         executionPath: String ): Mono<File>

    fun downloadBlueprintFromDB(blueprintName : String,
                                blueprintVersion : String, executionPath : String): Mono<File>

    fun deCompressBlueprint(blueprintFileName: String, executionPath: String): Mono<File>
}
