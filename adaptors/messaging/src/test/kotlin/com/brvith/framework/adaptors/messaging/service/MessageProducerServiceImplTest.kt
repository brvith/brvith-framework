package com.brvith.framework.adaptors.messaging.service

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import kotlin.test.assertNotNull

@RunWith(SpringRunner::class)
@SpringBootTest(classes = arrayOf(com.brvith.framework.adaptors.messaging.MessagingAutoConfiguration::class))
class MessageProducerServiceImplTest {
    @Autowired
    lateinit var messageProducerFactory : com.brvith.framework.adaptors.messaging.MessageProducerFactory

    @Autowired
    lateinit var defaultMessageProducer: com.brvith.framework.adaptors.messaging.service.DefaultMessageProducerService

    @Test
    fun testSelectorProperties(): Unit {
        val messageProducerService = messageProducerFactory.getInstance("selector1")
        assertNotNull(messageProducerService, "Failed to get Message Consumenr for selector1")
        //defaultMessageProducer.getSender()

    }

}