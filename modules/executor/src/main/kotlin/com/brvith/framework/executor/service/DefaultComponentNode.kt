package com.brvith.framework.executor.service

import com.brvith.framework.core.interfaces.ComponentNode
import org.slf4j.Logger
import org.slf4j.LoggerFactory

//@Service("DefaultComponentNode")
open class DefaultComponentNode : ComponentNode {

    val logger: Logger = LoggerFactory.getLogger(this::class.toString())

    override fun prepare(context: MutableMap<String, Any>, componentContext: MutableMap<String, Any?>) {
        logger.info("execution prepare with context {} ", context)
    }

    override fun process(context: MutableMap<String, Any>, componentContext: MutableMap<String, Any?>) {
        logger.info("execution process with context {} ", context)
    }


}