package com.brvith.framework.spec.utils

import io.swagger.models.properties.*
import org.apache.commons.lang3.BooleanUtils

object SpecGeneratorUtils {

    fun getPropery(name: String, propertyDefinition: com.brvith.framework.core.PropertyDefinition): Property {
        var defProperty: Property? = null

        if (com.brvith.framework.core.BluePrintTypes.validPrimitiveTypes().contains(propertyDefinition.type)) {
            if (com.brvith.framework.core.BluePrintConstants.DATA_TYPE_BOOLEAN.equals(propertyDefinition.type)) {
                defProperty = BooleanProperty()
            } else if (com.brvith.framework.core.BluePrintConstants.DATA_TYPE_INTEGER.equals(propertyDefinition.type)) {
                val stringProperty = StringProperty()
                stringProperty.type = "integer"
                defProperty = stringProperty
            } else if (com.brvith.framework.core.BluePrintConstants.DATA_TYPE_FLOAT.equals(propertyDefinition.type)) {
                val stringProperty = StringProperty()
                stringProperty.format = "float"
                defProperty = stringProperty
            } else if (com.brvith.framework.core.BluePrintConstants.DATA_TYPE_TIMESTAMP.equals(propertyDefinition.type)) {
                val dateTimeProperty = DateTimeProperty()
                dateTimeProperty.format = "date-time"
                defProperty = dateTimeProperty
            } else {
                defProperty = StringProperty()
            }
        } else if (com.brvith.framework.core.BluePrintTypes.validColletionTypes().contains(propertyDefinition.type)) {
            val arrayProperty = ArrayProperty()
            if (propertyDefinition.entrySchema != null) {
                val entrySchema = propertyDefinition.entrySchema!!.type
                if (!com.brvith.framework.core.BluePrintTypes.validPrimitiveTypes().contains(entrySchema)) {
                    val innerType = RefProperty("#/definitions/" + entrySchema)
                    arrayProperty.items = innerType
                } else {
                    val innerType = StringProperty()
                    arrayProperty.items = innerType
                }
                defProperty = arrayProperty
            }

        } else {
            defProperty = RefProperty("#/definitions/" + propertyDefinition.type!!)
        }

        defProperty!!.name = name
        if (propertyDefinition.defaultValue != null) {
            defProperty.setDefault(propertyDefinition.defaultValue.toString())
        }
        propertyDefinition.defaultValue?.let { defProperty.setDefault(propertyDefinition.defaultValue?.toString()) }

        defProperty.required = BooleanUtils.isTrue(propertyDefinition.required)
        defProperty.description = propertyDefinition.description
        return defProperty
    }
}