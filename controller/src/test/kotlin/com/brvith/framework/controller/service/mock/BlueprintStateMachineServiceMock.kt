package com.brvith.framework.controller.service.mock

import com.brvith.framework.core.interfaces.BlueprintStateMachineService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import java.io.File

@Service
class BlueprintStateMachineServiceMock : BlueprintStateMachineService {

    val logger: Logger = LoggerFactory.getLogger(this::class.toString())

    override fun load(tenantId: String, bpmnDir: String): Mono<MutableList<File>> {
        logger.info("loading BPMN Files..")
        return Mono.empty()
    }

    override fun execute(tenantId: String, key: String, inputs: MutableMap<String, Any>): Mono<MutableMap<String, Any>> {
        logger.info("execute Files..")
        return Mono.empty()
    }

    override fun unDeploy(tenantId: String) : Mono<MutableList<String>> {
        logger.info("unDeploy Files..")
        return Mono.empty()
    }

}