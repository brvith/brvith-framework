package com.brvith.framework.workflowrunner.utils

import com.brvith.framework.core.utils.JacksonUtils
import org.junit.Test
import org.slf4j.LoggerFactory
import org.springframework.cloud.dataflow.core.dsl.TaskParser


class WorkFlowGraphUtilsTest {

    private val log = LoggerFactory.getLogger(WorkFlowGraphUtilsTest::class.java)

    @Test
    fun testWorkFlow2Graph() {
        val serviceTemplate = JacksonUtils.readValueFromClassPathFile("workflows/simple-workflow.json",
                com.brvith.framework.core.ServiceTemplate::class.java)!!

        serviceTemplate.topologyTemplate?.workflows?.forEach { workFlowName, workflow ->

            val workFlowGraphUtils = WorkFlowGraphUtils(workFlowName, workflow)
            val graph = workFlowGraphUtils.generateGraph()
            log.info("DSL Verbose String : {}", graph.toVerboseString())
            log.info("DSL : {}", graph.toDSLText())
        }
    }

    @Test
    fun testTaskNodeParser() {
        // <<A '*'->B '*'->C 'FAILURE'->F || E && F> && F || G> && B && D && C && D
        // "<A && <B || C> && D || E && F || G>"
        // "AAA && failedStep 'FAILED' -> EEE '*' -> FFF && <BBB||CCC> && DDD"
        val dsl = "<A '*'->B '*'->C 'FAILURE'->REC && D  || E && F || G>"
        val taskParser = TaskParser("graph-test", dsl, false, true)
        taskParser.parse()
    }
}