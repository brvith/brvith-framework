package com.brvith.framework.controller

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration

@Configuration
class ControllerProperty {

    @Value("\${orchestrator.blueprint.retrieve-type}")
    lateinit var retrieveType: String

    @Value("\${orchestrator.blueprint.deploy-path}")
    lateinit var deployPath: String

    @Value("\${orchestrator.blueprint.archive-path}")
    lateinit var archivePath: String

    @Value("\${orchestrator.blueprint.execution-path}")
    lateinit var executionPath: String

}

