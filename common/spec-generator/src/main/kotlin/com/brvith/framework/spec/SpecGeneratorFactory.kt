package com.brvith.framework.spec

import com.brvith.framework.core.service.BluePrintContext
import com.brvith.framework.spec.service.ServiceTemplateSpecGeneratorService
import com.brvith.framework.spec.service.WorkflowSpecGeneratorService

interface SpecGeneratorService {
    fun generate(bluePrintContext: BluePrintContext, context : MutableMap<String, Any>) : String
}

enum class  SpecGeneratorType {SERVICE_TEMPLATE_INPUT, WORKFLOW_INPUTS}

object SpecGeneratorFactory {
    fun instance(type : SpecGeneratorType) : SpecGeneratorService{
        return when (type) {
            SpecGeneratorType.SERVICE_TEMPLATE_INPUT -> ServiceTemplateSpecGeneratorService()
            SpecGeneratorType.WORKFLOW_INPUTS -> WorkflowSpecGeneratorService()
        }
    }
}