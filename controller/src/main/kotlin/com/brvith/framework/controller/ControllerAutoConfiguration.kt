package com.brvith.framework.controller

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan
class ControllerAutoConfiguration