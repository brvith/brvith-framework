from com.brvith.framework.core.interfaces import ComponentNode
from com.brvith.framework.core import ServiceTemplate

class SamplePythonComponent(ComponentNode):
    logger = ""
    def __init__(self):
        self.logger = eval('logger')
        return None

    def prepare(self, context, componentContext):
        self.logger.info("prepare Context {}", context)
        return None

    def process(self, context, componentContext):
        self.logger.info("process Context {}", context)
        self.logger.info("component Context {}", componentContext)
        st = ServiceTemplate()
        componentContext.put("output", "Success")
        componentContext.put("st", st)
        return None
