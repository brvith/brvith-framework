package com.brvith.framework.core.service

import com.brvith.framework.core.factories.BluePrintParserFactory
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import kotlin.test.assertNotNull


class BluePrintParserFactoryTest {
    private val logger: Logger = LoggerFactory.getLogger(this::class.toString())

    @Test
    fun testBluePrintJson() {
        val basepath = javaClass.classLoader.getResource(".").path +"/csar"

        val bluePrintContext: BluePrintContext = BluePrintParserFactory.instance(com.brvith.framework.core.BluePrintConstants.TYPE_DEFAULT)!!
                .readBlueprintFile("Definitions/activation-blueprint.json", basepath)
        assertNotNull(bluePrintContext, "Failed to populate Blueprint context")
        logger.trace("Blue Print {}",bluePrintContext.blueprintJson(true))
    }

    @Test
    fun testBluePrintYaml() {
        val basepath = javaClass.classLoader.getResource(".").path +"/csar"
        val bluePrintContext: BluePrintContext = BluePrintParserFactory.instance(com.brvith.framework.core.BluePrintConstants.TYPE_DEFAULT)!!
                .readBlueprintFile("Definitions/activation-blueprint.json", basepath)
        assertNotNull(bluePrintContext, "Failed to populate Blueprint context")
        logger.trace("Blue Print {}",bluePrintContext.blueprintYaml(true))
    }




}