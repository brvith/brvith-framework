package com.brvith.framework.adaptors.rest

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient


interface RestAdaptorService {

    fun setRestAdaptorProperties(properties: MutableMap<String, String>)

    fun getWebClient(selectorName: String): WebClient
}

@Service
class RestAdaptorFactory : ApplicationContextAware {
    val logger: Logger = LoggerFactory.getLogger(this::class.toString())
    var restAdaptorServices: MutableMap<String, com.brvith.framework.adaptors.rest.RestAdaptorService> = hashMapOf()

    @Autowired
    lateinit var restAdaptorProperty : com.brvith.framework.adaptors.rest.RestAdaptorProperty

    fun getInstance(selector: String): com.brvith.framework.adaptors.rest.RestAdaptorService? {
        val instanceNameProperty= selector+".instance"

        val instanceName = restAdaptorProperty.properties[instanceNameProperty]
                ?: throw Exception(String.format("failed to get instance name for orchestrator selector (%s)",selector))

        val restAdaptorService : com.brvith.framework.adaptors.rest.RestAdaptorService =  restAdaptorServices.get(instanceName)
                ?: throw Exception(String.format("failed to get message producer instance (%s)",instanceName))

        restAdaptorService.setRestAdaptorProperties(restAdaptorProperty.getSelectorProperties(selector))
        return restAdaptorService
    }

    fun injectInstance(instanceName: String, componentNode : com.brvith.framework.adaptors.rest.RestAdaptorService): Unit {
        restAdaptorServices[instanceName] = componentNode
    }

    override fun setApplicationContext(context: ApplicationContext) {
        restAdaptorServices = context.getBeansOfType(com.brvith.framework.adaptors.rest.RestAdaptorService::class.java)
        logger.info("Injected Rest Adaptor Services : {}", restAdaptorServices)
    }
}

