package com.brvith.framework.executor

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan
class ExecutorAutoConfiguration