package com.brvith.framework.workflowrunner.function

import org.slf4j.LoggerFactory
import org.springframework.batch.core.StepContribution
import org.springframework.batch.core.scope.context.ChunkContext
import org.springframework.batch.core.step.tasklet.Tasklet
import org.springframework.batch.repeat.RepeatStatus
import org.springframework.beans.factory.config.ConfigurableBeanFactory
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Service

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
class WorkFlowStepFunction : Tasklet {
    private val log = LoggerFactory.getLogger(WorkFlowStepFunction::class.java)

    private lateinit var taskName: String

    fun setTaskName(taskName: String) {
        this.taskName = taskName
    }

    override fun execute(stepContribution: StepContribution, chunkContext: ChunkContext): RepeatStatus {

        val stepExecutionContext = chunkContext.stepContext

        log.info("-> Executing Task : {} :: {}  ", taskName, stepExecutionContext)
        return RepeatStatus.FINISHED
    }
}