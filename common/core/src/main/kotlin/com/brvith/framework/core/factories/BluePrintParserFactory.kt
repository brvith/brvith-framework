package com.brvith.framework.core.factories

import com.brvith.framework.core.BluePrintConstants
import com.brvith.framework.core.service.BluePrintContext
import com.brvith.framework.core.service.BluePrintParserDefaultService
import org.slf4j.Logger
import org.slf4j.LoggerFactory

interface BluePrintParserService {
    fun readBlueprint(content: String) : BluePrintContext
    fun readBlueprintFile(fileName: String) : BluePrintContext
    fun readBlueprintFile(fileName: String, basePath : String) : BluePrintContext
}


object BluePrintParserFactory {
    private val logger: Logger = LoggerFactory.getLogger(this::class.toString())

    var bluePrintParserServices: MutableMap<String, BluePrintParserService> = HashMap()

    init {
        logger.info("Initialised default BluePrintParser Service ")
        bluePrintParserServices.put(BluePrintConstants.TYPE_DEFAULT, BluePrintParserDefaultService())
    }

    fun register(key:String, bluePrintParserService:BluePrintParserService){
        bluePrintParserServices.put(key, bluePrintParserService)
    }

    /**
     * Called by clients to get a Blueprint Parser for the Blueprint parser type
     */
    fun instance(key : String) : BluePrintParserService? {
        return bluePrintParserServices.get(key)
    }
}

