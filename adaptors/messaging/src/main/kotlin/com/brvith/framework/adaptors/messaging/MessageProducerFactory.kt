package com.brvith.framework.adaptors.messaging


import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.stereotype.Service


interface MessageProducerService {

    open fun setProducerProperties(properties: MutableMap<String, String>)

}

@Service
class MessageProducerFactory : ApplicationContextAware {
    val logger: Logger = LoggerFactory.getLogger(this::class.toString())
    var messageProducers: MutableMap<String, com.brvith.framework.adaptors.messaging.MessageProducerService> = hashMapOf()

    @Autowired
    lateinit var messagingProperty : com.brvith.framework.adaptors.messaging.MessagingProperty

    fun getInstance(selector: String): com.brvith.framework.adaptors.messaging.MessageProducerService? {
        val instanceNameProperty= selector+".instance"

        val instanceName = messagingProperty.properties[instanceNameProperty]
                ?: throw com.brvith.framework.adaptors.messaging.MessagingException(String.format("failed to get instance name for orchestrator selector (%s)", selector))

        val messageProducerService : com.brvith.framework.adaptors.messaging.MessageProducerService =  messageProducers.get(instanceName)
                ?: throw com.brvith.framework.adaptors.messaging.MessagingException(String.format("failed to get message producer instance (%s)", instanceName))

        messageProducerService.setProducerProperties(messagingProperty.getSelectorProperties(selector))
        return messageProducerService
    }

    fun injectInstance(instanceName: String, componentNode : com.brvith.framework.adaptors.messaging.MessageProducerService): Unit {
        messageProducers[instanceName] = componentNode
    }

    override fun setApplicationContext(context: ApplicationContext) {
        messageProducers = context.getBeansOfType(com.brvith.framework.adaptors.messaging.MessageProducerService::class.java)
        logger.info("Injected Message Producers : {}", messageProducers)
    }
}

