package com.brvith.framework.core.utils

import org.junit.Test
import kotlin.test.assertNotNull

class BluePrintMetadataUtilsTest {
    
    @Test
    fun testToscaMetaData(): Unit {

        val metadataFile : String = javaClass.classLoader.getResource(".").path + "/csar/TOSCA-Metadata/TOSCA.meta"

        val toscaMetaData : com.brvith.framework.core.ToscaMetaData =  BluePrintMetadataUtils.toscaMetaData(metadataFile)
        assertNotNull(toscaMetaData, "Missing Tosca Definition Object")
        assertNotNull(toscaMetaData.toscaMetaVersion, "Missing Tosca Metadata Version")
        assertNotNull(toscaMetaData.csarVersion, "Missing CSAR version")
        assertNotNull(toscaMetaData.createdBy, "Missing Created by")
        assertNotNull(toscaMetaData.entityDefinitions, "Missing Tosca Entity Definition")
        
    }
}