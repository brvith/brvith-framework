package com.brvith.framework.controller.service.mock

import com.brvith.framework.core.utils.ZipUtils
import com.brvith.framework.modules.db.domain.BlueprintCatalog
import com.brvith.framework.modules.db.service.BlueprintCatalogDBService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.io.File
import java.nio.charset.Charset
import kotlin.test.assertNotNull


@Service
class BlueprintCatalogDBServiceMock : BlueprintCatalogDBService {

    val logger: Logger = LoggerFactory.getLogger(this::class.toString())

    override fun deployBlueprint(fileName: String, executionPath: String): Mono<String> {
        assertNotNull(fileName, "failed to get fileName")
        assertNotNull(executionPath, "failed to get executionPath")
        logger.info("Received upload Request bluprint file name {}, execution path {}",fileName, executionPath)
        return Mono.just("1234")
    }

    override fun getBlueprints(id: String): Flux<BlueprintCatalog> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun unDeployBlueprint(id: String): Mono<Void> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun deCompressBlueprint(csarFileName: String, executionPath: String): Mono<File> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun downloadBlueprintFromDB(blueprintName: String, blueprintVersion: String, csarDownloadPath: String): Mono<File> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun prepareBlueprint(blueprintName: String, blueprintVersion: String, executionPath: String): Mono<File> {
        logger.info("prepareBlueprint Blueprint {} version ({})", blueprintName, blueprintVersion)

        assertNotNull(blueprintName, "failed to get blueprintName")
        assertNotNull(blueprintVersion, "failed to get blueprintVersion")
        assertNotNull(executionPath, "failed to get executionPath")

        // Hard Coded for Testing
        val csarFileName: String = javaClass.classLoader.getResource(".").path + "sample-blueprint/1.0.0/csar.zip"

        val excutionFile = File(executionPath)

        ZipUtils.extract(excutionFile,File(csarFileName), Charset.defaultCharset().name(), true)
        logger.info("Copied {} to  {}" , csarFileName, executionPath )
        return Mono.just(excutionFile)
    }

}