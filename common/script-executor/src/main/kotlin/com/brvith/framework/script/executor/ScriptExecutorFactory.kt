package com.brvith.framework.script.executor

import com.brvith.framework.core.interfaces.ScriptExecutorService
import com.brvith.framework.script.executor.service.DefaultExecutorService
import com.brvith.framework.script.executor.service.PythonExecutorService
import javax.script.ScriptEngineManager

enum class  ScriptExecutorType {PYTHON, SH, KOTLIN, JAVA_SCRIPT, GROOVY}

object ScriptExecutorFactory {

    fun instance(type : ScriptExecutorType) : ScriptExecutorService {
        //listEngines()

        return when (type) {
            ScriptExecutorType.PYTHON -> PythonExecutorService()
            ScriptExecutorType.SH -> DefaultExecutorService()
            ScriptExecutorType.KOTLIN -> DefaultExecutorService()
            ScriptExecutorType.JAVA_SCRIPT -> DefaultExecutorService()
            ScriptExecutorType.GROOVY -> DefaultExecutorService()
        }
    }

    fun listEngines() {
        val mgr = ScriptEngineManager()
        val factories = mgr.engineFactories
        for (factory in factories) {
            println("ScriptEngineFactory Info")

            val engName = factory.engineName
            val engVersion = factory.engineVersion
            val langName = factory.languageName
            val langVersion = factory.languageVersion

            System.out.printf("\tScript Engine: %s (%s)\n", engName, engVersion)

            val engNames = factory.names
            for (name in engNames) {
                System.out.printf("\tEngine Alias: %s\n", name)
            }
            System.out.printf("\tLanguage: %s (%s)\n", langName, langVersion)
        }
    }
}