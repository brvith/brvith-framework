package com.brvith.framework.core.service

import com.brvith.framework.core.*
import com.brvith.framework.core.factories.BluePrintValidatorService
import com.fasterxml.jackson.databind.JsonNode
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class BluePrintValidatorDefaultService(bluePrintContext: BluePrintContext, properties: MutableMap<String, Any>) : BluePrintValidatorService {
    val logger: Logger = LoggerFactory.getLogger(this::class.toString())

    var serviceTemplate: com.brvith.framework.core.ServiceTemplate = bluePrintContext.serviceTemplate
    var message: StringBuilder = StringBuilder()
    val seperator: String = "/"
    var paths: MutableList<String> = arrayListOf()

    @Throws(com.brvith.framework.core.BluePrintException::class)
    override fun validateBlueprint(){
        try {
            message.appendln("-> Config Blueprint")
            serviceTemplate.metadata?.let { validateMetadata(serviceTemplate.metadata!!) }
            serviceTemplate.artifactTypes?.let { validateArtifactTypes(serviceTemplate.artifactTypes!!) }
            serviceTemplate.dataTypes?.let { validateDataTypes(serviceTemplate.dataTypes!!) }
            serviceTemplate.nodeTypes?.let { validateNodeTypes(serviceTemplate.nodeTypes!!) }
            serviceTemplate.topologyTemplate?.let { validateTopologyTemplate(serviceTemplate.topologyTemplate!!) }
        } catch (e: Exception) {
            logger.error("validation failed in the path : {}", paths.joinToString(seperator), e)
            logger.error("validation trace message :{} ", message)
            throw com.brvith.framework.core.BluePrintException(e,
                    format("failed to validate blueprint on path ({}) with message {}"
                            , paths.joinToString(seperator), e.message))
        }
    }

    open fun validateMetadata(metaDataMap: MutableMap<String, String>){
        paths.add("metadata")
        checkNotEmptyNThrow(metaDataMap.get(com.brvith.framework.core.BluePrintConstants.METADATA_BLUEPRINT_NAME))
        checkNotEmptyNThrow(metaDataMap.get(com.brvith.framework.core.BluePrintConstants.METADATA_BLUEPRINT_VERSION))
        checkNotEmptyNThrow(metaDataMap.get(com.brvith.framework.core.BluePrintConstants.METADATA_BLUEPRINT_TAGS))
        checkNotEmptyNThrow(metaDataMap.get(com.brvith.framework.core.BluePrintConstants.METADATA_AUTHOR_EMAIL))
        checkNotEmptyNThrow(metaDataMap.get(com.brvith.framework.core.BluePrintConstants.METADATA_AUTHOR_NAME))
        checkNotEmptyNThrow(metaDataMap.get(com.brvith.framework.core.BluePrintConstants.METADATA_USER_GROUPS))
        paths.removeAt(paths.lastIndex)
    }

    private fun validateArtifactTypes(artifactTypes: MutableMap<String, com.brvith.framework.core.ArtifactType>){
        paths.add("artifact_types")
        artifactTypes.forEach { artifactName, artifactType ->
            paths.add(artifactName)
            message.appendln("--> Artifact Type :" + paths.joinToString(seperator))
            artifactType.properties?.let { validatePropertyDefinitions(artifactType.properties!!) }
            paths.removeAt(paths.lastIndex)
        }
        paths.removeAt(paths.lastIndex)
    }

    private fun validateDataTypes(dataTypes: MutableMap<String, com.brvith.framework.core.DataType>){
        paths.add("dataTypes")
        dataTypes.forEach { dataTypeName, dataType ->
            paths.add(dataTypeName)
            message.appendln("--> Data Type :" + paths.joinToString(seperator))
            dataType.properties?.let { validatePropertyDefinitions(dataType.properties!!) }
            paths.removeAt(paths.lastIndex)
        }
        paths.removeAt(paths.lastIndex)
    }

    private fun validateNodeTypes(nodeTypes: MutableMap<String, com.brvith.framework.core.NodeType>){
        paths.add("nodeTypes")
        nodeTypes.forEach { nodeTypeName, nodeType ->
            paths.add(nodeTypeName)
            message.appendln("--> Node Type :" + paths.joinToString(seperator))
            val derivedFrom: String = nodeType.derivedFrom
            nodeType.properties?.let { validatePropertyDefinitions(nodeType.properties!!) }
            nodeType.interfaces?.let { validateInterfaceDefinitions(nodeType.interfaces!!) }
            paths.removeAt(paths.lastIndex)
        }
        paths.removeAt(paths.lastIndex)
    }

    private fun validateTopologyTemplate(topologyTemplate: com.brvith.framework.core.TopologyTemplate){
        paths.add("topology")
        message.appendln("--> Topology Template")
        topologyTemplate.inputs?.let { validateInputs(topologyTemplate.inputs!!) }
        topologyTemplate.nodeTemplates?.let { validateNodeTemplates(topologyTemplate.nodeTemplates!!) }
        topologyTemplate.workflows?.let { validateWorkflows(topologyTemplate.workflows!!) }
        paths.removeAt(paths.lastIndex)
    }

    private fun validateInputs(inputs: MutableMap<String, com.brvith.framework.core.PropertyDefinition>){
        paths.add("inputs")
        message.appendln("---> Input :" + paths.joinToString(seperator))
        validatePropertyDefinitions(inputs)
        paths.removeAt(paths.lastIndex)
    }

    private fun validateNodeTemplates(nodeTemplates: MutableMap<String, com.brvith.framework.core.NodeTemplate>){
        paths.add("nodeTemplates")
        nodeTemplates.forEach { nodeTemplateName, nodeTemplate ->
            paths.add(nodeTemplateName)
            message.appendln("---> Node Template :" + paths.joinToString(seperator))
            val type: String = nodeTemplate.type

            val nodeType: com.brvith.framework.core.NodeType = serviceTemplate.nodeTypes?.get(type)
                    ?: throw com.brvith.framework.core.BluePrintException("Failed to node type definition  for node template :" + nodeTemplateName)

            nodeTemplate.artifacts?.let { validateArtifactDefinitions(nodeTemplate.artifacts!!) }
            nodeTemplate.properties?.let { validatePropertyAssignments(nodeType.properties!!, nodeTemplate.properties!!) }
            nodeTemplate.capabilities?.let { validateCapabilityAssignments(nodeTemplate.capabilities!!) }
            nodeTemplate.requirements?.let { validateRequirementAssignments(nodeTemplate.requirements!!) }
            nodeTemplate.interfaces?.let { validateInterfaceAssignments(nodeTemplate.interfaces!!) }
            paths.removeAt(paths.lastIndex)
        }
        paths.removeAt(paths.lastIndex)
    }

    private fun validateWorkflows(workflows: MutableMap<String, com.brvith.framework.core.Workflow>){
        paths.add("workflows")
        workflows.forEach { workflowName, workflow ->
            paths.add(workflowName)
            message.appendln("---> Workflow :" + paths.joinToString(seperator))
            // Step Validation Start
            paths.add("steps")
            workflow.steps?.forEach { stepName, step ->
                paths.add(stepName)
                message.appendln("----> Steps :" + paths.joinToString(seperator))
                paths.removeAt(paths.lastIndex)
            }
            paths.removeAt(paths.lastIndex)
            // Step Validation Ends
            paths.removeAt(paths.lastIndex)
        }
        paths.removeAt(paths.lastIndex)
    }

    private fun validatePropertyDefinitions(properties: MutableMap<String, com.brvith.framework.core.PropertyDefinition>){
        paths.add("properties")
        properties.forEach { propertyName, propertyDefinition ->
            paths.add(propertyName)
            val dataType: String = propertyDefinition.type!!
            if (com.brvith.framework.core.BluePrintTypes.validPrimitiveTypes().contains(dataType)) {
                // Do Nothing
            } else if (com.brvith.framework.core.BluePrintTypes.validColletionTypes().contains(dataType)) {
                val entrySchemaType: String = propertyDefinition.entrySchema?.type
                        ?: throw com.brvith.framework.core.BluePrintException(format("Entry schema for data type ({}) for the property ({}) not found", dataType, propertyName))
                checkPrimitiveOrComplex(entrySchemaType, propertyName)
            } else {
                checkPropertyDataType(dataType, propertyName)
            }
            message.appendln("property " + paths.joinToString(seperator) + " of type " + dataType)
            paths.removeAt(paths.lastIndex)
        }
        paths.removeAt(paths.lastIndex)
    }

    private fun validatePropertyAssignments(nodeTypeProperties: MutableMap<String, com.brvith.framework.core.PropertyDefinition>,
                                            properties: MutableMap<String, JsonNode>){
        properties.forEach { propertyName, propertyAssignment ->
            val propertyDefinition: com.brvith.framework.core.PropertyDefinition = nodeTypeProperties.get(propertyName)
                    ?: throw com.brvith.framework.core.BluePrintException(format("failed to get definition for the property ({})", propertyName))
            // Check and Validate if Expression Node
            val expressionData = BluePrintExpressionService.getExpressionData(propertyAssignment)
            if(!expressionData.isExpression){
                checkPropertyValue(propertyDefinition, propertyAssignment)
            }
        }
    }

    private fun validateArtifactDefinitions(artifacts: MutableMap<String, com.brvith.framework.core.ArtifactDefinition>){
        paths.add("artifacts")
        artifacts.forEach { artifactName, artifactDefinition ->
            paths.add(artifactName)
            message.appendln("Validating artifact " + paths.joinToString(seperator))
            val type: String = artifactDefinition.type
                    ?: throw com.brvith.framework.core.BluePrintException("type is missing for artifact definition :" + artifactName)
            val file: String = artifactDefinition.file
                    ?: throw com.brvith.framework.core.BluePrintException("file is missing for artifact definition :" + artifactName)
            serviceTemplate.artifactTypes?.containsKey(type)
                    ?: throw com.brvith.framework.core.BluePrintException("Failed to node type definition  for artifact definition :" + artifactName)
            paths.removeAt(paths.lastIndex)
        }
        paths.removeAt(paths.lastIndex)
    }

    private fun validateCapabilityAssignments(capabilities: MutableMap<String, com.brvith.framework.core.CapabilityAssignment>){

    }

    private fun validateRequirementAssignments(requirements: MutableMap<String, com.brvith.framework.core.RequirementAssignment>){

    }

    private fun validateInterfaceAssignments(interfaces: MutableMap<String, com.brvith.framework.core.InterfaceAssignment>){

    }

    private fun validateInterfaceDefinitions(interfaces: MutableMap<String, com.brvith.framework.core.InterfaceDefinition>){
        paths.add("interfaces")
        interfaces.forEach { interfaceName, interfaceDefinition ->
            paths.add(interfaceName)
            message.appendln("Validating : " + paths.joinToString(seperator))
            interfaceDefinition.operations?.let { validateOperationDefinitions(interfaceDefinition.operations!!) }
            paths.removeAt(paths.lastIndex)
        }
        paths.removeAt(paths.lastIndex)
    }

    private fun validateOperationDefinitions(operations: MutableMap<String, com.brvith.framework.core.OperationDefinition>){
        paths.add("operations")
        operations.forEach { opertaionName, operationDefinition ->
            paths.add(opertaionName)
            message.appendln("Validating : " + paths.joinToString(seperator))
            operationDefinition.implementation?.let { validateImplementation(operationDefinition.implementation!!) }
            operationDefinition.inputs?.let { validatePropertyDefinitions(operationDefinition.inputs!!) }
            operationDefinition.outputs?.let { validatePropertyDefinitions(operationDefinition.outputs!!) }
            paths.removeAt(paths.lastIndex)
        }
        paths.removeAt(paths.lastIndex)
    }

    private fun validateImplementation(implementation: com.brvith.framework.core.Implementation){

    }


    private fun checkPropertyValue(propertyDefinition : com.brvith.framework.core.PropertyDefinition, jsonNode : JsonNode){
        logger.info("validating path ({}), Property {}, value ({})", paths, propertyDefinition,jsonNode )
    }

    private fun checkPropertyDataType(dataType: String, propertyName: String): Boolean {
        if (checkDataType(dataType)) {
            return true
        } else {
            throw com.brvith.framework.core.BluePrintException(format("Data type ({}) for the property ({}) not found", dataType, propertyName))
        }
    }

    private fun checkPrimitiveOrComplex(dataType: String, propertyName: String): Boolean {
        if (com.brvith.framework.core.BluePrintTypes.validPrimitiveTypes().contains(dataType) || checkDataType(dataType)) {
            return true
        } else {
            throw com.brvith.framework.core.BluePrintException(format("Data type ({}) for the property ({}) is not valid", dataType))
        }
    }

    private fun checkDataType(key: String): Boolean {
        return serviceTemplate.dataTypes?.containsKey(key) ?: false
    }

    private fun checkNodeType(key: String): Boolean {
        return serviceTemplate.nodeTypes?.containsKey(key) ?: false
    }

}