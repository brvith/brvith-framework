package com.brvith.framework.adaptors.messaging.service

import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.common.serialization.IntegerSerializer
import org.apache.kafka.common.serialization.StringSerializer
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import reactor.kafka.sender.KafkaSender
import reactor.kafka.sender.SenderOptions


@Service("defaultMessageProducer")
class DefaultMessageProducerService : com.brvith.framework.adaptors.messaging.MessageProducerService {

    val logger: Logger = LoggerFactory.getLogger(this::class.toString())
    var properties: MutableMap<String, String> = hashMapOf()

    override fun setProducerProperties(properties: MutableMap<String, String>) {
        this.properties = properties
        logger.info("Selector properties : {}", this.properties)
    }

    fun getSender(): KafkaSender<Integer, String> {
        val props :MutableMap<String, Any> = hashMapOf()
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, properties[com.brvith.framework.adaptors.messaging.MessagingConstants.BOOTSTRAP_SERVERS_CONFIG] as String)
        props.put(ConsumerConfig.CLIENT_ID_CONFIG, properties[com.brvith.framework.adaptors.messaging.MessagingConstants.CLIENT_ID_CONFIG] as String)
        props.put(ConsumerConfig.GROUP_ID_CONFIG, properties[com.brvith.framework.adaptors.messaging.MessagingConstants.GROUP_ID_CONFIG] as String)
        props.put(ProducerConfig.ACKS_CONFIG, "all")
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, IntegerSerializer::class.java)
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer::class.java)
        val senderOptions = SenderOptions.create<Integer, String>(props)
        val sender : KafkaSender<Integer, String>  = KafkaSender.create(senderOptions)
        return sender
    }

}