package com.brvith.framework.core

object BluePrintConstants {

    const val TYPE_DEFAULT: String = "default"

    const val DATA_TYPE_STRING: String = "string"
    const val DATA_TYPE_INTEGER: String = "integer"
    const val DATA_TYPE_FLOAT: String = "float"
    const val DATA_TYPE_BOOLEAN: String = "boolean"
    const val DATA_TYPE_TIMESTAMP: String = "timestamp"
    const val DATA_TYPE_NULL: String = "null"
    const val DATA_TYPE_LIST: String = "list"
    const val DATA_TYPE_MAP: String = "map"

    const val USER_SYSTEM: String = "System"

    const val MODEL_CONTENT_TYPE_JSON: String = "JSON"
    const val MODEL_CONTENT_TYPE_YAML: String = "YAML"
    const val MODEL_CONTENT_TYPE_YANG: String = "YANG"
    const val MODEL_CONTENT_TYPE_SCHEMA: String = "SCHEMA"

    const val MODEL_DEFINITION_TYPE_NODE_TYPE: String = "node_types"
    const val MODEL_DEFINITION_TYPE_ARTIFACT_TYPE: String = "artifact_type"
    const val MODEL_DEFINITION_TYPE_CAPABILITY_TYPE: String = "capability_type"
    const val MODEL_DEFINITION_TYPE_RELATIONSHIP_TYPE: String = "relationship_type"
    const val MODEL_DEFINITION_TYPE_DATA_TYPE: String = "data_type"

    const val PATH_DIVIDER: String = "/"
    const val PATH_INPUTS: String = "inputs"
    const val PATH_NODE_WORKFLOWS: String = "workflows"
    const val PATH_NODE_TEMPLATES: String = "node_templates"
    const val PATH_CAPABILITIES: String = "capabilities"
    const val PATH_REQUIREMENTS: String = "requirements"
    const val PATH_INTERFACES: String = "interfaces"
    const val PATH_OPERATIONS: String = "operations"
    const val PATH_OUTPUTS: String = "outputs"
    const val PATH_PROPERTIES: String = "properties"
    const val PATH_ATTRIBUTES: String = "attributes"
    const val PATH_ARTIFACTS: String = "artifacts"

    const val ROOT_DATATYPE: String = "tosca.datatypes.Root"
    const val ROOT_NODES: String = "tosca.nodes.Root"
    const val ROOT_GROUPS: String = "tosca.groups.Root"
    const val ROOT_RELATIONSHIPS: String = "tosca.relationships.Root"
    const val ROOT_ARTIFACTS: String = "tosca.artifacts.Root"
    const val ROOT_CAPABILITIES: String = "tosca.capabilities.Root"
    const val ROOT_INTERFACES: String = "tosca.interfaces.Root"

    const val CUSTOM_NODETYPE_COMPONENT_JAVA: String = "tosca.nodes.component.Java"
    const val CUSTOM_NODETYPE_COMPONENT_SCRIPT: String = "tosca.nodes.component.Script"
    const val CUSTOM_NODETYPE_COMPONENT_PYTHON: String = "tosca.nodes.component.Python"
    const val CUSTOM_NODETYPE_COMPONENT_JAVA_SCRIPT: String = "tosca.nodes.component.JavaScript"

    const val EXPRESSION_GET_INPUT: String = "get_input"
    const val EXPRESSION_GET_ATTRIBUTE: String = "get_attribute"
    const val EXPRESSION_GET_ARTIFACT: String = "get_artifact"
    const val EXPRESSION_GET_PROPERTY: String = "get_property"
    const val EXPRESSION_GET_OPERATION_OUTPUT: String = "get_operation_output"
    const val EXPRESSION_GET_NODE_OF_TYPE: String = "get_nodes_of_type"

    const val PROPERTY_BLUEPRINT_PROCESS_ID: String = "blueprint-process-id"
    const val PROPERTY_BLUEPRINT_BASE_PATH: String = "blueprint-basePath"
    const val PROPERTY_BLUEPRINT_RUNTIME: String = "blueprint-runtime"
    const val PROPERTY_BLUEPRINT_INPUTS_DATA: String = "blueprint-inputs-data"
    const val PROPERTY_BLUEPRINT_CONTEXT: String = "blueprint-context"
    const val PROPERTY_BLUEPRINT_NAME: String = "blueprint-name"
    const val PROPERTY_BLUEPRINT_VERSION: String = "blueprint-version"

    const val DEFAULT_CSAR_FILE_NAME: String = "csar.zip"
    const val DEFAULT_TOSCA_METADATA_ENTRY_DEFINITION_FILE: String = "TOSCA-Metadata/TOSCA.meta"
    const val DEFAULT_TOSCA_PLAN_DIR: String = "Plans"
    const val DEFAULT_TOSCA_SCRIPT_DIR: String = "Scripts"

    const val METADATA_AUTHOR_NAME = "author-name"
    const val METADATA_AUTHOR_EMAIL = "author-email"
    const val METADATA_USER_GROUPS = "user-groups"
    const val METADATA_BLUEPRINT_NAME = "blueprint-name"
    const val METADATA_BLUEPRINT_VERSION = "blueprint-version"
    const val METADATA_BLUEPRINT_TAGS = "blueprint-tags"


    const val PAYLOAD_CONTENT = "payload-content"
    const val PAYLOAD_DATA = "payload-data"
    const val SELECTOR = "selector"
    const val PROPERTY_CURRENT_INTERFACE = "current-interface"
    const val PROPERTY_CURRENT_OPERATION = "current-operation"

    const val PROPERTY_ACTION_NAME = "action"

    const val OPERATION_PROCESS = "process"
    const val OPERATION_PREPARE = "prepare"

    const val BLUEPRINT_RETRIEVE_TYPE_DB = "db"
    const val BLUEPRINT_RETRIEVE_TYPE_FILE = "file"
    const val BLUEPRINT_RETRIEVE_TYPE_REPO = "repo"

}