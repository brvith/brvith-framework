package com.brvith.framework.executor.service

import com.brvith.framework.core.factories.BluePrintParserFactory
import com.brvith.framework.core.service.BluePrintContext
import com.brvith.framework.core.utils.BluePrintRuntimeUtils
import com.brvith.framework.executor.ExecutorAutoConfiguration
import org.junit.Test
import org.junit.runner.RunWith
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(classes = arrayOf(ExecutorAutoConfiguration::class))
class ComponentExecutorTest {

    val logger: Logger = LoggerFactory.getLogger(this::class.java)

    val blueprintBasePath = this.javaClass.classLoader.getResource(".")!!.path + "csar/1234"

    @Autowired
    lateinit var componentExecutor: ComponentExecutor

    @Autowired
    lateinit var javaComponentNodeFactory: JavaComponentNodeFactory

    @Test
    fun testJavaExecute() {
        val bluePrintContext: BluePrintContext = BluePrintParserFactory.instance(com.brvith.framework.core.BluePrintConstants.TYPE_DEFAULT)!!
                .readBlueprintFile("Definitions/activation-blueprint.json", blueprintBasePath)

        val context: MutableMap<String, Any> = hashMapOf()
        context[com.brvith.framework.core.BluePrintConstants.PROPERTY_BLUEPRINT_BASE_PATH] = blueprintBasePath
        context[com.brvith.framework.core.BluePrintConstants.PROPERTY_BLUEPRINT_CONTEXT] = bluePrintContext
        context[com.brvith.framework.core.BluePrintConstants.PROPERTY_BLUEPRINT_PROCESS_ID] = "1234"
        context[com.brvith.framework.core.BluePrintConstants.SELECTOR] = "resource-assignment"

        BluePrintRuntimeUtils.assignInputsFromClassPathFile(bluePrintContext, "data/default-context.json", context)

        javaComponentNodeFactory.injectInstance("DefaultComponentNode", DefaultComponentNode())

        componentExecutor.execute(context)

    }

    @Test
    fun testPythonExecute() {
        val bluePrintContext: BluePrintContext = BluePrintParserFactory.instance(com.brvith.framework.core.BluePrintConstants.TYPE_DEFAULT)!!
                .readBlueprintFile("Definitions/activation-blueprint-py.json", blueprintBasePath)

        val context: MutableMap<String, Any> = hashMapOf()
        context[com.brvith.framework.core.BluePrintConstants.PROPERTY_BLUEPRINT_BASE_PATH] = blueprintBasePath
        context[com.brvith.framework.core.BluePrintConstants.PROPERTY_BLUEPRINT_CONTEXT] = bluePrintContext
        context[com.brvith.framework.core.BluePrintConstants.PROPERTY_BLUEPRINT_PROCESS_ID] = "1234"
        context[com.brvith.framework.core.BluePrintConstants.SELECTOR] = "resource-assignment-py"

        BluePrintRuntimeUtils.assignInputsFromClassPathFile(bluePrintContext, "data/default-context.json", context)

        componentExecutor.execute(context)

    }

}