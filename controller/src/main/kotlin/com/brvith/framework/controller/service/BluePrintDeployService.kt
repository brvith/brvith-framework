package com.brvith.framework.controller.service

import com.brvith.framework.controller.ControllerProperty
import com.brvith.framework.controller.data.Status
import com.brvith.framework.modules.db.service.BlueprintCatalogDBService
import org.apache.commons.io.FileUtils
import org.apache.commons.io.FilenameUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.MediaType
import org.springframework.http.codec.multipart.FilePart
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.BodyExtractors
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import java.io.File
import java.util.*

@Service
class BluePrintDeployService {
    val logger: Logger = LoggerFactory.getLogger(this::class.toString())
    var controllerProperty: ControllerProperty
    var blueprintCatalogDBService: BlueprintCatalogDBService

    constructor(controllerProperty: ControllerProperty,
                blueprintCatalogDBService: BlueprintCatalogDBService) {
        this.controllerProperty = controllerProperty
        this.blueprintCatalogDBService = blueprintCatalogDBService
    }

    fun deployBlueprint(request: ServerRequest): Mono<ServerResponse> {
        logger.info("received BluePrintExecutionService deploy request {}", request)

        return request.body(BodyExtractors.toMultipartData()).flatMap { multipart ->

            val requestId: String = UUID.randomUUID().toString()

            val executionPath = controllerProperty.executionPath.plus(File.separator).plus(requestId)

            val deployBlueprintFileName = executionPath.plus(File.separator).plus("csar.zip")

            val filePart: FilePart = multipart.getFirst("csar-file") as FilePart
            logger.info("Blueprint file {} to store in {}", filePart.filename(), deployBlueprintFileName)

            var dirPath = FilenameUtils.getPath(deployBlueprintFileName)

            FileUtils.forceMkdir(File(dirPath))

            val deployBlueprintFile = File(deployBlueprintFileName)
            deployBlueprintFile.createNewFile()

            filePart.transferTo(deployBlueprintFile)
                    .doOnSuccess {
                        logger.info("Blueprint file saved successfully {}", deployBlueprintFile.absolutePath)
                    }.onErrorMap { e ->
                        com.brvith.framework.core.BlueprintProcessException(e)
                    }.compose {
                        blueprintCatalogDBService.deployBlueprint(deployBlueprintFile.absolutePath, executionPath)
                    }.doOnSuccess { blueprintId ->
                        logger.info("Blueprint {} uploaded successfully", blueprintId)
                    }.doFinally {
                        FileUtils.forceDeleteOnExit(File(executionPath))
                        logger.info("Blueprint execution path {} cleaned", executionPath)
                    }.subscribe()

            val status: Status = Status(code = "200", message = "Success")

            ServerResponse.ok()
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .body(BodyInserters.fromObject(status))

        }
    }

    fun unDeployBlueprint(request: ServerRequest): Mono<ServerResponse> {
        val id : String = request.pathVariable("id")

        val status: Status = Status(code = "200", message = "Success")

        return ServerResponse.ok()
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(BodyInserters.fromObject(status))
    }

}