package com.brvith.framework.spec.service

import com.brvith.framework.core.service.BluePrintContext
import com.brvith.framework.spec.utils.SpecGeneratorUtils
import com.google.common.io.Resources
import io.swagger.models.*
import io.swagger.models.parameters.BodyParameter
import io.swagger.models.parameters.Parameter
import io.swagger.models.properties.RefProperty
import io.swagger.util.Json

open class WorkflowSpecGeneratorService : AbstractSpecGeneratorService(){

    override fun generate(bluePrintContext: BluePrintContext, context: MutableMap<String, Any>): String {
        init(bluePrintContext)
        var swaggerContent = ""
        val swagger = Swagger().info(generateInfo())
        swagger.paths = generatePaths()
        swagger.definitions = generateDefinition()
        swaggerContent = Json.pretty(swagger)
        return swaggerContent
    }

    fun generatePaths(): MutableMap<String, Path> {
        var paths: MutableMap<String, Path> = hashMapOf()

        bluePrintContext().workflows?.forEach { (name, workflow) ->
            val pathName = "/blueprint/".plus(name)
            val requestName = name.plus("-request")
            val responseName = name.plus("-response")
            val path = Path()
            val post = Operation()
            post.operationId = name
            post.consumes = arrayListOf("application/json", "application/xml")
            post.produces = arrayListOf("application/json", "application/xml")

            val parameters: MutableList<Parameter> = arrayListOf()
            val request = BodyParameter().schema(RefModel("#/definitions/"+name+"-request"))
            request.required = true
            request.name = "request"
            parameters.add(request)
            post.parameters = parameters

            var responses: MutableMap<String, Response> = hashMapOf()
            val response = Response().description("Success")
            response.schema = RefProperty("#/definitions/"+name+"-response")
            responses.put("200", response)

            val failureResponse = Response().description("Failure")
            responses.put("400", failureResponse)
            post.responses = responses

            path.post = post
            paths.put(pathName, path)

        }
        return paths
    }

    fun generateDefinition(): MutableMap<String, Model> {

        val models: MutableMap<String, Model> = hashMapOf()

        val url = Resources.getResource("templates/workflow-input-pd.json")
        populateDefaultModels(models, url)

        val requestResponseUrl = Resources.getResource("templates/workflow-input-request.vtl")


        bluePrintContext().workflows?.forEach { (name, workflow) ->
            // Populate for Request Response
            populateRequestResponseModels(models, requestResponseUrl, name)

            val requestName = name.plus("-request-data")
            val responseName = name.plus("-response-data")

            val inputModel = ModelImpl()
            inputModel.title = requestName
            val inputs = workflow.inputs

            inputs?.forEach { (propertyName, property) ->
                val defProperty = SpecGeneratorUtils.getPropery(propertyName, property)
                inputModel.property(propertyName, defProperty)
            }
            models.put(requestName, inputModel)

            val outputModel = ModelImpl()
            outputModel.title = responseName
            models.put(responseName, outputModel)
        }

        val dataTypes = serviceTemplate().dataTypes

        // Populate Definitions for DataTypes
        populateModelsFromDataTypes(dataTypes, models)

        return models
    }
}