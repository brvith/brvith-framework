package com.brvith.framework.core.service

import com.brvith.framework.core.utils.ServiceTemplateUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.net.URL
import java.net.URLDecoder
import java.nio.charset.Charset

class BluePrintResolverService {

    private val logger: Logger = LoggerFactory.getLogger(this::class.toString())
    val PARENT_SERVICE_TEMPLATE : String = "parent"


    var parentServiceTemplate : com.brvith.framework.core.ServiceTemplate
    var schemaBasePath : String
    var importServiceTemplateMap: MutableMap<String, com.brvith.framework.core.ServiceTemplate> = hashMapOf()

    constructor(parentServiceTemplate : com.brvith.framework.core.ServiceTemplate, schemaBasePath : String){
        this.parentServiceTemplate = parentServiceTemplate
        this.schemaBasePath = schemaBasePath
    }

    fun getImportResolvedServiceTemplate(): com.brvith.framework.core.ServiceTemplate {
        // Populate Imported Service Templates
        traverseSchema(PARENT_SERVICE_TEMPLATE, parentServiceTemplate)

        importServiceTemplateMap.forEach { key, serviceTemplate ->
            BluePrintMergeService.merge(parentServiceTemplate, serviceTemplate)
            logger.info("merged service template ({})", key)
        }
        return parentServiceTemplate
    }

    private fun traverseSchema(key: String, serviceTemplate : com.brvith.framework.core.ServiceTemplate): Unit {
        if(!key.equals(PARENT_SERVICE_TEMPLATE)){
            importServiceTemplateMap.put(key, serviceTemplate)
        }
        val  imports : List<com.brvith.framework.core.ImportDefinition>? = serviceTemplate.imports

        imports?.let {
            serviceTemplate.imports?.forEach { importDefinition  ->
                val childServiceTemplate = resolveImportDefinition(importDefinition)
                var keyName : String = importDefinition.file
                traverseSchema(keyName, childServiceTemplate )
            }
        }
    }

    fun resolveImportDefinition(importDefinition : com.brvith.framework.core.ImportDefinition): com.brvith.framework.core.ServiceTemplate {
        var serviceTemplate : com.brvith.framework.core.ServiceTemplate? = null
        val file : String = importDefinition.file
        val decodedSystemId : String = URLDecoder.decode(file,  Charset.defaultCharset().toString())
        logger.trace("file ({}), decodedSystemId ({}) ", file, decodedSystemId)
        try{
            if(decodedSystemId.startsWith("http", true)
                    || decodedSystemId.startsWith("https", true)){
                val givenUrl : String = URL(decodedSystemId).toString()
                val systemUrl : String = File(".").toURI().toURL().toString()
                logger.trace("givenUrl ({}), systemUrl ({}) ", givenUrl, systemUrl)
                if(givenUrl.startsWith(systemUrl)){

                }
            }else{
                if(!decodedSystemId.startsWith("/")){
                    importDefinition.file = StringBuilder().append(schemaBasePath).append(File.separator).append(file).toString()
                }
                serviceTemplate = ServiceTemplateUtils.getServiceTemplate(importDefinition.file)
            }
        }catch (e : Exception){
            throw com.brvith.framework.core.BluePrintException(String.format("failed to populate service template for : (%s)" + importDefinition.file), e)
        }
        if (serviceTemplate == null) {
            throw com.brvith.framework.core.BluePrintException(String.format("failed to populate service template for : (%s)" + importDefinition.file))
        }
        return serviceTemplate
    }

/*
    fun getUrlResource(importDefinition : ImportDefinition): ServiceTemplate {
        
    }

    fun getClassPathResource(importDefinition : ImportDefinition): ServiceTemplate {
        
    }

    */

}