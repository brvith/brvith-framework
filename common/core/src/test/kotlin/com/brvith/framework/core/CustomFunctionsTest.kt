package com.brvith.framework.core

import org.junit.Test
import kotlin.test.assertEquals

class CustomFunctionsTest {
    @Test
    fun testFormat(): Unit {
        val returnValue : String = format("This is {} for times {}", "test", 2)
        assertEquals("This is test for times 2", returnValue, "Failed to format String")

        val returnValue1 : String = format("This is test for times 2")
        assertEquals("This is test for times 2", returnValue1, "Failed to format empty args")
    }
}