package com.brvith.framework.workflowrunner.listener

import org.slf4j.LoggerFactory
import org.springframework.batch.core.JobExecution
import org.springframework.batch.core.JobExecutionListener
import org.springframework.beans.factory.config.ConfigurableBeanFactory
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Service

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
class WorkFlowJobExecutionListener : JobExecutionListener {
    private val log = LoggerFactory.getLogger(WorkFlowJobExecutionListener::class.java)

    override fun beforeJob(jobExecution: JobExecution) {

        jobExecution.executionContext.put("blueprint-name", "sample-blueprint")
        log.info(" JOb STart : {}", jobExecution.executionContext)
    }

    override fun afterJob(jobExecution: JobExecution) {
        log.info(" JOb End : {}", jobExecution.executionContext)
    }
}