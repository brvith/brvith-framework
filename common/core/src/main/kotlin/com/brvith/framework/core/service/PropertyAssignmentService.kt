package com.brvith.framework.core.service

import com.brvith.framework.core.format
import com.brvith.framework.core.utils.JacksonUtils
import com.brvith.framework.core.utils.ResourceResolverUtils
import com.brvith.tosca.model.service.BluePrintRuntimeService
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.NullNode
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class PropertyAssignmentService(var bluePrintContext: BluePrintContext, var context: MutableMap<String, Any>,
                                var bluePrintRuntimeService: BluePrintRuntimeService) {
    private val logger: Logger = LoggerFactory.getLogger(this::class.toString())
/*

If Property Assignment is Expression.
    Get the Expression
    Recurssely resolve the expression
 */

    fun resolveAssignmentExpression(nodeTemplateName: String, assignmentName: String,
                                            assignment: Any): JsonNode {
        var valueNode: JsonNode = NullNode.getInstance()
        logger.trace("Assignment ({})", assignment)
        val expressionData = BluePrintExpressionService.getExpressionData(assignment)

        if (expressionData.isExpression) {
            valueNode = resolveExpression(nodeTemplateName, assignmentName, expressionData)
        } else {
            valueNode = expressionData.valueNode
        }
        return valueNode
    }

    fun resolveExpression(nodeTemplateName: String, propName: String, expressionData: com.brvith.framework.core.ExpressionData): JsonNode {
        var valueNode: JsonNode = NullNode.getInstance()

        if(expressionData.isExpression) {
            val command = expressionData.command!!

            when(command){
                com.brvith.framework.core.BluePrintConstants.EXPRESSION_GET_INPUT ->{
                    valueNode = bluePrintRuntimeService.getInputValue(expressionData.inputExpression?.propertyName!!)
                }
                com.brvith.framework.core.BluePrintConstants.EXPRESSION_GET_ATTRIBUTE ->{
                    valueNode = resolveAttributeExpression(nodeTemplateName, expressionData.attributeExpression!!)
                }
                com.brvith.framework.core.BluePrintConstants.EXPRESSION_GET_PROPERTY ->{
                    valueNode = resolvePropertyExpression(nodeTemplateName, expressionData.propertyExpression!!)
                }
                com.brvith.framework.core.BluePrintConstants.EXPRESSION_GET_OPERATION_OUTPUT ->{
                    valueNode = resolveOperationOutputExpression(nodeTemplateName, expressionData.operationOutputExpression!!)
                }
                com.brvith.framework.core.BluePrintConstants.EXPRESSION_GET_ARTIFACT ->{
                    valueNode = resolveArtifactExpression(nodeTemplateName, expressionData.artifactExpression!!)
                }
                com.brvith.framework.core.BluePrintConstants.EXPRESSION_GET_NODE_OF_TYPE ->{

                }
                else ->{
                    throw com.brvith.framework.core.BluePrintException(format("for property ({}), command ({}) is not supported ", propName, command))
                }
            }
        }
        return valueNode
    }

    /*
    get_property: [ <modelable_entity_name>, <optional_req_or_cap_name>, <property_name>,
    <nested_property_name_or_index_1>, ..., <nested_property_name_or_index_n> ]
 */
    fun resolveAttributeExpression(nodeTemplateName: String, attributeExpression: com.brvith.framework.core.AttributeExpression): JsonNode {
        var valueNode: JsonNode = NullNode.getInstance()

        val attributeName = attributeExpression.attributeName
        val subAttributeName: String? = attributeExpression.subAttributeName

        var attributeNodeTemplateName = nodeTemplateName
        if (!attributeExpression.modelableEntityName.equals("SELF", true)) {
            attributeNodeTemplateName = attributeExpression.modelableEntityName
        }

        var attributeExpression = bluePrintContext.nodeTemplateByName(attributeNodeTemplateName).attributes?.get(attributeName)
                ?: throw com.brvith.framework.core.BluePrintException(format("failed to get property definitions for node template ({})'s property name ({}) ", nodeTemplateName, attributeName))

        var propertyDefinition: com.brvith.framework.core.AttributeDefinition = bluePrintContext.nodeTemplateNodeType(attributeNodeTemplateName).attributes?.get(attributeName)!!

        logger.info("template name ({}), property Name ({}) resolved value ({})", attributeNodeTemplateName, attributeName, attributeExpression)

        // Check it it is a nested expression
        valueNode = resolveAssignmentExpression(attributeNodeTemplateName, attributeName, attributeExpression)

//        subPropertyName?.let {
//            valueNode = valueNode.at(JsonPointer.valueOf(subPropertyName))
//        }
        return valueNode
    }

    /*
        get_property: [ <modelable_entity_name>, <optional_req_or_cap_name>, <property_name>,
        <nested_property_name_or_index_1>, ..., <nested_property_name_or_index_n> ]
     */
    fun resolvePropertyExpression(nodeTemplateName: String, propertyExpression: com.brvith.framework.core.PropertyExpression): JsonNode {
        var valueNode: JsonNode = NullNode.getInstance()

        val propertyName = propertyExpression.propertyName
        val subPropertyName: String? = propertyExpression.subPropertyName

        var propertyNodeTemplateName = nodeTemplateName
        if (!propertyExpression.modelableEntityName.equals("SELF", true)) {
            propertyNodeTemplateName = propertyExpression.modelableEntityName
        }

        var propertyExpression = bluePrintContext.nodeTemplateByName(propertyNodeTemplateName).properties?.get(propertyName)
                ?: throw com.brvith.framework.core.BluePrintException(format("failed to get property definitions for node template ({})'s property name ({}) ", nodeTemplateName, propertyName))

        var propertyDefinition: com.brvith.framework.core.PropertyDefinition = bluePrintContext.nodeTemplateNodeType(propertyNodeTemplateName).properties?.get(propertyName)!!

        logger.info("template name ({}), property Name ({}) resolved value ({})", propertyNodeTemplateName, propertyName, propertyExpression)

        // Check it it is a nested expression
        valueNode = resolveAssignmentExpression(propertyNodeTemplateName, propertyName, propertyExpression)

//        subPropertyName?.let {
//            valueNode = valueNode.at(JsonPointer.valueOf(subPropertyName))
//        }
        return valueNode
    }

    /*
    get_operation_output: <modelable_entity_name>, <interface_name>, <operation_name>, <output_variable_name>
     */
    fun resolveOperationOutputExpression(nodeTemplateName: String, operationOutputExpression: com.brvith.framework.core.OperationOutputExpression): JsonNode {
        var outputNodeTemplateName = nodeTemplateName
        if (!operationOutputExpression.modelableEntityName.equals("SELF", true)) {
            outputNodeTemplateName = operationOutputExpression.modelableEntityName
        }
        return bluePrintRuntimeService.getNodeTemplateOperationOutputValue(outputNodeTemplateName,
                operationOutputExpression.interfaceName, operationOutputExpression.operationName,
                operationOutputExpression.propertyName)
    }

    /*
    get_artifact: [ <modelable_entity_name>, <artifact_name>, <location>, <remove> ]
     */
    fun resolveArtifactExpression(nodeTemplateName: String,  artifactExpression: com.brvith.framework.core.ArtifactExpression): JsonNode {

        var artifactNodeTemplateName = nodeTemplateName
        if (!artifactExpression.modelableEntityName.equals("SELF", true)) {
            artifactNodeTemplateName = artifactExpression.modelableEntityName
        }
        val artifactDefinition: com.brvith.framework.core.ArtifactDefinition = bluePrintContext.nodeTemplateByName(artifactNodeTemplateName)
                .artifacts?.get(artifactExpression.artifactName)
                ?: throw com.brvith.framework.core.BluePrintException(format("failed to get artifact definitions for node template ({})'s " +
                        "artifact name ({}) ", nodeTemplateName, artifactExpression.artifactName))

        return JacksonUtils.jsonNodeFromObject(artifactContent(artifactDefinition))
    }

    fun artifactContent(artifactDefinition: com.brvith.framework.core.ArtifactDefinition): String {
        val bluePrintBasePath: String = context[com.brvith.framework.core.BluePrintConstants.PROPERTY_BLUEPRINT_BASE_PATH] as? String
                ?: throw com.brvith.framework.core.BluePrintException(String.format("failed to get property (%s) from context", com.brvith.framework.core.BluePrintConstants.PROPERTY_BLUEPRINT_BASE_PATH))

        if (artifactDefinition.repository != null) {
            TODO()
        } else if (artifactDefinition.file != null) {
            return ResourceResolverUtils.getFileContent(artifactDefinition.file!!, bluePrintBasePath)
        }
        return ""
    }
}

