package com.brvith.framework.controller.service

import com.brvith.framework.controller.ControllerProperty
import com.brvith.framework.controller.data.SpecGenerationRequest
import com.brvith.framework.controller.data.SpecGenerationResponse
import com.brvith.framework.controller.data.Status
import com.brvith.framework.core.service.BluePrintContext
import com.brvith.framework.core.utils.BluePrintMetadataUtils
import com.brvith.framework.modules.db.service.BlueprintCatalogDBService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import reactor.core.publisher.toMono
import java.io.File

@Service
class SpecGeneratorService {

    val logger: Logger = LoggerFactory.getLogger(this::class.toString())
    val controllerProperty: ControllerProperty
    val blueprintCatalogDBService: BlueprintCatalogDBService

    constructor(
            controllerProperty: ControllerProperty,
            blueprintCatalogDBService: BlueprintCatalogDBService) {
        this.controllerProperty = controllerProperty
        this.blueprintCatalogDBService = blueprintCatalogDBService

    }

    fun generateBlueprintSpec(specGenerationRequest: SpecGenerationRequest): Mono<SpecGenerationResponse> {

        val requestId = specGenerationRequest.header.requestId
        val blueprintName = specGenerationRequest.blueprintName
        val blueprintVersion = specGenerationRequest.blueprintVersion
        val executionPath = controllerProperty.executionPath.plus(File.separator).plus(requestId)


        blueprintCatalogDBService.prepareBlueprint(blueprintName, blueprintVersion, executionPath )

        val csarBasePath = StringBuffer(controllerProperty.executionPath).append(File.separator)
                .append(requestId).toString()

        val bluePrintContext : BluePrintContext = BluePrintMetadataUtils.getBluePrintContext(csarBasePath)

        return SpecGenerationResponse(Status(code = "200", message = "success")).toMono()
    }

}