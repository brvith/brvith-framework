package com.brvith.framework.core.utils

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.apache.commons.io.FileUtils
import org.apache.commons.io.FilenameUtils
import java.io.File
import java.nio.charset.Charset

object ServiceTemplateUtils{

    fun getServiceTemplate(fileName : String): com.brvith.framework.core.ServiceTemplate {
        val fileType: String = FilenameUtils.getExtension(fileName)
        val content : String = FileUtils.readFileToString(File(fileName), Charset.defaultCharset())
        return getServiceTemplate(fileType, content)
    }

    fun getServiceTemplate(fileType : String, content : String): com.brvith.framework.core.ServiceTemplate {
        if (fileType.equals("json", true)) {
            return getServiceTemplateFromContent(content)
        } else if (fileType.equals("yaml", true)) {
            return readBlueprintYamlContent(content)
        } else {
            throw com.brvith.framework.core.BluePrintException("Failed to read blueprint for extension " + fileType)
        }
        return jacksonObjectMapper().readValue<com.brvith.framework.core.ServiceTemplate>(content)
    }

    fun getServiceTemplateFromContent(content: String): com.brvith.framework.core.ServiceTemplate {
        return JacksonUtils.readValue<com.brvith.framework.core.ServiceTemplate>(content)
    }

    private fun readBlueprintYamlContent(content: String): com.brvith.framework.core.ServiceTemplate {
        val mapper = ObjectMapper(YAMLFactory())
        return mapper.readValue<com.brvith.framework.core.ServiceTemplate>(content)
    }
}