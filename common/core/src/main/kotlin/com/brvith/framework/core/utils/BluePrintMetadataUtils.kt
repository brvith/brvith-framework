package com.brvith.framework.core.utils

import com.brvith.framework.core.factories.BluePrintParserFactory
import com.brvith.framework.core.service.BluePrintContext
import com.brvith.tosca.model.service.BluePrintRuntimeService
import org.apache.commons.io.FileUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.nio.charset.Charset

object BluePrintMetadataUtils {
    private val logger: Logger = LoggerFactory.getLogger(this::class.toString())

    fun toscaMetaData(filePath: String): com.brvith.framework.core.ToscaMetaData {
        var toscaMetaData = com.brvith.framework.core.ToscaMetaData()
        val lines : MutableList<String> =  FileUtils.readLines(File(filePath), Charset.defaultCharset())
        lines.forEach { line ->
            if(line.contains(":")){
                val keyValue = line.split(":")
               if(keyValue.size == 2){
                   val value : String = keyValue[1].trim()
                   when(keyValue[0]){
                       "TOSCA-Meta-Version" -> toscaMetaData.toscaMetaVersion = value
                       "CSAR-Version" -> toscaMetaData.csarVersion = value
                       "Created-By" -> toscaMetaData.createdBy = value
                       "Entry-Definitions" -> toscaMetaData.entityDefinitions =  value
                   }
               }
            }

        }
        return toscaMetaData
    }

    fun getBluePrintContext(blueprintBasePath: String): BluePrintContext {

        val metaDataFile = StringBuilder().append(blueprintBasePath).append(File.separator)
                .append(com.brvith.framework.core.BluePrintConstants.DEFAULT_TOSCA_METADATA_ENTRY_DEFINITION_FILE).toString()

        val toscaMetaData: com.brvith.framework.core.ToscaMetaData = BluePrintMetadataUtils.toscaMetaData(metaDataFile)

        logger.info("Processing blueprint base path ({}) and entry definition file ({})", blueprintBasePath, toscaMetaData.entityDefinitions)

        return BluePrintParserFactory.instance(com.brvith.framework.core.BluePrintConstants.TYPE_DEFAULT)!!
                .readBlueprintFile(toscaMetaData.entityDefinitions!!, blueprintBasePath)
    }

    fun getBluePrintRuntime(requestId :String, blueprintBasePath: String): BluePrintRuntimeService {

        val metaDataFile = StringBuilder().append(blueprintBasePath).append(File.separator)
                .append(com.brvith.framework.core.BluePrintConstants.DEFAULT_TOSCA_METADATA_ENTRY_DEFINITION_FILE).toString()

        val toscaMetaData : com.brvith.framework.core.ToscaMetaData = BluePrintMetadataUtils.toscaMetaData(metaDataFile)

        logger.info("Processing blueprint base path ({}) and entry definition file ({})", blueprintBasePath, toscaMetaData.entityDefinitions)

        val bluePrintContext: BluePrintContext = BluePrintParserFactory.instance(com.brvith.framework.core.BluePrintConstants.TYPE_DEFAULT)!!
                .readBlueprintFile(toscaMetaData.entityDefinitions!!, blueprintBasePath)

        val context: MutableMap<String, Any> = hashMapOf()
        context[com.brvith.framework.core.BluePrintConstants.PROPERTY_BLUEPRINT_BASE_PATH] = blueprintBasePath
        context[com.brvith.framework.core.BluePrintConstants.PROPERTY_BLUEPRINT_PROCESS_ID] = requestId

        val bluePrintRuntimeService : BluePrintRuntimeService = BluePrintRuntimeService(bluePrintContext, context)

        return bluePrintRuntimeService
    }
}