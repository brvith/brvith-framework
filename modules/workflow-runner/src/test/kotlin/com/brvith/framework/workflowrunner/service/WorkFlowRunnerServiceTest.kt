package com.brvith.framework.workflowrunner.service

import com.brvith.framework.core.utils.JacksonUtils
import com.brvith.framework.workflowrunner.WorkFlowRunnerConfiguration
import org.junit.Test
import org.junit.runner.RunWith
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.jdbc.EmbeddedDataSourceConfiguration
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@ContextConfiguration(classes = arrayOf(EmbeddedDataSourceConfiguration::class, WorkFlowRunnerConfiguration::class))
class WorkFlowRunnerServiceTest {

    private val log = LoggerFactory.getLogger(WorkFlowRunnerServiceTest::class.java)

    @Autowired
    private lateinit var workFlowRunnerService: WorkFlowRunnerService

    @Test
    fun testExecute() {
        val serviceTemplate = JacksonUtils.readValueFromClassPathFile("workflows/simple-workflow.json",
                com.brvith.framework.core.ServiceTemplate::class.java)!!

        workFlowRunnerService.executeWorkFlow(serviceTemplate, "sample-workflow")

    }


}