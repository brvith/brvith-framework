package com.brvith.framework.workflowrunner.service

import com.brvith.framework.workflowrunner.factory.WorkFlowRunnerJobFactory
import com.brvith.framework.workflowrunner.utils.WorkFlowGraphUtils
import org.slf4j.LoggerFactory
import org.springframework.batch.core.JobParametersBuilder
import org.springframework.batch.core.launch.JobLauncher
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service


@Service
class WorkFlowRunnerService {
    private val log = LoggerFactory.getLogger(WorkFlowRunnerService::class.java)
    @Autowired
    private lateinit var workFlowRunnerJobFactory: WorkFlowRunnerJobFactory

    @Autowired
    private lateinit var jobLauncher: JobLauncher

    fun executeWorkFlow(serviceTemplate: com.brvith.framework.core.ServiceTemplate, workFlowName: String) {

        serviceTemplate.topologyTemplate?.workflows?.filter { it.key == workFlowName }!!
                .forEach { wfName, workflow ->

                    val workFlowGraphUtils = WorkFlowGraphUtils(wfName, workflow)
                    val graph = workFlowGraphUtils.generateGraph()
                    log.info("Graph : {} ",graph.toDSLText() )

                    val job = workFlowRunnerJobFactory.getWorkFlowJob(workFlowName, graph.toDSLText())

                    val jobBuilder = JobParametersBuilder()
                    jobBuilder.addString("workflow-name", workFlowName)

                    val jobExecution = jobLauncher.run(job, jobBuilder.toJobParameters())
                }
    }
}