package com.brvith.framework.script.executor.service

import com.brvith.framework.script.executor.ScriptExecutorFactory
import com.brvith.framework.script.executor.ScriptExecutorType
import org.apache.commons.io.FileUtils
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.nio.charset.Charset
import kotlin.test.assertNotNull

class PythonExecutorServiceTest {
    val logger: Logger = LoggerFactory.getLogger(this::class.toString())

    @Test
    fun testExecuteContent(): Unit {
        val scriptExecutorService = ScriptExecutorFactory.instance(ScriptExecutorType.PYTHON)

        scriptExecutorService.execute(".",
                "print \"Python - Hello, world!\"")

        var returnParameters: MutableList<String> = arrayListOf()
        returnParameters.add("result")
        val results = scriptExecutorService.execute(".",
                "result=2 + 3", arrayListOf(), returnParameters)

        assertNotNull(results, "failed to python execution get result")
        assertNotNull(results.get("result"), "failed to match the output value")
        println("results " + results)
    }

    @Test
    fun executeComponent(): Unit {
        val executePath = javaClass.classLoader.getResource(".").path + "/python"
        val componentName = "SamplePythonComponentNode"
        val filePath = executePath + "/SamplePythonComponentNode.py"
        val pythonPath: MutableList<String> = arrayListOf()
        val content = FileUtils.readFileToString(File(filePath), Charset.defaultCharset())

        val properties: MutableMap<String, Any> = hashMapOf()
        properties.put("logger", logger)

        val scriptExecutorService = ScriptExecutorFactory.instance(ScriptExecutorType.PYTHON)

        val componentNode = scriptExecutorService.getComponent(executePath, pythonPath,
                content, componentName, properties)
        assertNotNull(componentNode, "failed to get SamplePythonComponentNode")

        val context: MutableMap<String, Any> = hashMapOf()
        context.put("name", "brinda")

        val componentContext: MutableMap<String, Any?> = hashMapOf()
        componentContext.put("name", componentName)

        componentNode.process(context, componentContext)

        logger.info("componentContext {}", componentContext)
    }


}