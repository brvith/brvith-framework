package com.brvith.framework.workflowrunner.listener

import org.slf4j.LoggerFactory
import org.springframework.batch.core.ExitStatus
import org.springframework.batch.core.StepExecution
import org.springframework.batch.core.StepExecutionListener
import org.springframework.beans.factory.config.ConfigurableBeanFactory
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Service

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
class WorkFlowStepExecutionListener : StepExecutionListener {

    private val log = LoggerFactory.getLogger(WorkFlowStepExecutionListener::class.java)

    override fun beforeStep(stepExecution: StepExecution) {
        // Global Context
        stepExecution.jobExecution.executionContext.put(stepExecution.stepName + "_global_context", "junk")
        // Task Context
        stepExecution.executionContext.put(stepExecution.stepName + "_task_context", "value")
    }

    override fun afterStep(stepExecution: StepExecution): ExitStatus {
        log.info(String.format("AfterStep processing for stepExecution %s", stepExecution))
        return ExitStatus.COMPLETED

    }

}