package com.brvith.framework.core

class BluePrintException : Exception {
    constructor(message: String, cause: Throwable) : super(message, cause)
    constructor(message: String) : super(message)
    constructor(cause: Throwable) : super(cause)
    constructor(cause: Throwable, message: String, vararg args: Any?) : super(com.brvith.framework.core.format(message, *args), cause)
}

