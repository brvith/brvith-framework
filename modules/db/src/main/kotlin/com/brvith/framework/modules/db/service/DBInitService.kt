package com.brvith.framework.modules.db.service

import com.brvith.framework.modules.db.DatabaseProperty
import org.apache.commons.io.FileUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.io.File

@Service
class DBInitService {
    val logger: Logger = LoggerFactory.getLogger(this::class.toString())

    var databaseProperty : DatabaseProperty

    var blueprintCatalogDBService: BlueprintCatalogDBService

    constructor(databaseProperty : DatabaseProperty, blueprintCatalogDBService: BlueprintCatalogDBService) {
        this.databaseProperty = databaseProperty
        this.blueprintCatalogDBService = blueprintCatalogDBService
        loadBlueprintCatalog()
        //loadBlueprintModels()
    }

    fun loadBlueprintCatalog() {
        logger.info("loading initial csar from the path ({})", databaseProperty.initialLoadPath)

        val loadDirPath : String = if( databaseProperty.initialLoadPath.startsWith("/")){
            databaseProperty.initialLoadPath
        }else{
            javaClass.classLoader.getResource(".").path + databaseProperty.initialLoadPath
        }

        val loadDir = File(loadDirPath)

        if(loadDir.exists()) {
            FileUtils.listFiles(loadDir, arrayOf("zip"), true).forEach { file ->
                logger.info("************* Loading Resource {} *****************", file.absolutePath)
                blueprintCatalogDBService.deployBlueprint(file.absolutePath, databaseProperty.initialLoadPath).subscribe()
            }
        }else{
            logger.info("Load dir {} doesn't exists", loadDir.absolutePath)
        }
    }

//    fun loadBlueprintModels() {
//        var blueprintModelEntity: BlueprintModelEntity = BlueprintModelEntity(modelName = "sample2",
//                tags = "tags1", updatedBy = "Brinda Santh M <brindasanth@gmail.com>", definitionType = "node_types", definition = "sample")
//        this.blueprintModelEntityRepository.save(blueprintModelEntity).subscribe({
//            logger.info("Saved Model " + blueprintModelEntity.modelName)
//
//            logger.info("Started Searching Results")
//            this.blueprintModelEntityRepository.findAll().subscribe { model ->
//                logger.info("Found Result " + model.modelName)
//            }
//        })
//    }

}